# Movement Primitive Library

MPLib is a light weighted library of Dynamic Movement Primitive Library ([dmplib](https://gitlab.com/h2t/DynamicMovementPrimitive.git))
MPLib contains DMP and [VMP](https://ieeexplore.ieee.org/document/8968586) developed in our group. 

### Requirement

In order to use MPLib, you need the following extra libraries:

- CMake (required minimun version: 2.6) [https://cmake.org/] 
- Eigen3 [http://eigen.tuxfamily.org/index.php?title=Main_Page]
- BOOST [http://www.boost.org/]
 
All libraries are available as default packages in Ubuntu 18.04 and are tested with them.
They can be installed with:
```sh
$ sudo apt-get install libboost-dev libboost-test-dev libboost-serialization-dev libeigen3-dev cmake g++ git  libblas-dev swig libpython-dev
```

To use [via-point movement primitives](https://ieeexplore.ieee.org/document/8968586), make sure to install armadillo [http://arma.sourceforge.net/]
```sh
$ sudo apt install libopenblas-dev liblapack-dev
```

To visualize trajectories, make sure to install gnuplot [http://www.gnuplot.info/]
```sh
$ sudo apt-get install gnuplot
```

### Download


You can download our MP library as a normal git repository:
```sh
$ git clone https://gitlab.com/h2t/MovementPrimitiveLibrary.git mplib
$ cd mplib/build
$ cmake ..
$ make
```

### Examples

After you build MPLib, open your IDE (recommended: QT). You will find the folder "examples". We have several examples, from which you can learn how to use MPLib. You can also find the executable files in the "/build/bin/" directory, where you can have a first look at the result given by MPLib.

### Structure

MPLib contains several parts:

- representation (contains different types of DMPs (Discrete and Periodic), VMPs (GaussVMP and PCVMP) supported in our MPLib)
- functionapproximation (contains different types of regression models)
- general (contains some general helper functions)
- generalization (contains MP generalization tools)

### Usage

##### Learn and Reproduce a Trajectory
In MPLib, we have also a special data structure to represent sampled trajectories (SampledTrajectory). We show a simple example here to illustrate the usage of MPLib. You can find more examples in the "examples/" directory.

You can learn a MP from a trajectory.
```c++
SampledTrajectory traj;
traj.readFromCSVFile(filepath);
DiscreteDMPPtr dmp(new DiscreteDMP());
dmp->learnFromTrajectory(traj);
```
You can get a reproduced trajectory from a learned DMP. "DVec" is just a "vector<double>" 

```c++
DVec timestamps = SampledTrajectory::generateTimestamps(startTime, endTime, stepSize);
DVec goals;
DVec2d initialStates;
for(size_t i = 0; i < traj.dim(); i++)
{
    DVec state;
    state.push_back(traj.begin()->getPosition(i)); 
    state.push_back(traj.begin()->getDeriv(i,1));
    initialStates.push_back(state);
    goals.push_back(traj.rbegin()->getPosition(i));
}
DVec initialCanonicalValue = DVec(1.0);
double temporalFactor = 1.0;
SampledTrajectory newtraj = dmp->calculateTrajectory(timestamps, goals, initialStates, initialCanonicalValue, temporalFactor);
```


