/**
* This file is part of MPLIB.
*
* MPLIB is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* MPLIB is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MPLIB::
* @author     H2T @ KIT
* @date       2021
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

//#include <QCoreApplication>

#include <fstream>
#include <iostream>

#include <mplib/plot/GNUPlot.h>
#include <mplib/representation/dmp/DiscreteDMP.h>
#include <mplib/core/SampledTrajectory.h>
#include <mplib/math/canonical_system/CanonicalSystem.h>

#include <boost/archive/xml_iarchive.hpp>
#include <boost/filesystem.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/serialization/vector.hpp>

int main(int argc, char* argv[])
{
    using namespace mplib::core;
    using namespace mplib::representation;
    using namespace mplib::representation::dmp;

    // Load Discrete DMP
    boost::filesystem::path path = boost::filesystem::current_path();
    if (argc < 2)
    {
        path /= "DiscreteDMP.xml";
    }
    else
    {
        path /= argv[1];
    }
    DiscreteDMP* dmpPtr;
    std::ifstream ifs(path.string());
    boost::archive::xml_iarchive ari(ifs);
    ari >> boost::serialization::make_nvp("dmp", dmpPtr);
    ifs.close();

    // prepare initial state and goals for reproducing new trajectory
    std::vector<MPState > initialState = dmpPtr->getStartState();
    DVec2d initialStateVec = SystemState::convertStatesTo2DArray<MPState>(initialState);
    DVec goals = dmpPtr->getGoals();

    DVec timestamps = SampledTrajectory::generateTimestamps(0, 1.0, 1.0 / (-0.5 + 1000));
    SampledTrajectory newTraj =
            dmpPtr->calculateTrajectory(timestamps, goals, initialStateVec, DVec{1.0}, 1.0);

    // only for plotting
    KILL_ALL_PLOT_WINDOWS;
    mplib::plot::GNUPlot plots;
    for(size_t i = 0; i < newTraj.dim(); i++)
    {
        plots.clear();
        plots.plotLine(newTraj.getTimestamps(), newTraj.getDimensionData(i, 0), "testing", "red");
        plots.show();
    }

}


