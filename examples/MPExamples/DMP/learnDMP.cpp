/**
* This file is part of MPLIB.
*
* MPLIB is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* MPLIB is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MPLIB::
* @author     H2T @ KIT
* @date       2021
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

//#include <QCoreApplication>

#include <fstream>
#include <iostream>

#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/vector.hpp>

#include <mplib/core/SampledTrajectory.h>
#include <mplib/factories/DMPFactory.h>
#include <mplib/factories/MPFactoryCreator.h>
#include <mplib/plot/GNUPlot.h>
#include <mplib/representation/dmp/DiscreteDMP.h>
#include <mplib/representation/dmp/PeriodicDMP.h>
#include <mplib/serialization/boost/mp_registration.h>

namespace bpo = boost::program_options;

int
main(int argc, char* argv[])
{
    using namespace mplib;
    using namespace mplib::core;
    using namespace mplib::representation;
    using namespace mplib::representation::dmp;
    using namespace mplib::math::function_approximation;

    bpo::options_description opt_desc("LearnDMP (an example application)");
    opt_desc.add_options()("help", "help message")(
        "filename", bpo::value<std::string>(), "filename contains the trajectory to be learned")(
        "n_kernels", bpo::value<int>(), "number of kernels")(
        "dmp_type",
        bpo::value<std::string>(),
        "type of vmp (discrete or periodic, default: discrete)")(
        "reg_model",
        bpo::value<std::string>(),
        "learning models (lwr: locally weighted regression, gpr: gaussian process regression)");

    bpo::variables_map vm;
    bpo::store(bpo::parse_command_line(argc, argv, opt_desc), vm);
    bpo::notify(vm);

    if (vm.count("help"))
    {
        std::cout << opt_desc << std::endl;
        return 0;
    }

    boost::filesystem::path file(DATA_DIR);
    if (vm.count("filename"))
    {
        file /= vm["filename"].as<std::string>();
    }
    else
    {
        file /= "ExampleTrajectories/sampletraj.csv";
    }

    int n_kernels = 100;
    if (vm.count("n_kernels"))
    {
        n_kernels = vm["n_kernels"].as<int>();
    }

    auto dmp_type = representation::DMPType::Discrete;
    if (vm.count("dmp_type"))
    {
        std::string t = vm["dmp_type"].as<std::string>();
        dmp_type = representation::DMPType::_from_string_nocase(t.c_str());
    }

    std::string reg_model = "";
    if (vm.count("reg_model"))
    {
        reg_model = vm["reg_model"].as<std::string>();
    }

    // Load one trajectory
    SampledTrajectory traj;
    traj.readFromCSVFile(file.string());
    traj = SampledTrajectory::normalizeTimestamps(traj);
    // prepare initial state and goals for reproducing new trajectory

    // setup dmp and train dmp
    factories::DMPFactory mpfactory;
    mpfactory.addConfig("kernelSize", n_kernels);

    DVec goals;
    double initCanVal = 1.0;
    auto dmp = mpfactory.createMP(dmp_type);

    switch (dmp_type)
    {
        case representation::DMPType::Discrete:
            goals = traj.getStates(*traj.getTimestamps().rbegin(), 0);
            break;

        case representation::DMPType::Periodic:
            goals = traj.calcAnchorPoint();
            initCanVal = 0.0;
            break;
    }

#ifdef ARMADILLO_FOUND
    if (reg_model == "gpr")
    {
        dmp->setBaseFunctionApproximator(std::make_unique<GaussianProcessRegression<math::kernel::SquaredExponentialCovarianceFunction>>());
        traj = SampledTrajectory::downSample(traj, 50);
    }
#endif

    if (reg_model == "lwr")
    {
        dmp->setBaseFunctionApproximator(std::make_unique<LocallyWeightedRegression<math::kernel::GaussianFunction>>());
        traj = SampledTrajectory::downSample(traj, 50);
    }

    DVec2d initialState;
    for (size_t i = 0; i < traj.dim(); i++)
    {
        DVec state;
        state.push_back(traj.begin()->getPosition(i));
        state.push_back(traj.begin()->getDeriv(i, 1));
        initialState.push_back(state);
    }

    // train a dynamic movement primitive (dmp)
    std::vector<SampledTrajectory> trajs{
        traj}; // need to pass multiple trajectories (for dmp, it can only learn from one traj)
    dmp->learnFromTrajectories(trajs);

    DVec timestamps =
        SampledTrajectory::generateTimestamps(0, 1.0, 1.0 / (-0.5 + traj.getTimestamps().size()));
    SampledTrajectory newTraj =
        dmp->calculateTrajectory(timestamps, goals, initialState, DVec{initCanVal}, 1.0);

    // only for plotting
    KILL_ALL_PLOT_WINDOWS;
    plot::GNUPlot plots;
    for (size_t i = 0; i < traj.dim(); i++)
    {
        plots.clear();
        plots.plotLine(traj.getTimestamps(),
                       traj.getDimensionData(i, 0),
                       "training",
                       "blue",
                       plot::GNUPlot::LineType::Dash);
        plots.plotLine(newTraj.getTimestamps(), newTraj.getDimensionData(i, 0), "testing", "red");
        plots.show();
    }

    // dmp serialization
    switch (dmp_type)
    {
        case mplib::representation::DMPType::Discrete:
        {
            auto* dmpPtr = dynamic_cast<DiscreteDMP*>(dmp.get());
            std::ofstream ofs("DiscreteDMP.xml");
            boost::archive::xml_oarchive aro(ofs);
            aro << boost::serialization::make_nvp("dmp", dmpPtr);
            ofs.close();
            break;
        }
        case mplib::representation::DMPType::Periodic:
        {
            auto* dmpPtr = dynamic_cast<PeriodicDMP*>(dmp.get());
            std::ofstream ofs("PeriodicDMP.xml");
            boost::archive::xml_oarchive aro(ofs);
            aro << boost::serialization::make_nvp("dmp", dmpPtr);
            ofs.close();
            break;
        }
    }
}
