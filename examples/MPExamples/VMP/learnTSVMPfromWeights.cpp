#include <fstream>

#include <boost/filesystem.hpp>
#include <boost/range/iterator_range.hpp>

#include <boost/unordered_map.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/program_options.hpp>

#include <sys/time.h>
#include <iostream>

#include <mplib/representation/vmp/TaskSpacePrincipalComponentVMP.h>
#include <mplib/representation/vmp/TaskSpaceGaussianVMP.h>
#include <mplib/representation/vmp/ViaPose.h>
#include <mplib/plot/GNUPlot.h>

#include <armadillo>

namespace bpo = boost::program_options;


using namespace mplib::core;

using namespace boost::filesystem;

std::vector<SampledTrajectory > getTrajs(const std::string& filepath)
{
    path datapath(filepath);
    std::vector<SampledTrajectory > trajs;
    if(is_directory(datapath))
    {
        for(auto& entry : boost::make_iterator_range(directory_iterator(datapath), {}))
        {
            std::string filepath = entry.path().string();
            SampledTrajectory traj0;
            traj0.readFromCSVFile(filepath);
            traj0 = SampledTrajectory::normalizeTimestamps(traj0, 0, 1);
            traj0.gaussianFilter(0.005);
            trajs.push_back(traj0);
        }
    }
    else
    {
        SampledTrajectory traj0;
        traj0.readFromCSVFile(filepath);
        traj0 = SampledTrajectory::normalizeTimestamps(traj0, 0, 1);
        traj0.gaussianFilter(0.005);
        trajs.push_back(traj0);
    }

    return trajs;
}

int main(int argc, char* argv[])
{
    using namespace mplib::representation::vmp;

    std::string dirname = "ExampleTrajectories/multi_ts";
    boost::filesystem::path fdir(DATA_DIR);
    fdir /= dirname;

    std::vector<SampledTrajectory > trajs = getTrajs(fdir.string());
    std::shared_ptr<VMP> vmp = std::make_shared<TaskSpacePrincipalComponentVMP>("tspcvmp");
    vmp->learnFromTrajectory(trajs[0]);
    mplib::core::DVec2d weights = vmp->getWeights();

    std::shared_ptr<VMP> vmp_from_weights = std::make_shared<TaskSpacePrincipalComponentVMP>("ts_from_weights", weights);

    // set via-point
    {
        Eigen::Quaterniond quat(-0.2, 0.5, -0.3, -0.5);
        quat.normalize();
        vmp->setViaPoint(0.5, DVec(DVec{800, 400, 1000, quat.w(), quat.x(), quat.y(), quat.z()}));
        vmp_from_weights->setViaPoint(0.5, DVec(DVec{800, 400, 1000, quat.w(), quat.x(), quat.y(), quat.z()}));
    }

    DVec2d initialState;
    DVec goals;
    for(size_t i = 0; i < trajs[0].dim(); i++)
    {
        DVec state;
        state.push_back(trajs[0].begin()->getPosition(i));
        state.push_back(0.0);
        initialState.push_back(state);
        goals.push_back(trajs[0].rbegin()->getPosition(i));
    }

    DVec timestamps = SampledTrajectory::generateTimestamps(0, 1.0, 1.0 / (-0.5 + trajs[0].getTimestamps().size()));
    SampledTrajectory vmpTraj = vmp->calculateTrajectory(timestamps, goals, initialState, DVec(1, 1.0), 1.0);
    SampledTrajectory vmpWeightTraj = vmp_from_weights->calculateTrajectory(timestamps, goals, initialState, DVec(1, 1.0), 1.0);

    /*plot the result*/
    KILL_ALL_PLOT_WINDOWS;
    mplib::plot::GNUPlot plt;
    plt.clear();
    std::vector<std::string> colors{"#5BB727", "#F5B041", "#2980B9", "#000000"};
    auto viapoints = vmp->getViaPoints();

    for(size_t i = 0; i < 3; i++)
    {
        for(size_t j = 0; j < 3; ++j)
        {
            plt.plotLine(trajs[0].getTimestamps(),trajs[j].getDimensionData(i, 0), "unknown", colors[i], mplib::plot::GNUPlot::LineType::Dash);
        }
        plt.plotLine(vmpTraj.getTimestamps(), vmpTraj.getDimensionData(i, 0), "unknown", colors[i], mplib::plot::GNUPlot::LineType::Solid, 6);
        plt.plotLine(vmpWeightTraj.getTimestamps(), vmpWeightTraj.getDimensionData(i, 0), "unknown", "#FFFFFF", mplib::plot::GNUPlot::LineType::Solid);

        for(auto& viapoint : viapoints)
        {
            double u = 1 - viapoint.first;
            double pos = viapoint.second[i].pos;
            plt.plotPoint(u, pos, colors[i]);
        }
    }
    plt.show();

    plt.clear();
    for(size_t i = 3; i < 7; i++)
    {
        for(size_t j = 0; j < 3; ++j)
        {
            plt.plotLine(trajs[0].getTimestamps(),trajs[j].getDimensionData(i, 0), "unknown", colors[i - 3], mplib::plot::GNUPlot::LineType::Dash);
        }
        plt.plotLine(vmpTraj.getTimestamps(), vmpTraj.getDimensionData(i, 0), "unknown", colors[i - 3], mplib::plot::GNUPlot::LineType::Solid, 6);
        plt.plotLine(vmpWeightTraj.getTimestamps(), vmpWeightTraj.getDimensionData(i, 0), "unknown", "#FFFFFF", mplib::plot::GNUPlot::LineType::Solid);

        for(auto& viapoint : viapoints)
        {
            double u = 1 - viapoint.first;
            double pos = viapoint.second[i].pos;
            plt.plotPoint(u, pos, colors[i - 3]);
        }

    }
    plt.show();
}
