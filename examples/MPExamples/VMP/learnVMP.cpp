#include <exception>
#include <fstream>
#include <iostream>
#include <stdexcept>

#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/unordered_map.hpp>

#include <mplib/factories/VMPFactory.h>
#include <mplib/factories/AbstractMPFactory.h>
#include <mplib/factories/MPFactoryCreator.h>
#include <mplib/plot/GNUPlot.h>
#include <mplib/representation/vmp/GaussianVMP.h>
#include <mplib/representation/vmp/PrincipalComponentVMP.h>
#include <mplib/serialization/boost/mp_registration.h>
#include <sys/time.h>

namespace bpo = boost::program_options;

int
main(int argc, char* argv[])
{
    using namespace mplib;
    using namespace mplib::core;
    using namespace mplib::math::function_approximation;
    using namespace mplib::representation::vmp;

    bpo::options_description opt_desc("LearnVMP (an example application)");
    opt_desc.add_options()("help", "help message")(
        "dirname", bpo::value<std::string>(), "dirname contains all trajectories")(
        "n_kernels", bpo::value<int>(), "number of kernels")(
        "vmp_type", bpo::value<std::string>(), "type of vmp")(
        "n_comps",
        bpo::value<int>(),
        "number of Gaussian components (only used if vmp_type=='gaussvmp'.")(
        "reg_model",
        bpo::value<std::string>(),
        "learning models (lwr: locally weighted regression, gpr: gaussian process regression)");

    bpo::variables_map vm;
    bpo::store(bpo::parse_command_line(argc, argv, opt_desc), vm);
    bpo::notify(vm);

    if (vm.count("help"))
    {
        std::cout << opt_desc << std::endl;
        return 0;
    }

    std::string dirname = "ExampleTrajectories/multi";
    boost::filesystem::path fdir(DATA_DIR);
    if (vm.count("dirname"))
    {
        fdir /= vm["dirname"].as<std::string>();
    }
    else
    {
        fdir /= dirname;
    }

    int n_kernels = 100;
    if (vm.count("n_kernels"))
    {
        n_kernels = vm["n_kernels"].as<int>();
    }

    // int n_comps = 1;
    // if (vm.count("n_comps"))
    // {
    //     n_comps = vm["n_comps"].as<int>();
    // }

    representation::VMPType vmp_type = representation::VMPType::PrincipalComponent;
    if (vm.count("vmp_type"))
    {
        vmp_type =
            representation::VMPType::_from_string_nocase(vm["vmp_type"].as<std::string>().c_str());
    }

    std::string reg_model = "lwr";
    if (vm.count("reg_model"))
    {
        reg_model = vm["reg_model"].as<std::string>();
    }

    if (!boost::filesystem::is_directory(fdir))
    {
        throw Exception()
            << "learnVMP requires a directory containing single or multiple trajectories";
    }

    std::vector<SampledTrajectory> trajs;
    for (auto& entry : boost::make_iterator_range(boost::filesystem::directory_iterator(fdir), {}))
    {
        SampledTrajectory traj;
        traj.readFromCSVFile(entry.path().string());
        trajs.push_back(traj);
    }

    // setup dmp and train dmp
    factories::VMPFactory mpfactory;
    mpfactory.addConfig("kernelSize", n_kernels);
    std::shared_ptr<representation::AbstractMovementPrimitive> vmp = mpfactory.createMP(vmp_type);

    if (reg_model == "gpr")
    {
        vmp->setBaseFunctionApproximator(std::make_unique<GaussianProcessRegression<math::kernel::SquaredExponentialCovarianceFunction>>(0.01, 0.1, 0.000001));
        for (auto& traj : trajs)
        {
            traj = SampledTrajectory::downSample(traj, 100);
        }
    }

    vmp->learnFromTrajectories(trajs);

    // different functions to manipulate the generated trajectories
    //    if(vmp_type == "pvmp")
    //    {
    //        // play with the numbers and see the different trajectories.
    //        DVec ratios(core::DVec{1.0, 0.0});
    //        boost::dynamic_pointer_cast<PCVMP>(vmp)->setStyleRatios(ratios);
    //    }

    //    if(vmp_type == "gvmp")
    //    {
    //        // choose different Gaussian compont id.
    //        boost::dynamic_pointer_cast<GaussVMP>(vmp)->setCurrCompId(0);
    //    }

    //    // set via-point
    //    {
    //        vmp->setViaPoint(0.2, DVec(vector<double>{1.5,0.5}));
    //        vmp->setViaPoint(0.5, DVec(vector<double>{1.0,0.5}));
    //        vmp->setViaPoint(0.8, DVec(vector<double>{-1.5, 0.5}));
    //    }

    DVec2d initialState;
    DVec goals;
    for (size_t i = 0; i < trajs[0].dim(); i++)
    {
        DVec state;
        state.push_back(trajs[0].begin()->getPosition(i));
        state.push_back(trajs[0].begin()->getDeriv(i, 1));
        initialState.push_back(state);
        goals.push_back(trajs[0].rbegin()->getPosition(i));
    }

    DVec timestamps = SampledTrajectory::generateTimestamps(
        0, 1.0, 1.0 / (-0.5 + trajs[0].getTimestamps().size()));

    SampledTrajectory newTraj =
        vmp->calculateTrajectory(timestamps, goals, initialState, DVec(1, 1.0), 1.0);

    KILL_ALL_PLOT_WINDOWS;
    plot::GNUPlot plt;

    auto viapoints = std::dynamic_pointer_cast<VMP>(vmp)->getViaPoints();
    for (size_t i = 0; i < trajs[0].dim(); i++)
    {
        plt.clear();
        for (size_t j = 0; j < trajs.size(); ++j)
        {
            plt.plotLine(timestamps,
                         trajs[j].getDimensionData(i, 0),
                         std::string("trainingTrajs") + "_" + std::to_string(j),
                         "blue",
                         plot::GNUPlot::LineType::Dash,
                         2);
        }

        plt.plotLine(timestamps,
                     newTraj.getDimensionData(i, 0),
                     "testing",
                     "red",
                     plot::GNUPlot::LineType::Solid,
                     2);

        for (auto& viapoint : viapoints)
        {
            double u = 1 - viapoint.first;
            double pos = viapoint.second[i].pos;
            plt.plotPoint(u, pos);
        }

        plt.show();
    }

    // vmp serialization
    std::ofstream ofs("VMP.xml");
    boost::archive::xml_oarchive aro(ofs);

    switch (vmp_type)
    {
        case representation::VMPType::PrincipalComponent:
        {
            auto* vmpPtr = std::dynamic_pointer_cast<PrincipalComponentVMP>(vmp).get();
            aro << boost::serialization::make_nvp("pcvmp", vmpPtr);
            break;
        }
        case representation::VMPType::Gaussian:
        {
            auto* vmpPtr = std::dynamic_pointer_cast<GaussianVMP>(vmp).get();
            aro << boost::serialization::make_nvp("gaussvmp", vmpPtr);
            break;
        }
        default:
            std::invalid_argument("Unsupported VMP type!");
    }

    ofs.close();

    // vmp loading test
    {
        std::cout << "test serialization" << std::endl;
        std::ifstream ifs("VMP.xml");
        boost::archive::xml_iarchive ari(ifs);
        std::shared_ptr<VMP> nvmp;

        switch (vmp_type)
        {
            case representation::VMPType::PrincipalComponent:
            {
                PrincipalComponentVMP* vmpPtr = nullptr;
                ari >> boost::serialization::make_nvp("pcvmp", vmpPtr);
                nvmp.reset(vmpPtr);
                break;
            }
            case representation::VMPType::Gaussian:
            {
                GaussianVMP* vmpPtr = nullptr;
                ari >> boost::serialization::make_nvp("gaussvmp", vmpPtr);
                nvmp.reset(vmpPtr);
                break;
            }
            default:
                throw std::invalid_argument("Unsupported VMP type!");
        }

        ifs.close();

        SampledTrajectory nTraj =
            nvmp->calculateTrajectory(timestamps, goals, initialState, DVec(1, 1.0), 1.0);
        plt.clear();

        for (size_t i = 0; i < nTraj.dim(); ++i)
        {
            plt.plotLine(nTraj.getTimestamps(), nTraj.getDimensionData(i, 0), "unknown", "blue");
        }

        plt.show();
    }
}
