#include <fstream>

#include <boost/filesystem.hpp>
#include <boost/range/iterator_range.hpp>

#include <boost/unordered_map.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/program_options.hpp>

#include <sys/time.h>
#include <iostream>

#include <mplib/representation/vmp/TaskSpacePrincipalComponentVMP.h>
#include <mplib/representation/vmp/TaskSpaceGaussianVMP.h>
#include <mplib/representation/vmp/ViaPose.h>
#include <mplib/plot/GNUPlot.h>

#include <armadillo>

namespace bpo = boost::program_options;


using namespace mplib::core;

using namespace boost::filesystem;

std::vector<SampledTrajectory > getTrajs(const std::string& filepath)
{
    path datapath(filepath);
    std::vector<SampledTrajectory > trajs;
    if(is_directory(datapath))
    {
        for(auto& entry : boost::make_iterator_range(directory_iterator(datapath), {}))
        {
            std::string filepath = entry.path().string();
            SampledTrajectory traj0;
            traj0.readFromCSVFile(filepath);
            traj0 = SampledTrajectory::normalizeTimestamps(traj0, 0, 1);
            traj0.gaussianFilter(0.005);
            trajs.push_back(traj0);
        }
    }
    else
    {
        SampledTrajectory traj0;
        traj0.readFromCSVFile(filepath);
        traj0 = SampledTrajectory::normalizeTimestamps(traj0, 0, 1);
        traj0.gaussianFilter(0.005);
        trajs.push_back(traj0);
    }

    return trajs;
}

int main(int argc, char* argv[])
{
    using namespace mplib::representation::vmp;

    bpo::options_description opt_desc("LearnTSVMP (an example application)");
    opt_desc.add_options()
            ("help", "help message")
            ("dirname", bpo::value<std::string>() ,"dirname contains all trajectories")
            ("n_kernels", bpo::value<int>(), "number of kernels")
            ("vmp_type", bpo::value<std::string>(), "type of vmp (tspcvmp or tsgaussvmp, default: tspcvmp)")
            ("n_comps", bpo::value<int>(), "number of Gaussian components (only used if vmp_type=='tsgaussvmp').");


    bpo::variables_map vm;
    bpo::store(bpo::parse_command_line(argc, argv, opt_desc), vm);
    bpo::notify(vm);

    if(vm.count("help"))
    {
        std::cout << opt_desc << std::endl;
        return 0;
    }

    std::string dirname = "ExampleTrajectories/multi_ts";
    boost::filesystem::path fdir(DATA_DIR);
    if(vm.count("dirname"))
    {
        fdir /= vm["dirname"].as<std::string>();
    }
    fdir /= dirname;

    int n_kernels = 100;
    if(vm.count("n_kernels"))
    {
        n_kernels = vm["n_kernels"].as<int>();
    }

    int n_comps = 1;
    if(vm.count("n_comps"))
    {
        n_comps = vm["n_comps"].as<int>();
    }

    auto vmp_type = mplib::representation::VMPType::TaskSpacePrincipalComponent;
    if(vm.count("vmp_type"))
    {
        std::string t = vm["vmp_type"].as<std::string>();
        vmp_type = mplib::representation::VMPType::_from_string_nocase(t.c_str());
    }

    std::vector<SampledTrajectory > trajs = getTrajs(fdir.string());

    std::shared_ptr<VMP> vmp = nullptr;
    switch (vmp_type)
    {
        case mplib::representation::VMPType::TaskSpacePrincipalComponent:
        {
            vmp.reset(new TaskSpacePrincipalComponentVMP("tspcvmp", n_kernels));
            vmp->learnFromTrajectories(trajs);
            // play with the numbers and see the different trajectories.
            DVec ratios(DVec{1.0, 0.0, 0.0});
            std::dynamic_pointer_cast<TaskSpacePrincipalComponentVMP>(vmp)->setStyleRatios(ratios);
            break;
        }
        case mplib::representation::VMPType::TaskSpaceGaussian:
        {
            vmp.reset(new TaskSpaceGaussianVMP(n_comps, "tsgaussvmp", n_kernels));
            vmp->learnFromTrajectories(trajs);
            // choose different Gaussian compont id.
            std::dynamic_pointer_cast<TaskSpaceGaussianVMP>(vmp)->setCurrCompId(0);
            break;
        }
        default:
            std::invalid_argument("Unsupported VMP type!");
    }

    // set via-point
    {
        Eigen::Quaterniond quat(-0.2, 0.5, -0.3, -0.5);
        quat.normalize();
        vmp->setViaPoint(0.5, DVec(DVec{800, 400, 1000, quat.w(), quat.x(), quat.y(), quat.z()}));
    }

    DVec2d initialState;
    DVec goals;
    for(size_t i = 0; i < trajs[0].dim(); i++)
    {
        DVec state;
        state.push_back(trajs[0].begin()->getPosition(i));
        state.push_back(0.0);
        initialState.push_back(state);
        goals.push_back(trajs[0].rbegin()->getPosition(i));
    }

    DVec timestamps = SampledTrajectory::generateTimestamps(0, 1.0, 1.0 / (-0.5 + trajs[0].getTimestamps().size()));
    vmp->learnFromTrajectories(trajs);
    SampledTrajectory newTraj = vmp->calculateTrajectory(timestamps, goals, initialState, DVec(1, 1.0), 1.0);

    /*plot the result*/
    KILL_ALL_PLOT_WINDOWS;
    mplib::plot::GNUPlot plt;
    plt.clear();
    std::vector<std::string> colors{"#5BB727", "#F5B041", "#2980B9", "#000000"};
    auto viapoints = vmp->getViaPoints();

    for(size_t i = 0; i < 3; i++)
    {
        for(size_t j = 0; j < 3; ++j)
        {
            plt.plotLine(trajs[0].getTimestamps(),trajs[j].getDimensionData(i, 0), "unknown", colors[i], mplib::plot::GNUPlot::LineType::Dash);
        }
        plt.plotLine(newTraj.getTimestamps(), newTraj.getDimensionData(i, 0), "unknown", colors[i]);

        for(auto& viapoint : viapoints)
        {
            double u = 1 - viapoint.first;
            double pos = viapoint.second[i].pos;
            plt.plotPoint(u, pos, colors[i]);
        }
    }
    plt.show();

    plt.clear();
    for(size_t i = 3; i < 7; i++)
    {
        for(size_t j = 0; j < 3; ++j)
        {
            plt.plotLine(trajs[0].getTimestamps(),trajs[j].getDimensionData(i, 0), "unknown", colors[i - 3], mplib::plot::GNUPlot::LineType::Dash);
        }
        plt.plotLine(newTraj.getTimestamps(), newTraj.getDimensionData(i, 0), "unknown", colors[i - 3]);

        for(auto& viapoint : viapoints)
        {
            double u = 1 - viapoint.first;
            double pos = viapoint.second[i].pos;
            plt.plotPoint(u, pos, colors[i - 3]);
        }

    }
    plt.show();

    // vmp serialization
    std::ofstream ofs("TSVMP.xml");
    boost::archive::xml_oarchive aro(ofs);
    switch (vmp_type)
    {
        case mplib::representation::VMPType::TaskSpacePrincipalComponent:
        {
            auto vmpPtr = std::dynamic_pointer_cast<TaskSpacePrincipalComponentVMP>(vmp).get();
            aro << boost::serialization::make_nvp("tspcvmp", vmpPtr);
            break;
        }
        case mplib::representation::VMPType::TaskSpaceGaussian:
        {
            auto vmpPtr = std::dynamic_pointer_cast<TaskSpaceGaussianVMP>(vmp).get();
            aro << boost::serialization::make_nvp("tsgaussvmp", vmpPtr);
            break;
        }
        default:
            std::invalid_argument("Unsupported VMP type!");
    }

    ofs.close();


    // vmp loading test
    {
        std::cout << "test serialization" << std::endl;
        std::ifstream ifs("TSVMP.xml");
        boost::archive::xml_iarchive ari(ifs);
        std::shared_ptr<VMP> nvmp = nullptr;
        switch (vmp_type)
        {
            case mplib::representation::VMPType::TaskSpacePrincipalComponent:
            {
                TaskSpacePrincipalComponentVMP* vmpPtr;
                ari >> boost::serialization::make_nvp("tspcvmp", vmpPtr);
                nvmp.reset(vmpPtr);
                break;
            }
            case mplib::representation::VMPType::TaskSpaceGaussian:
            {
                TaskSpaceGaussianVMP* vmpPtr;
                ari >> boost::serialization::make_nvp("tsgaussvmp", vmpPtr);
                nvmp.reset(vmpPtr);
                break;
            }
            default:
                std::invalid_argument("Unsupported VMP type!");
        }

        ifs.close();

        SampledTrajectory nTraj = nvmp->calculateTrajectory(timestamps, goals, initialState, DVec(1, 1.0), 1.0);
        plt.clear();

        for(size_t i = 0; i < 3; ++i)
            plt.plotLine(nTraj.getTimestamps(), nTraj.getDimensionData(i, 0), "unknown", "blue");

        plt.show();

        plt.clear();

        for(size_t i = 3; i < 7; ++i)
            plt.plotLine(nTraj.getTimestamps(), nTraj.getDimensionData(i, 0), "unknown", "blue");

        plt.show();


    }



}
