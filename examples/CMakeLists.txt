find_package(Boost ${MPLIB_BOOST_VERSION} EXACT REQUIRED COMPONENTS filesystem program_options serialization)

add_subdirectory(MPExamples)

if ("${ARMADILLO_FOUND}")
    add_subdirectory(MPGeneralization)
endif()
