#include <fstream>

#include <boost/unordered_map.hpp>
#include <boost/range/iterator_range.hpp>

#include "mplib/factories/VMPFactory.h"
#include <sys/time.h>

#include <iostream>

#include <mplib/math/function_approximation/GaussianProcessRegression.h>
#include <mplib/representation/vmp/GaussianVMP.h>
#include <mplib/representation/vmp/PrincipalComponentVMP.h>
#include <mplib/generalization/MPGeneralization.h>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>
#include <mplib/factories/MPFactoryCreator.h>
#include <mplib/factories/AbstractMPFactory.h>
#include <mplib/plot/GNUPlot.h>

int main(int argc, char* argv[])
{
    using namespace mplib;
    using namespace mplib::core;
    using namespace mplib::math::function_approximation;
    namespace bpo = boost::program_options;

    bpo::options_description opt_desc("GenMP: generalize MPs for new queries (check the data/MPGeneralization/capture for training and testing data file format)");
    opt_desc.add_options()
            ("help", "learn MP generalization with a structured directory. The first row of the trajectory csv file contains the query.")
            ("dirname", bpo::value<std::string>() ,"dirname contains train and test folder (see data/MPGeneralization/capture)")
            ("n_kernels", bpo::value<int>(), "number of kernels")
            ("vmp_type", bpo::value<std::string>(), "type of vmp (gaussvmp, pcvmp)");

    bpo::variables_map vm;
    bpo::store(bpo::parse_command_line(argc, argv, opt_desc), vm);
    bpo::notify(vm);

    if(vm.count("help"))
    {
        std::cout << opt_desc << std::endl;
        return 0;
    }

    std::string dirname = "MPGeneralization/capture";
    boost::filesystem::path fdir(DATA_DIR);
    if(vm.count("dirname"))
    {
        fdir /= vm["dirname"].as<std::string>();
    }
    else
    {
        fdir /= dirname;

    }

    int n_kernels = 100;
    if(vm.count("n_kernels"))
    {
        n_kernels = vm["n_kernels"].as<int>();
    }


    representation::VMPType vmp_type = representation::VMPType::PrincipalComponent;
    if (vm.count("vmp_type"))
    {
        vmp_type =
            representation::VMPType::_from_string_nocase(vm["vmp_type"].as<std::string>().c_str());
    }

    boost::filesystem::path ptrain= fdir/"train";

    // prepare training data
    DVec2d queries;
    std::vector<SampledTrajectory > trajs;
    for (auto& entry : boost::make_iterator_range(boost::filesystem::directory_iterator(ptrain), {}))
    {
        std::string filepath = entry.path().string();
        SampledTrajectory traj;
        DVec query;
        traj.readFromCSVFileWithQuery(filepath, query, false);
        queries.push_back(query);
        trajs.push_back(traj);
    }


    factories::VMPFactory mpfactory;
    mpfactory.addConfig("kernelSize", n_kernels);
    std::unique_ptr<representation::vmp::VMP> vmp = mpfactory.createDerivedMP(vmp_type);

    generalization::MPGeneralization mpgen(std::make_unique<GaussianProcessRegression<math::kernel::SquaredExponentialCovarianceFunction>>(20, 1, 0.001), std::move(vmp));
    mpgen.learn(queries, trajs);

    // evaluation
    boost::filesystem::path ptest= fdir/"test";
    KILL_ALL_PLOT_WINDOWS;

    for (auto& entry : boost::make_iterator_range(boost::filesystem::directory_iterator(ptest), {}))
    {
        std::string filepath = entry.path().string();
        SampledTrajectory testTraj;
        DVec query;
        testTraj.readFromCSVFileWithQuery(filepath, query);
        auto mp = mpgen.generateMP(query);

        DVec2d initialState;
        DVec goals;
        for (size_t i = 0; i < testTraj.dim(); i++)
        {
            DVec state{trajs[0].begin()->getPosition(i), trajs[0].begin()->getDeriv(i,1)};
            initialState.push_back(state);
            goals.push_back(testTraj.rbegin()->getPosition(i));
        }

        DVec timestamps = SampledTrajectory::generateTimestamps(0, 1.0, 1.0 / (-0.5 + testTraj.getTimestamps().size()));
        SampledTrajectory newTraj = mp->calculateTrajectory(timestamps, goals, initialState, DVec{1.0}, 1.0);

        plot::GNUPlot plt;
        plt.clear();
        for (size_t i = 0; i < newTraj.dim(); i++)
        {
            plt.plotLine(testTraj.getTimestamps(), testTraj.getDimensionData(i, 0), "training", "blue", plot::GNUPlot::LineType::Dash);
            plt.plotLine(newTraj.getTimestamps(), newTraj.getDimensionData(i, 0), "testing", "red");
        }
        plt.show();
    }
}
