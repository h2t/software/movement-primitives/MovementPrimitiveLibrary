link_directories(${CMAKE_BINARY_DIR})

add_executable(genMP genMP.cpp )
target_link_libraries(genMP MPLIB::MPLIB Boost::system Boost::program_options Boost::filesystem)

target_compile_options(genMP PRIVATE -Werror -Wall -Wunused -Wunreachable-code -Wswitch -Wpedantic -Wno-unknown-pragmas -Wextra -Wno-unused-parameter -Wno-sign-compare) #-Wno-deprecated-copy-with-user-provided-copy)
