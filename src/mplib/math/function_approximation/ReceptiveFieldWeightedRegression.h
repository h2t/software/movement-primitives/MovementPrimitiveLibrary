#pragma once

#include <map>

#include "AbstractFunctionApproximation.h"
#include "mplib/math/kernel/ReceptiveFieldWeightedRegression.h"

namespace mplib::math::function_approximation
{

    class ReceptiveFieldWeightedRegression : public AbstractFunctionApproximation
    {
    public:
        struct KernelData
        {
            kernel::ReceptiveFieldWeightedRegression kernel;
            Eigen::VectorXf beta;
            Eigen::MatrixXf P;
            double Wsum;
            double Esum;

            Eigen::VectorXf H;
            Eigen::MatrixXf R;
            Eigen::VectorXf r;
        };

        ReceptiveFieldWeightedRegression(double forgettingfactor = 0.99);

        // inherited from FunctionApproximationInterface
        void learn(const std::vector<core::DVec>& x, const std::vector<core::DVec>& y) override;
        void ilearn(const core::DVec& x, double y) override;
        const core::DVec operator()(const core::DVec& x) const override;
        void reset() override;
        std::unique_ptr<AbstractFunctionApproximation> clone() override;

        // evaluation
        double evaluate(const std::vector<core::DVec>& x,
                        const core::DVec& y,
                        double* maxError = nullptr,
                        core::DVec* predictedValues = nullptr);

        const std::vector<KernelData>& getKernels() const;

        void setKernels(const std::vector<KernelData>& kernels);

        core::DVec getWeights() const override;

        size_t getKernelSize() const override;

        unsigned int dim();

        bool supportsIncrementalLearning() override;

    protected:
        double calcWeight(const kernel::ReceptiveFieldWeightedRegression& kernel,
                          const std::vector<core::DVec>& x,
                          const core::DVec& y,
                          double guessedMinimum = 0);

        double getWeightValue(unsigned int kernelIndex) const;
        const Eigen::VectorXf getNetInput(const core::DVec& x, int kernelId) const;

    private:
        core::DVec widthFactor;

        double lambda;

        std::vector<KernelData> kernels;

        unsigned int dimension;
    };

} // namespace mplib::math::function_approximation
