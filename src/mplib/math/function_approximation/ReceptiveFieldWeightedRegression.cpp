#include "ReceptiveFieldWeightedRegression.h"

#include "mplib/core/Exception.h"
#include "mplib/math/util/check_value.h"

namespace mplib::math::function_approximation
{
    void
    ReceptiveFieldWeightedRegression::reset()
    {
        kernels.clear();
    }

    std::unique_ptr<AbstractFunctionApproximation>
    ReceptiveFieldWeightedRegression::clone()
    {
        throw std::logic_error{"Not yet implemented!"};
    }

    ReceptiveFieldWeightedRegression::ReceptiveFieldWeightedRegression(double forgettingfactor)
    {
        lambda = forgettingfactor;
    }

    void
    ReceptiveFieldWeightedRegression::learn(const std::vector<core::DVec>& /*x*/,
                                            const std::vector<core::DVec>& /*y*/)
    {
        throw core::Exception("Not Implemented");
    }

    const Eigen::VectorXf
    ReceptiveFieldWeightedRegression::getNetInput(const core::DVec& x, int kernelId) const
    {
        core::DVec inx = x;
        inx.push_back(1);
        Eigen::VectorXf inxv = Eigen::VectorXd::Map(inx.data(), inx.size()).cast<float>();
        Eigen::VectorXf incv(inxv.rows());
        incv << kernels.at(kernelId).kernel.c, 0;
        inxv = inxv - incv;

        return inxv.cast<float>();
    }

    void
    ReceptiveFieldWeightedRegression::ilearn(const core::DVec& x, double y)
    {
        // set learning parameters
        double gama = 1;
        double alpha = 0.1;

        double wgen = 0.5;
        double wprune = 0.98;
        double wlow = 0.001;

        bool isNew = true;

        double curDetD = std::numeric_limits<double>::max();
        bool isPrune = false;
        int pruneID = 0;

        double rstart = 1;

        Eigen::VectorXf xv = Eigen::VectorXd::Map(x.data(), x.size()).cast<float>();

        //    std::cout << "kernel size: " << __kernels.size() << std::endl;
        // step: update parameters

        std::vector<int> pruneIDs;
        for (size_t i = 0; i < kernels.size(); ++i)
        {
            double weight = kernels.at(i).kernel(x);

            //        std::cout << "i: " << i << " weight: " << weight << std::endl;

            if (weight < wlow)
            {
                continue;
            }

            if (weight > wgen)
            {
                isNew = false;
            }

            if (weight > wprune)
            {
                double detD = kernels.at(i).kernel.detD();

                if (!isPrune)
                {
                    curDetD = detD;
                    pruneID = i;
                    isPrune = true;
                }
                else if (detD > curDetD)
                {
                    pruneIDs.push_back(i);
                }
                else if (detD < curDetD)
                {
                    curDetD = detD;
                    pruneIDs.push_back(pruneID);
                    pruneID = i;
                }
            }

            Eigen::MatrixXf P = kernels.at(i).P;
            Eigen::VectorXf beta = kernels.at(i).beta;
            double Wsum = kernels.at(i).Wsum;
            double Esum = kernels.at(i).Esum;
            Eigen::VectorXf H = kernels.at(i).H;
            Eigen::MatrixXf R = kernels.at(i).R;
            Eigen::MatrixXf M = kernels.at(i).kernel.M;

            Eigen::VectorXf c = kernels.at(i).kernel.c;

            Eigen::VectorXf inxv = getNetInput(x, i);

            double err = y - beta.transpose() * inxv;

            // update net weights
            //        std::cout << "err: " << err << std::endl;
            //        std::cout << "\t P: " << P << std::endl;
            //        std::cout << "\t inxv: " << inxv << std::endl;

            Eigen::VectorXf betaCan = beta + err * weight * P * inxv;

            if (betaCan.norm() < 1e10)
            {
                beta = betaCan;
            }

            //        std::cout << "\t beta: " << beta << std::endl;
            // update P
            Eigen::MatrixXf PGain = P * inxv * inxv.transpose() * P;
            double factor = lambda / weight + inxv.transpose() * P * inxv;
            P = 1 / lambda * (P - 1 / factor * PGain);

            // update Wsum
            Wsum = lambda * Wsum + weight;

            // update Esum
            Esum = lambda * Esum + weight * err * err;

            double h = weight * inxv.transpose() * P * inxv;
            double de = 1 - h;
            if (de == 0)
            {
                throw core::Exception("de is zero.");
            }
            // update H
            H = lambda * H + 1 / de * weight * err * inxv;

            // updae R
            R = lambda * R + (weight * weight * (1 / de) * err * err) * (inxv * inxv.transpose());

            // update M
            double deriJ2W;

            Eigen::VectorXf mat1 = 2 * P * inxv * (y - inxv.transpose() * beta);
            Eigen::MatrixXf mat2 = 2 * P * inxv * inxv.transpose() * P;

            //        std::cout << '\t' << "mat1: " << mat1 << std::endl;
            //        std::cout <<'\t' << "mat2: " << mat2 << std::endl;

            //        std::cout << '\t' << "err: " << err << std::endl;
            //        std::cout <<'\t' << "H: " << H << std::endl;

            deriJ2W = -Esum / (Wsum * Wsum) +
                      1 / (Wsum) *
                          (err * err - (H.cwiseProduct(mat1)).sum() - (mat2.cwiseProduct(R)).sum());

            //        std::cout <<'\t' << "deriJ2W: " << deriJ2W << std::endl;

            //        std::cout << '\t' << "pre M:  " << M << std::endl;
            Eigen::MatrixXf D = M.transpose() * M;
            for (int row = 0; row < M.rows(); ++row)
            {
                for (int col = 0; col < M.cols(); ++col)
                {
                    Eigen::MatrixXf deriD = Eigen::MatrixXf::Zero(M.rows(), M.cols());
                    deriD.row(col) = M.row(row);
                    deriD.col(col) = M.col(row);
                    deriD(col, col) = M(row, col) + M(col, row);

                    //                std::cout <<'\t' << "deriD: " << deriD << std::endl;

                    double deriW2M = -0.5 * weight * (xv - c).transpose() * deriD * (xv - c);

                    //                std::cout <<'\t' << "deriW2M: " << deriW2M << std::endl;

                    Eigen::MatrixXf deriJ2mat = deriD.cwiseProduct(D);
                    double deriJ22M = 2 * gama * deriJ2mat.sum();

                    //                std::cout << '\t' << "deriJ2mat.sum(): " << deriJ2mat.sum() << std::endl;

                    //                std::cout << '\t' << "first term: " << deriW2M * deriJ2W << std::endl;
                    //                std::cout << '\t' << "second term: " << weight/Wsum << std::endl;
                    double MCand =
                        M(row, col) - alpha * (deriW2M * deriJ2W + weight / Wsum * deriJ22M);

                    if (fabs(MCand) < 1e3)
                    {
                        M(row, col) = MCand;
                    }
                }
            }

            //        std::cout <<'\t' << "i: " << i << " M: " << M << std::endl;

            // TODO: update r

            // update all parameters
            kernels.at(i).P = P;
            kernels.at(i).beta = beta;
            kernels.at(i).Wsum = Wsum;
            kernels.at(i).Esum = Esum;
            kernels.at(i).H = H;
            kernels.at(i).R = R;
            kernels.at(i).kernel.M = M;
        }

        // Step2: add new kernels
        if (isNew)
        {
            KernelData kerData;
            int dim = xv.rows() + 1;
            Eigen::MatrixXf Md = Eigen::MatrixXf::Random(dim - 1, dim - 1);

            for (int ri = 0; ri < Md.cols(); ri++)
            {
                for (int ci = 0; ci < ri; ci++)
                {
                    Md(ri, ci) = 0;
                }

                for (int ci = ri; ci < Md.cols(); ci++)
                {
                    Md(ri, ci) = fabs(Md(ri, ci));
                }
            }

            Md = 50 * Md;

            kerData.kernel = kernel::ReceptiveFieldWeightedRegression(xv, Md);

            Eigen::MatrixXf P = Eigen::MatrixXf::Zero(dim, dim);

            Eigen::VectorXf rv(dim);
            for (int i = 0; i < dim; ++i)
            {
                rv(i) = rstart;
                P(i, i) = 1 / (rstart * rstart);
            }

            kerData.P = P;
            kerData.r = rv;
            kerData.beta = Eigen::VectorXf::Zero(dim);
            kerData.Wsum = 0;
            kerData.Esum = 0;
            kerData.H = Eigen::VectorXf::Zero(dim);
            kerData.R = Eigen::MatrixXf::Zero(dim, dim);

            kernels.push_back(kerData);

            ilearn(x, y);
        }

        //    for(size_t i = 0; i < pruneIDs.size(); i++)
        //    {
        //        __kernels.erase(__kernels.begin() + i);
        //    }
    }

    const core::DVec
    ReceptiveFieldWeightedRegression::operator()(const core::DVec& x) const
    {
        core::DVec result(1, 0.0);
        double weightedSum = 0;
        double sum = 0;

        math::util::checkValues(x);

        for (size_t i = 0; i < kernels.size(); ++i)
        {

            if (kernels.at(i).kernel.isInSupp(x)) // compact description
            {
                Eigen::VectorXf inxv = this->getNetInput(x, i);

                Eigen::VectorXf netw = kernels.at(i).beta;
                double outy = netw.dot(inxv);
                double outw = (kernels.at(i).kernel)(x);

                //            std::cout << "outw: " << outw << std::endl;
                //            std::cout << "netw: " << netw << std::endl;

                math::util::checkValue(outy);
                math::util::checkValue(outw);

                weightedSum += outw * outy;
                sum += outw;
            }
        }

        if (sum == 0)
        {
            result.at(0) = 0;
        }
        else
        {
            result.at(0) = weightedSum / sum;
        }
        return result;
    }

    double
    ReceptiveFieldWeightedRegression::getWeightValue(unsigned int /*kernelIndex*/) const
    {

        return 0;
    }

    double
    ReceptiveFieldWeightedRegression::evaluate(const std::vector<core::DVec>& /*x*/,
                                               const core::DVec& /*y*/,
                                               double* /*maxError*/,
                                               core::DVec* /*predictedValues*/)
    {
        throw core::Exception("Not Implemented");
    }

    const std::vector<ReceptiveFieldWeightedRegression::KernelData>&
    ReceptiveFieldWeightedRegression::getKernels() const
    {
        return kernels;
    }

    void
    ReceptiveFieldWeightedRegression::setKernels(
        const std::vector<ReceptiveFieldWeightedRegression::KernelData>& kernels)
    {
        if (kernels.size() != getKernelSize())
        {
            throw core::Exception(
                "The given kernels' size is different from the required kernel size.");
        }
        this->kernels = kernels;
    }

    core::DVec
    ReceptiveFieldWeightedRegression::getWeights() const
    {
        core::DVec res;

        for (size_t i = 0; i < kernels.size(); i++)
        {
            res.push_back(getWeightValue(i));
        }

        return res;
    }

    size_t
    ReceptiveFieldWeightedRegression::getKernelSize() const
    {
        return kernels.size();
    }

    unsigned int
    ReceptiveFieldWeightedRegression::dim()
    {
        return dimension;
    }

    bool
    ReceptiveFieldWeightedRegression::supportsIncrementalLearning()
    {
        return true;
    }

    double
    ReceptiveFieldWeightedRegression::calcWeight(
        const kernel::ReceptiveFieldWeightedRegression& /*kernel*/,
        const std::vector<core::DVec>& /*x*/,
        const core::DVec& /*y*/,
        double /*guessedMinimum*/)
    {
        return 0;
    }
} // namespace mplib::math::function_approximation
