#include "LocallyWeightedRegression.h"

namespace mplib::math::function_approximation
{

    double
    getLwrWeightTarget(double w, void* params)
    {
        auto* p = (GetLwrWeightTargetParams*)params;

        double resSum = 0.0;

        if (!p->basisFunction)
        {
            throw core::Exception("Basis function pointer is NULL");
        }

        unsigned int size = p->y.size();

        for (unsigned int i = 0; i < size; ++i)
        {
            double x = p->x.at(i);
            double diff = fabs(p->y[i] - w);
            double result = (*p->basisFunction)(x)*diff * diff;

            resSum += result;
        }

        return resSum;
    }

    double
    getLwrWeightGradientTarget(double w, void* params)
    {
        auto* p = (GetLwrWeightGradientTargetParams*)params;

        double resSum = 0.0;

        if (!p->basisFunction)
        {
            throw core::Exception("Basis function pointer is NULL");
        }

        unsigned int size = p->y.size();

        for (unsigned int i = 0; i < size; ++i)
        {
            double x = p->x.at(i);
            double interpolatedPosition = (p->WEIGHT + w * (x - p->CENTER));
            double diff = fabs(p->y[i] - interpolatedPosition);
            double result = pow((*p->basisFunction)(x), 1) * diff * diff;
            resSum += result;
        }

        return resSum;
    }

    double
    GetLwrWeightGradientFunctor::operator()(const double& w)
    {
        double resSum = 0.0;

        if (!p->basisFunction)
        {
            throw core::Exception("Basis function pointer is NULL");
        }

        unsigned int size = p->y.size();

        for (unsigned int i = 0; i < size; ++i)
        {
            double x = p->x.at(i);
            double interpolatedPosition = (p->WEIGHT + w * (x - p->CENTER));
            double diff = fabs(p->y[i] - interpolatedPosition);
            double result = pow((*p->basisFunction)(x), 1) * diff * diff;
            resSum += result;
        }

        return resSum;
    }

} // namespace mplib::math::function_approximation
