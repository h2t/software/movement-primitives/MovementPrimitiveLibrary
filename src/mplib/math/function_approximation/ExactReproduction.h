/**
* This file is part of MPLIB.
*
* MPLIB is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* MPLIB is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MPLIB::
* @author     H2T @ KIT
* @date       2021
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#pragma once
#include "AbstractFunctionApproximation.h"

namespace mplib::math::function_approximation
{
    class ExactReproduction : public AbstractFunctionApproximation
    {
    public:
        void learn(const std::vector<core::DVec>& x, const std::vector<core::DVec>& y) override;

        const core::DVec operator()(const core::DVec& x) const override;

        void ilearn(const core::DVec& x, double y) override;

        void createKernels(const core::DVec& xmin, const core::DVec& xmax, int kernelNum) override;

        const core::DVec getDeriv(const core::DVec& x) const override;

        void reset() override;

        std::unique_ptr<AbstractFunctionApproximation> clone() override;

    private:
        core::SampledTrajectory* data = nullptr;

        friend class boost::serialization::access;
        template <class Archive>
        void
        serialize(Archive& ar, const unsigned int /*fileVersion*/)
        {
            AbstractFunctionApproximation& base =
                boost::serialization::base_object<AbstractFunctionApproximation>(*this);
            ar& BOOST_SERIALIZATION_NVP(base);
            //            unsigned int dim = data.dim();
            //            ar & BOOST_SERIALIZATION_NVP(dim);
            //            core::DVec times = data.getTimestamps();
            //            ar & BOOST_SERIALIZATION_NVP(times);

            ar& BOOST_SERIALIZATION_NVP(data);
        }
    };
} // namespace mplib::math::function_approximation
