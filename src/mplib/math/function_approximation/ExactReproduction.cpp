#include "ExactReproduction.h"

#include <iostream>

#include "mplib/core/Exception.h"
#include "mplib/core/SampledTrajectory.h"

namespace mplib::math::function_approximation
{

    void
    ExactReproduction::learn(const std::vector<core::DVec>& x, const std::vector<core::DVec>& y)
    {
        core::DVec x1 = x.at(0);
        core::DVec y1 = y.at(0);

        data = new core::SampledTrajectory(x1, y1);
    }

    const core::DVec
    ExactReproduction::operator()(const core::DVec& x) const
    {
        if (data)
        {
            return core::DVec(1, data->getState(x.at(0), 0, 0));
        }
        return core::DVec();
    }

    void
    ExactReproduction::ilearn(const core::DVec& /*x*/, double /*y*/)
    {
        throw core::Exception("Incremental Learning is not supported or not implemented.");
    }

    void
    ExactReproduction::createKernels(const core::DVec& /*xmin*/,
                                     const core::DVec& /*xmax*/,
                                     int /*kernelNum*/)
    {
        throw core::Exception("Not Implemented");
    }

    const core::DVec
    ExactReproduction::getDeriv(const core::DVec& /*x*/) const
    {
        throw core::Exception("Not Implemented");
    }

    void
    ExactReproduction::reset()
    {
        if (data)
        {
            data->clear();
        }
    }

    std::unique_ptr<AbstractFunctionApproximation>
    ExactReproduction::clone()
    {
        return std::make_unique<ExactReproduction>(*this);
    }

} // namespace mplib::math::function_approximation
