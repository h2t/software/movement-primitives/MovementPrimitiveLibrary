#pragma once

#include <iostream>
#include <map>

#include "AbstractFunctionApproximation.h"
#include "mplib/core/Exception.h"
#include "mplib/math/kernel/GaussianRadialBasisFunction.h"
#include "mplib/math/util/check_value.h"
#include "mplib/math/util/vector_arithmetic.h"

namespace mplib::math::function_approximation
{
    template <typename Kernel = kernel::GaussianRadialBasisFunction>
    class RadialBasisFunctionInterpolator : public AbstractFunctionApproximation
    {

#define LOW_WIDTH_LIMIT 0.01
    public:
        struct KernelData
        {
            Kernel kernel;
            core::DVec center;
            core::DVec width;
            double weight;
            double P;

            friend class boost::serialization::access;
            template <class Archive>
            void
            serialize(Archive& ar, const unsigned int /*file_version*/)
            {
                ar& boost::serialization::make_nvp("center", center);
                ar& boost::serialization::make_nvp("width", width);
                ar& boost::serialization::make_nvp("weight", weight);
                ar& boost::serialization::make_nvp("kernel", kernel);
            }
        };

        RadialBasisFunctionInterpolator();

        RadialBasisFunctionInterpolator(unsigned int dim, core::DVec widthFactor = core::DVec());

        // inherited from FunctionApproximationInterface
        void learn(const std::vector<core::DVec>& x, const std::vector<core::DVec>& y) override;

        // incremental learning
        void ilearn(const core::DVec& x, double y) override;

        // create kernels
        //    void createKernels(const core::DVec &xmin, const core::DVec &xmax, int kernelNum);

        const core::DVec operator()(const core::DVec& x) const override;
        void reset() override;
        std::unique_ptr<AbstractFunctionApproximation> clone() override;

        // evaluation
        double evaluate(const std::vector<core::DVec>& x,
                        const core::DVec& y,
                        double* maxError = nullptr,
                        core::DVec* predictedValues = nullptr);

        const std::vector<KernelData>&
        getKernels() const
        {
            return kernels;
        }

        void setKernels(const std::vector<KernelData>& kernels);

        core::DVec getWeights() override;

        size_t getKernelSize() override;

        unsigned int dim();
        bool supportsIncrementalLearning() override;

    protected:
        double calcWeight(const Kernel& kernel,
                          const std::vector<core::DVec>& x,
                          const core::DVec& y,
                          double guessedMinimum = 0);
        double getWeightValue(unsigned int kernelIndex) const;

    private:
        core::DVec widthFactor;

        std::vector<KernelData> kernels;

        unsigned int dimension;

    public:
        friend class boost::serialization::access;
        template <class Archive>
        void
        serialize(Archive& ar, const unsigned int /*fileVersion*/)
        {
            AbstractFunctionApproximation& base =
                boost::serialization::base_object<AbstractFunctionApproximation>(*this);
            ar& boost::serialization::make_nvp("base", base);
            ar& boost::serialization::make_nvp("__widthFactor", widthFactor);
            ar& boost::serialization::make_nvp("__kernels", kernels);
        }
    };

    template <typename Kernel>
    RadialBasisFunctionInterpolator<Kernel>::RadialBasisFunctionInterpolator() = default;

    template <typename Kernel>
    RadialBasisFunctionInterpolator<Kernel>::RadialBasisFunctionInterpolator(unsigned int dim,
                                                                             core::DVec widthFactor)
    {
        dimension = dim;
        widthFactor.resize(dimension);

        for (size_t i = 0; i < dim; ++i)
        {
            if (i >= widthFactor.size())
            {
                widthFactor[i] = 1;
            }
            else
            {
                widthFactor[i] = widthFactor[i];
            }
        }
        lambda = 0.99;
    }

    template <typename Kernel>
    std::unique_ptr<AbstractFunctionApproximation>
    RadialBasisFunctionInterpolator<Kernel>::clone()
    {
        return std::make_unique<RadialBasisFunctionInterpolator<Kernel>>(*this);
    }

    template <typename Kernel>
    void
    RadialBasisFunctionInterpolator<Kernel>::setKernels(
        const std::vector<RadialBasisFunctionInterpolator::KernelData>& kernels)
    {
        if (kernels.size() != getKernelSize())
        {
            throw core::Exception(
                "The given kernels' size is different from the required kernel size.");
        }
        this->kernels = kernels;
    }

    template <typename Kernel>
    size_t
    RadialBasisFunctionInterpolator<Kernel>::getKernelSize()
    {
        return kernels.size();
    }

    template <typename Kernel>
    unsigned int
    RadialBasisFunctionInterpolator<Kernel>::dim()
    {
        return dimension;
    }

    template <typename Kernel>
    bool
    RadialBasisFunctionInterpolator<Kernel>::supportsIncrementalLearning()
    {
        return true;
    }

    template <typename Kernel>
    void
    RadialBasisFunctionInterpolator<Kernel>::reset()
    {
        kernels.clear();
    }

    void getXTream(std::vector<core::DVec> x, core::DVec& x0, core::DVec& xT);

    template <typename Kernel>
    void
    RadialBasisFunctionInterpolator<Kernel>::learn(const std::vector<core::DVec>& x,
                                                   const std::vector<core::DVec>& y)
    {
        if (x.size() == 0 || y.size() == 0)
        {
            return;
        }

        reset();

        const core::DVec& y1 = y.at(0);

        kernels.resize(x.size());
#pragma omp parallel for schedule(dynamic, 1)
        for (size_t i = 0; i < x.size(); ++i)
        {
            core::DVec center;
            center.resize(dimension);

            core::DVec cx = x[i];

            for (unsigned int j = 0; j < cx.size(); ++j)
            {
                center[j] = cx[j];
            }

            KernelData kernel;

            core::DVec width;

#pragma omp parallel
            for (size_t j = 0; j < center.size(); ++j)
            {
                double closestDistance = std::numeric_limits<double>::max();

                for (size_t k = 0; k < x.size(); ++k)
                {
                    if (k == i)
                    {
                        continue;
                    }

                    if (fabs(x.at(k).at(j) - cx[j]) == 0)
                    {
                        continue;
                    }

                    if (fabs(x.at(k).at(j) - cx[j]) < closestDistance)
                    {
                        closestDistance = fabs(x.at(k).at(j) - cx[j]);
                    }
                }

                double cwidth = closestDistance * widthFactor[j] * 2.5;
                width.push_back(cwidth);
            }

            kernel.kernel = Kernel(center, width);
            kernel.width = width;
            kernel.center = center;
            kernel.weight = calcWeight(kernel.kernel, x, y1);
            kernel.P = 1;

            kernels[i] = kernel;
        }

        typename std::vector<KernelData>::iterator it = kernels.begin();
        for (; it != kernels.end(); ++it)
        {
            if (it->weight == 0)
            {
                kernels.erase(it);
            }
        }
        std::cout << kernels.size() << std::endl;
    }

    template <typename Kernel>
    void
    RadialBasisFunctionInterpolator<Kernel>::ilearn(const core::DVec& x, double y)
    {

        bool createNew = true;
        for (size_t i = 0; i < kernels.size(); i++)
        {
            if (kernels.at(i).kernel.isInSupp(x))
            {
                KernelData kernel = kernels.at(i);

                // update width
                core::DVec center = kernel.center;
                double val = kernel.kernel(x);

                if (val > 0.9)
                {
                    createNew = false;
                }

                for (size_t j = 0; j < dimension; ++j)
                {
                    double dist = fabs(x.at(j) - center.at(j));

                    double closestDistance = kernel.width[j] / (2.5 * widthFactor[j]);
                    if (dist > LOW_WIDTH_LIMIT && dist < closestDistance)
                    {
                        closestDistance = dist;
                    }

                    kernel.width[j] = closestDistance * widthFactor[j] * 2.5;
                }

                kernel.kernel.m_width = kernel.width;

                double err = y - kernel.weight;
                kernel.weight += val * kernel.P * err;
                kernel.P =
                    1 / lambda * (kernel.P - (kernel.P * kernel.P) / (lambda / val + kernel.P));

                kernels.at(i) = kernel;
            }
        }

        if (createNew)
        {
            KernelData kernel;

            core::DVec width;
            core::DVec center;
            center.resize(x.size());
            width.resize(x.size());

            for (unsigned int j = 0; j < x.size(); ++j)
            {
                center[j] = x[j];
                width[j] = 100;
            }

            kernel.kernel = Kernel(center, width);
            kernel.width = width;
            kernel.center = center;
            kernel.weight = 0;
            kernel.P = 1;

            kernels.push_back(kernel);

            ilearn(x, y);
        }
    }

    template <typename Kernel>
    double
    RadialBasisFunctionInterpolator<Kernel>::calcWeight(const Kernel& kernel,
                                                        const std::vector<core::DVec>& x,
                                                        const core::DVec& y,
                                                        double /*guessedMinimum*/)
    {

        double numerator = 0;
        double denominator = 0;
        for (size_t i = 0; i < x.size(); ++i)
        {
            core::DVec u = x.at(i);
            numerator += kernel(u) * y[i];
            denominator += kernel(u);
        }

        double res;

        if (denominator != 0)
        {
            res = numerator / denominator;
        }
        else
        {
            res = 0;
        }

        return res;
    }

    template <typename Kernel>
    const core::DVec
    RadialBasisFunctionInterpolator<Kernel>::operator()(const core::DVec& x) const
    {
        core::DVec result(1, 0.0);
        double weightedSum = 0;
        double sum = 0;

        math::util::checkValues(x);

        for (size_t i = 0; i < kernels.size(); ++i)
        {

            if (kernels.at(i).kernel.isInSupp(x)) // compact description
            {
                double value = (kernels.at(i).kernel)(x);
                double weight;
                weight = getWeightValue(i);

                math::util::checkValue(value);
                math::util::checkValue(weight);

                weightedSum += value * weight;
                sum += value;
            }
        }

        if (sum == 0)
        {
            result.at(0) = 0;
        }
        else
        {
            result.at(0) = weightedSum / sum;
        }
        //checkValues(result);
        return result;
    }

    template <typename Kernel>
    double
    RadialBasisFunctionInterpolator<Kernel>::getWeightValue(unsigned int kernelIndex) const
    {

        return kernels.at(kernelIndex).weight;
    }

    template <typename Kernel>
    double
    RadialBasisFunctionInterpolator<Kernel>::evaluate(const std::vector<core::DVec>& x,
                                                      const core::DVec& y,
                                                      double* maxError,
                                                      core::DVec* predictedValues)
    {
        if (predictedValues)
        {
            predictedValues->clear();
        }

        if (maxError)
        {
            *maxError = 0;
        }

        double errorSum = 0.0;
        unsigned int size = std::min(x.size(), y.size());
        double minValue = std::numeric_limits<double>::max();
        double maxValue = std::numeric_limits<double>::min();

        for (unsigned int i = 0; i < size; i++)
        {
            core::DVec cx = x[i];
            double prediction = operator()(cx).at(0);

            if (predictedValues)
            {
                predictedValues->push_back(prediction);
            }

            double error = fabs(y[i] - prediction);

            if (maxError)
            {
                *maxError = std::max(error, *maxError);
            }

            minValue = std::min(y[i], minValue);
            maxValue = std::max(y[i], maxValue);

            errorSum += error;
        }

        double meanError = errorSum / size;
        meanError /= maxValue - minValue;

        if (maxError)
        {
            *maxError /= maxValue - minValue;
        }

        return meanError;
    }

    template <typename Kernel>
    core::DVec
    RadialBasisFunctionInterpolator<Kernel>::getWeights()
    {
        core::DVec res;

        for (size_t i = 0; i < kernels.size(); i++)
        {
            res.push_back(getWeightValue(i));
        }

        return res;
    }

} // namespace mplib::math::function_approximation
