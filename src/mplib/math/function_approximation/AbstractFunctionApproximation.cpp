#include "AbstractFunctionApproximation.h"

#include <iostream>

namespace mplib::math::function_approximation
{

    bool
    AbstractFunctionApproximation::supportsIncrementalLearning()
    {
        return false;
    }

    void
    AbstractFunctionApproximation::setForgettingFactor(double fo)
    {
        if (supportsIncrementalLearning())
        {
            lambda = fo;
        }
        else
        {
            std::cerr
                << "Notice: This function approximator does not support incremental learning. "
                << std::endl;
        }
    }

    core::DVec
    AbstractFunctionApproximation::getWeights() const
    {
        core::DVec res;
        res.push_back(0);
        return res;
    }

    size_t
    AbstractFunctionApproximation::getKernelSize() const
    {
        return 1;
    }

    void
    AbstractFunctionApproximation::setWeights(const core::DVec& /*weights*/)
    {
        // do nothing
    }

    void
    AbstractFunctionApproximation::setupKernels(double /*xmin*/, double /*xmax*/)
    {
        // do nothing
    }

    core::DVec
    AbstractFunctionApproximation::getBasisFunctionVal(const core::DVec& /*x*/)
    {
        return {};
    }

    Eigen::VectorXf
    AbstractFunctionApproximation::getBasisFunctionVec(const core::DVec& /*x*/)
    {
        return {};
    }

} // namespace mplib::math::function_approximation
