#include "RadialBasisFunctionInterpolator.h"

namespace mplib::math::function_approximation
{

    void
    getXTream(std::vector<core::DVec> x, core::DVec& x0, core::DVec& xT)
    {
        unsigned int size = x.size();
        if (size < 1)
        {
            return;
        }

        x0 = x[0];
        xT = x[0];

        for (size_t i = 0; i < size; ++i)
        {

            for (size_t j = 0; j < x0.size(); ++j)
            {
                if (x[i][j] < x0[j])
                {
                    x0[j] = x[i][j];
                }
                if (x[i][j] > xT[j])
                {
                    xT[j] = x[i][j];
                }
            }
        }
    }

} // namespace mplib::math::function_approximation
