/**
* This file is part of MPLIB.
*
* MPLIB is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* MPLIB is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MPLIB::
* @author     H2T @ KIT
* @date       2021
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#pragma once

#include <map>
#include <memory>

#include <boost/serialization/access.hpp>
#include <boost/serialization/nvp.hpp>

#include <Eigen/Core>

#include "mplib/core/types.h"

namespace mplib::math::function_approximation
{
    class AbstractFunctionApproximation
    {
    public:
        double lambda;

        virtual void learn(const std::vector<core::DVec>& x, const std::vector<core::DVec>& y) = 0;
        virtual void ilearn(const core::DVec& x, double y) = 0;
        virtual void
        createKernels(const core::DVec& xmin, const core::DVec& xmax, int kernelNum = 0) = 0;

        virtual const core::DVec operator()(const core::DVec& x) const = 0;
        virtual const core::DVec getDeriv(const core::DVec& x) const = 0;

        virtual bool supportsIncrementalLearning();

        virtual void setForgettingFactor(double fo);

        virtual void reset() = 0;

        virtual std::unique_ptr<AbstractFunctionApproximation> clone() = 0;

        virtual core::DVec getWeights() const;

        virtual size_t getKernelSize() const;

        virtual void setWeights(const core::DVec& weights);
        virtual void setupKernels(double xmin, double xmax);

        virtual core::DVec getBasisFunctionVal(const core::DVec& x);

        virtual Eigen::VectorXf getBasisFunctionVec(const core::DVec& x);

    private:
        friend class boost::serialization::access;
        template <class Archive>
        void
        serialize(Archive& ar, const unsigned int /*fileVersion*/)
        {
            ar& boost::serialization::make_nvp("lambda", lambda);
        }
    };
} // namespace mplib::math::function_approximation
