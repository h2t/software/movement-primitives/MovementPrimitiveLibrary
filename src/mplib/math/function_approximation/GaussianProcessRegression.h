#pragma once

#include <armadillo>
#include <map>

#include "AbstractFunctionApproximation.h"
#include "mplib/math/kernel/SquaredExponentialCovarianceFunction.h"
#include "mplib/math/util/check_value.h"
#include "mplib/math/util/vector_arithmetic.h"

namespace mplib::math::function_approximation
{
#define LEARN_MAX_EPOCH 1000
    template <typename Kernel = kernel::SquaredExponentialCovarianceFunction>
    class GaussianProcessRegression : public AbstractFunctionApproximation
    {

    public:
        GaussianProcessRegression();

        GaussianProcessRegression(double l,
                                  double sigmaF,
                                  double sigmaN,
                                  bool learnHyperParas = false);

        const core::DVec getDeriv(const core::DVec& x) const override;

        void learn(const std::vector<core::DVec>& x, const std::vector<core::DVec>& y) override;

        void
        ilearn(const core::DVec& /*x*/, double /*y*/) override
        {
            throw core::Exception("Incremental Learning is not supported or not implemented.");
        }

        void
        createKernels(const core::DVec& /*xmin*/,
                      const core::DVec& /*xmax*/,
                      int /*kernelNum*/) override
        {
            throw core::Exception("Not Implemented");
        }

        double evaluate(const core::DVec2d& x, const core::DVec2d& y);

        // create kernels
        const core::DVec operator()(const core::DVec& x) const override;

        unsigned int dim();

        std::unique_ptr<AbstractFunctionApproximation> clone() override;

        void setLearnRate(double lrate);

        void reset() override;

    protected:
    private:
        unsigned int outdim;
        unsigned int indim;

        Kernel kernel;
        arma::mat kXxInv;
        arma::mat Z;
        core::DVec2d X;

        bool learnHyperParas;
        double learnRate;

        template <class T>
        std::string
        saveArma(T& t) const
        {
            std::stringstream stream(std::stringstream::out | std::stringstream::binary);
            t.save(stream, arma::file_type::raw_ascii);
            return stream.str();
        }

        template <class T>
        void
        loadArma(T& t, std::string s)
        {
            std::stringstream stream(s, std::stringstream::in | std::stringstream::binary);
            t.load(stream, arma::file_type::raw_ascii);
        }

    public:
        friend class boost::serialization::access;
        template <class Archive>
        void
        serialize(Archive& ar, const unsigned int /*fileVersion*/)
        {
            ar& boost::serialization::make_nvp("indim", indim);
            ar& boost::serialization::make_nvp("outdim", outdim);
            ar& boost::serialization::make_nvp("kernel", kernel);
            ar& boost::serialization::make_nvp("X", X);
            ar& boost::serialization::make_nvp("learnHyperParas", learnHyperParas);
            ar& boost::serialization::make_nvp("learn_rate", learnRate);

            AbstractFunctionApproximation& base =
                boost::serialization::base_object<AbstractFunctionApproximation>(*this);
            ar& boost::serialization::make_nvp("base", base);

            if (Archive::is_saving::value)
            {
                std::string k_inv_str = saveArma(kXxInv);
                ar& boost::serialization::make_nvp("K_XX_inv", k_inv_str);
            }
            else
            {
                std::string k_inv_str;
                ar& boost::serialization::make_nvp("K_XX_inv", k_inv_str);
                loadArma(kXxInv, k_inv_str);
            }

            if (Archive::is_saving::value)
            {
                std::string Z_str = saveArma(Z);
                ar& boost::serialization::make_nvp("Z", Z_str);
            }
            else
            {
                std::string Z_str;
                ar& boost::serialization::make_nvp("Z", Z_str);
                loadArma(Z, Z_str);
            }
        }
    };

    template <typename Kernel>
    GaussianProcessRegression<Kernel>::GaussianProcessRegression()
    {
        kernel = Kernel();
        learnHyperParas = false;
    }

    template <typename Kernel>
    GaussianProcessRegression<Kernel>::GaussianProcessRegression(double l,
                                                                 double sigmaF,
                                                                 double sigmaN,
                                                                 bool learnHyperParas)
    {
        kernel = Kernel(l, sigmaF, sigmaN);
        this->learnHyperParas = learnHyperParas;
        learnRate = 0.1;
    }

    template <typename Kernel>
    const core::DVec
    GaussianProcessRegression<Kernel>::getDeriv(const core::DVec& x) const
    {
        //ToDo: Implement derivative of gaussian process.
        return core::DVec(x.size(), 0);
    }

    template <typename Kernel>
    unsigned int
    GaussianProcessRegression<Kernel>::dim()
    {
        return indim;
    }

    template <typename Kernel>
    std::unique_ptr<AbstractFunctionApproximation>
    GaussianProcessRegression<Kernel>::clone()
    {
        return std::make_unique<GaussianProcessRegression<Kernel>>(*this);
    }

    template <typename Kernel>
    void
    GaussianProcessRegression<Kernel>::setLearnRate(double lrate)
    {
        learnRate = lrate;
    }

    template <typename Kernel>
    void
    GaussianProcessRegression<Kernel>::reset()
    {
        kXxInv.clear();
        ;
    }

    template <typename Kernel>
    void
    GaussianProcessRegression<Kernel>::learn(const core::DVec2d& x, const core::DVec2d& y)
    {
        if (x.size() == 0 || y.size() == 0)
        {
            return;
        }

        indim = x[0].size();
        outdim = y[0].size();
        X = x;

        double data_size = x.size();

        if (!learnHyperParas ||
            !std::is_same<Kernel, kernel::SquaredExponentialCovarianceFunction>::value)
        {
            arma::mat K_XX = arma::zeros<arma::mat>(x.size(), x.size());
            arma::mat Y = arma::zeros<arma::mat>(y.size(), outdim);
            for (size_t i = 0; i < x.size(); ++i)
            {
                for (size_t j = 0; j <= i; ++j)
                {
                    K_XX(i, j) = kernel.calc(x.at(i), x.at(j));

                    if (i != j)
                    {
                        K_XX(j, i) = K_XX(i, j);
                    }
                }

                for (size_t j = 0; j < outdim; ++j)
                {
                    Y(i, j) = y.at(i).at(j);
                }
            }

            kXxInv = inv(K_XX);
            Z = kXxInv * Y;
        }
        else
        {
            arma::mat K_XX = arma::zeros<arma::mat>(x.size(), x.size());
            arma::mat Y = arma::zeros<arma::mat>(y.size(), outdim);
            arma::mat K_DerivL = arma::zeros<arma::mat>(x.size(), x.size());
            arma::mat K_DerivSigmaF = arma::zeros<arma::mat>(x.size(), x.size());
            arma::mat K_DerivSigmaN = arma::zeros<arma::mat>(x.size(), x.size());

            double likelihood;
            double old_likelihood = 0;
            double epoch = 0;
            while (true)
            {
                for (size_t i = 0; i < x.size(); ++i)
                {
                    for (size_t j = 0; j <= i; ++j)
                    {
                        K_XX(i, j) = kernel.calc(x.at(i), x.at(j));
                        core::DVec deri_res = kernel.dericalc(x.at(i), x.at(j));
                        K_DerivL(i, j) = deri_res.at(0);
                        K_DerivSigmaF(i, j) = deri_res.at(1);
                        K_DerivSigmaN(i, j) = deri_res.at(2);

                        if (i != j)
                        {
                            K_XX(j, i) = K_XX(i, j);
                            K_DerivL(j, i) = K_DerivL(i, j);
                            K_DerivSigmaF(j, i) = K_DerivSigmaF(i, j);
                            K_DerivSigmaN(j, i) = K_DerivSigmaN(i, j);
                        }
                    }

                    for (size_t j = 0; j < outdim; ++j)
                    {
                        Y(i, j) = y.at(i).at(j);
                    }
                }

                kXxInv = inv(K_XX);
                Z = kXxInv * Y;

                likelihood = 0;
                for (size_t i = 0; i < Y.n_cols; ++i)
                {
                    arma::vec yvec = Y.col(i);
                    double normK = norm(K_XX);
                    arma::mat quadterm = yvec.t() * kXxInv * yvec;
                    double val =
                        -0.5 * quadterm(0, 0) - 0.5 * log(normK) - data_size * log(2 * M_PI) / 2;
                    likelihood += val;
                }

                std::cout << "epoch: " << epoch << " likelihood: " << likelihood
                          << " l: " << kernel.l << " sigma_f: " << kernel.sigmaF
                          << " sigma_n: " << kernel.sigmaN << std::endl;
                if (fabs(likelihood - old_likelihood) < 1e-5 || epoch > LEARN_MAX_EPOCH)
                {
                    break;
                }

                double deriL = 0;
                double deriSf = 0;
                double deriSn = 0;

                for (size_t i = 0; i < Z.n_cols; ++i)
                {
                    arma::mat upMat = Z.col(i) * Z.col(i).t() - kXxInv;

                    deriL += 0.5 * trace(upMat * K_DerivL);
                    deriSf += 0.5 * trace(upMat * K_DerivSigmaF);
                    deriSn += 0.5 * trace(upMat * K_DerivSigmaN);
                }

                // gradient descent to determine learn_rate
                double alpha_l = learnRate;
                double alpha_sf = learnRate * 0.01;
                double alpha_sn = learnRate * 0.01;

                kernel.l += alpha_l * deriL;
                kernel.sigmaF += alpha_sf * deriSf;
                kernel.sigmaN += alpha_sn * deriSn;

                old_likelihood = likelihood;
                epoch++;
            }
        }
    }

    template <typename Kernel>
    const core::DVec
    GaussianProcessRegression<Kernel>::operator()(const core::DVec& x) const
    {
        math::util::checkValues(x);

        arma::mat res = arma::zeros<arma::mat>(1, outdim);
        for (size_t i = 0; i < X.size(); ++i)
        {
            double weight = kernel.calc(x, X.at(i));
            res = res + weight * Z.row(i);
        }

        core::DVec result;
        for (size_t i = 0; i < outdim; ++i)
        {
            result.push_back(res(0, i));
        }

        return result;
    }

    template <typename Kernel>
    double
    GaussianProcessRegression<Kernel>::evaluate(const core::DVec2d& x, const core::DVec2d& y)
    {
        if (x.size() == 0 || y.size() == 0)
        {
            return 0;
        }

        double toterr = 0;
        for (size_t i = 0; i < x.size(); ++i)
        {
            core::DVec cx = x[i];
            core::DVec cy = operator()(cx);
            core::DVec ty = y[i];

            double sqerr = 0;
            for (size_t j = 0; j < outdim; ++j)
            {
                sqerr += pow(ty[j] - cy[j], 2);
            }
            sqerr = sqrt(sqerr);
            toterr += sqerr;
        }

        double meanError = toterr / x.size();
        return meanError;
    }

} // namespace mplib::math::function_approximation
