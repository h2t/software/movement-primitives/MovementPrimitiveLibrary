#pragma once

#include <armadillo>
#include <map>

#include "mplib/math/kernel/GaussianRadialBasisFunction.h"
#include "mplib/math/util/check_value.h"
#include "mplib/math/util/vector_arithmetic.h"

namespace mplib::math::function_approximation
{

    template <typename Kernel = kernel::GaussianRadialBasisFunction>
    class RadialBasisFunctionNetwork
    {

#define LOW_WIDTH_LIMIT 0.01
    public:
        RadialBasisFunctionNetwork();

        // inherited from FunctionApproximationInterface
        void learn(const core::DVec2d& x, const core::DVec2d& y);
        double evaluate(const core::DVec2d& x,
                        const core::DVec2d& y,
                        double* maxError,
                        core::DVec2d* predictedValues);

        // create kernels

        const core::DVec operator()(const core::DVec& x) const;
        void reset();

        const std::vector<Kernel>&
        getKernels() const
        {
            return kernels;
        }

        arma::mat getWeightMat();

        size_t getKernelSize();

        unsigned int dim();

        void setWidthFactor(core::DVec widthFactor);

    protected:
    private:
        core::DVec widthFactor;
        std::vector<Kernel> kernels;
        unsigned int outdim;
        unsigned int indim;

        double errtol;
        arma::mat wMat;

    public:
        friend class boost::serialization::access;
        template <class Archive>
        void
        serialize(Archive& ar, const unsigned int /*file_version*/)
        {
            ar& boost::serialization::make_nvp("__widthFactor", widthFactor);
            ar& boost::serialization::make_nvp("__kernels", kernels);
            ar& boost::serialization::make_nvp("indim", indim);
            ar& boost::serialization::make_nvp("outdim", outdim);
        }
    };

    template <typename Kernel>
    void
    RadialBasisFunctionNetwork<Kernel>::reset()
    {
        kernels.clear();
    }

    template <typename Kernel>
    void
    RadialBasisFunctionNetwork<Kernel>::learn(const core::DVec2d& x, const core::DVec2d& y)
    {
        if (x.size() == 0 || y.size() == 0)
        {
            return;
        }

        double data_size = x.size();
        outdim = y[0].size();
        indim = x[0].size();
        while (widthFactor.size() < indim)
        {
            std::cout << widthFactor.size() << ", " << indim << std::endl;
            widthFactor.push_back(1);
        }

        reset();

        // initialize kernel
        {
            const core::DVec& center = x.at(0);
            const core::DVec& cx = x.at(0);
            core::DVec width;
            //        #pragma omp parallel
            for (size_t j = 0; j < center.size(); ++j)
            {
                double closestDistance = std::numeric_limits<double>::max();

                for (size_t k = 0; k < x.size(); ++k)
                {
                    if (k == 0)
                    {
                        continue;
                    }

                    if (fabs(x.at(k).at(j) - cx.at(j)) == 0)
                    {
                        continue;
                    }

                    if (fabs(x.at(k).at(j) - cx.at(j)) < closestDistance)
                    {
                        closestDistance = fabs(x.at(k).at(j) - cx.at(j));
                    }
                }

                double cwidth = closestDistance * widthFactor.at(j) * 2.5;
                width.push_back(cwidth);
            }

            kernels.push_back(Kernel(center, width));
        }

        double toterr = 1000;
        while (toterr > errtol)
        {
            // calc weight matrix
            arma::mat fX = arma::zeros<arma::mat>(data_size, kernels.size());
            arma::mat fY = arma::zeros<arma::mat>(data_size, outdim);

            for (size_t i = 0; i < data_size; ++i)
            {

                for (size_t j = 0; j < kernels.size(); ++j)
                {
                    double kval = kernels.at(j)(x.at(i));

                    fX(i, j) = kval;
                }

                for (size_t j = 0; j < outdim; ++j)
                {
                    fY(i, j) = y.at(i).at(j);
                }
            }

            wMat = pinv(fX) * fY; // __kernels.size() x outdim
                                  //        std::cout << wMat << std::endl;
            // calc errors
            toterr = 0;
            double maxError = 0;
            int maxErrorId = 0;
            for (size_t i = 0; i < data_size; ++i)
            {
                core::DVec cy = operator()(x.at(i));
                core::DVec ty = y.at(i);

                double sqerr = 0;
                for (size_t j = 0; j < outdim; ++j)
                {
                    sqerr += pow(ty.at(j) - cy.at(j), 2);
                }
                sqerr = sqrt(sqerr);

                if (sqerr > maxError)
                {
                    maxErrorId = i;
                    maxError = sqerr;
                }

                toterr += sqerr;
            }
            toterr /= data_size;

            // add kernel
            if (toterr <= errtol || kernels.size() == data_size)
            {
                break;
            }

            const core::DVec& center = x.at(maxErrorId);
            const core::DVec& cx = x.at(maxErrorId);
            core::DVec width;

            for (size_t j = 0; j < center.size(); ++j)
            {
                double closestDistance = std::numeric_limits<double>::max();

                for (size_t k = 0; k < x.size(); ++k)
                {
                    if (k == maxErrorId)
                    {
                        continue;
                    }

                    if (fabs(x.at(k).at(j) - cx.at(j)) == 0)
                    {
                        continue;
                    }

                    if (fabs(x.at(k).at(j) - cx.at(j)) < closestDistance)
                    {
                        closestDistance = fabs(x.at(k).at(j) - cx.at(j));
                    }
                }

                double cwidth = closestDistance * widthFactor.at(j) * 2.5;
                width.push_back(cwidth);
            }

            kernels.push_back(Kernel(center, width));
        }

        std::cout << "Kernel size is: " << getKernelSize() << std::endl;
    }

    template <typename Kernel>
    const core::DVec
    RadialBasisFunctionNetwork<Kernel>::operator()(const core::DVec& x) const
    {
        math::util::checkValues(x);

        arma::mat fX = arma::zeros<arma::mat>(1, kernels.size());

        for (size_t j = 0; j < kernels.size(); ++j)
        {
            double kval = kernels.at(j)(x);

            fX(0, j) = kval;
        }

        arma::mat fY = fX * wMat; // 1 x outdim

        core::DVec result;
        for (size_t i = 0; i < fY.n_cols; ++i)
        {
            result.push_back(fY(0, i));
        }

        return result;
    }

    template <typename Kernel>
    double
    RadialBasisFunctionNetwork<Kernel>::evaluate(const core::DVec2d& x,
                                                 const core::DVec2d& y,
                                                 double* /*maxError*/,
                                                 core::DVec2d* /*predictedValues*/)
    {
        if (x.size() == 0 || y.size() == 0)
        {
            return 0;
        }

        double toterr = 0;
        for (size_t i = 0; i < x.size(); ++i)
        {
            core::DVec cx = x[i];
            core::DVec cy = operator()(cx);
            core::DVec ty = y[i];

            double sqerr = 0;
            for (size_t j = 0; j < outdim; ++j)
            {
                sqerr += pow(ty[j] - cy[j], 2);
            }
            sqerr = sqrt(sqerr);
            toterr += sqerr;
        }

        double meanError = toterr / x.size();
        return meanError;
    }

    template <typename Kernel>
    RadialBasisFunctionNetwork<Kernel>::RadialBasisFunctionNetwork()
    {
        errtol = 1e-5;
    }

    template <typename Kernel>
    size_t
    RadialBasisFunctionNetwork<Kernel>::getKernelSize()
    {
        return kernels.size();
    }

    template <typename Kernel>
    unsigned int
    RadialBasisFunctionNetwork<Kernel>::dim()
    {
        return indim;
    }

    template <typename Kernel>
    void
    RadialBasisFunctionNetwork<Kernel>::setWidthFactor(core::DVec widthFactor)
    {
        widthFactor = widthFactor;
    }

} // namespace mplib::math::function_approximation
