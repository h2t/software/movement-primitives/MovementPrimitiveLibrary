#pragma once

#include <iostream>
#include <map>

#include <boost/math/tools/minima.hpp>

#include "AbstractFunctionApproximation.h"
#include "mplib/core/Exception.h"
#include "mplib/core/SampledTrajectory.h"
#include "mplib/math/kernel/AbstractFunction.h"
#include "mplib/math/kernel/GaussianFunction.h"
#include "mplib/math/util/check_value.h"
#include "mplib/math/util/vector_arithmetic.h"

namespace mplib::math::function_approximation
{

    template <typename Kernel = kernel::GaussianFunction>
    class LocallyWeightedRegression : public AbstractFunctionApproximation
    {
    public:
        struct KernelData
        {
            Kernel kernel;
            double center{0};
            double width{0};
            double weight{0};
            double weightGradient{0};
            double P{0};

            friend class boost::serialization::access;
            template <class Archive>
            void
            serialize(Archive& ar, const unsigned int /*file_version*/)
            {
                ar& BOOST_SERIALIZATION_NVP(center);
                ar& BOOST_SERIALIZATION_NVP(width);
                ar& BOOST_SERIALIZATION_NVP(weight);
                ar& BOOST_SERIALIZATION_NVP(weightGradient);
                ar& BOOST_SERIALIZATION_NVP(kernel);
                ar& BOOST_SERIALIZATION_NVP(P);
            }
        };

        LocallyWeightedRegression(int baseKernelCount = 5,
                                  double widthFactor = 1,
                                  bool useWeightGradients = false);

        // inherited from FunctionApproximationInterface
        void learn(const std::vector<core::DVec>& x, const std::vector<core::DVec>& y) override;
        void ilearn(const core::DVec& x, double y) override;

        bool supportsIncrementalLearning() override;

        const core::DVec operator()(const core::DVec& x) const override;
        const core::DVec getDeriv(const core::DVec& x) const override;
        void reset() override;
        std::unique_ptr<AbstractFunctionApproximation> clone() override;

        // evaluation
        double evaluate(const core::DVec& x,
                        const core::DVec& y,
                        double* maxError = nullptr,
                        core::DVec* predictedValues = nullptr);

        // getters
        const std::vector<KernelData>&
        getKernels() const
        {
            return kernels;
        }

        void setKernels(const std::vector<KernelData>& kernels);

        // setters
        void setWeightGradients(bool activated);
        void setConvertToZeroOutOfBoundaries(bool activated);

        core::DVec getWeights() const override;

        void setWeights(const core::DVec& weights) override;
        void setupKernels(double xmin, double xmax) override;
        size_t getKernelSize() const override;

        void
        createKernels(const core::DVec& xminv, const core::DVec& xmaxv, int kernelNum = 0) override;

        core::DVec getBasisFunctionVal(const core::DVec& x) override;
        Eigen::VectorXf getBasisFunctionVec(const core::DVec& x) override;

    protected:
        double calcWeight(const Kernel& kernel, const core::DVec& x, const core::DVec& y);
        double calcWeightGradient(const Kernel& kernel,
                                  double center,
                                  double weight,
                                  const core::DVec& x,
                                  const core::DVec& y);
        double getWeightValue(unsigned int kernelIndex, double offset) const;

    private:
        double widthFactor;

        bool useWeightGradients;
        bool convertToZeroOutOfBoundaries{true};
        int baseKernelCount;
        std::vector<KernelData> kernels;

        // FunctionApproximationInterface interface
    public:
        friend class boost::serialization::access;
        template <class Archive>
        void
        serialize(Archive& ar, const unsigned int /*fileVersion*/)
        {
            AbstractFunctionApproximation& base =
                boost::serialization::base_object<AbstractFunctionApproximation>(*this);
            ar& BOOST_SERIALIZATION_NVP(base);

            ar& BOOST_SERIALIZATION_NVP(widthFactor);
            ar& BOOST_SERIALIZATION_NVP(useWeightGradients);
            ar& BOOST_SERIALIZATION_NVP(convertToZeroOutOfBoundaries);
            ar& BOOST_SERIALIZATION_NVP(baseKernelCount);
            std::vector<KernelData>& kernels = this->kernels;
            ar& BOOST_SERIALIZATION_NVP(kernels);
        }
    };

    class GetLwrWeightTargetParams
    {
    public:
        const kernel::AbstractFunction* basisFunction;
        const core::DVec& x;
        const core::DVec& y;

        GetLwrWeightTargetParams(const kernel::AbstractFunction* f,
                                 const core::DVec& x,
                                 const core::DVec& y) :
            basisFunction(f), x(x), y(y)
        {
        }
    };

    double getLwrWeightTarget(double w, void* params);

    template <typename T>
    int
    sgn(T val)
    {
        return (T(0) < val) - (val < T(0));
    }

    template <typename Kernel>
    void
    LocallyWeightedRegression<Kernel>::learn(const std::vector<core::DVec>& x,
                                             const std::vector<core::DVec>& y)
    {
        // only works for one-dimensional data
        if (x.size() == 0 || y.size() == 0)
        {
            return;
        }

        if (x[0].size() != 1 || y[0].size() != 1)
        {
            throw core::Exception() << "LWR only works for one-dimensional data";
        }

        core::DVec x1;
        core::DVec y1;
        for (size_t i = 0; i < x.size(); ++i)
        {
            for (size_t j = 0; j < x[i].size(); ++j)
            {
                x1.push_back(x[i].at(j));
                y1.push_back(y[i].at(j));
            }
        }

        core::SampledTrajectory traj(x1, y1);
        traj.differentiateDiscretly(2);

        core::SampledTrajectory gaussFiltered(traj);
        gaussFiltered.gaussianFilter(
            0.02 * (*traj.getTimestamps().rbegin() - *traj.getTimestamps().begin()));

        core::DVec xminv;
        core::DVec xmaxv;
        xminv.push_back(*x1.begin());
        xmaxv.push_back(*x1.rbegin());
        createKernels(xminv, xmaxv, baseKernelCount);

        for (int i = 0; i < baseKernelCount; i++)
        {
            kernels.at(i).weight = calcWeight(kernels.at(i).kernel, x1, y1);
            if (useWeightGradients)
            {
                kernels.at(i).weightGradient = gaussFiltered.getState(kernels.at(i).center, 0);
            }
            else
            {
                kernels.at(i).weightGradient = 0;
            }
        }
    }

    template <typename Kernel>
    void
    LocallyWeightedRegression<Kernel>::createKernels(const core::DVec& xminv,
                                                     const core::DVec& xmaxv,
                                                     int kernelNum)
    {
        reset();

        double xmin = xminv.at(0) - 0.1;
        double xmax = xmaxv.at(0) + 0.1;
        double kernelDistance, width;

        double center;

        kernelDistance = (xmax - xmin) / (kernelNum - 1);
        width = kernelDistance * widthFactor * 2.5;
        center = xmin;

        kernels.resize(kernelNum);

        for (int i = 0; i < kernelNum; i++)
        {
            KernelData kernel;
            kernel.kernel = Kernel(center, width);
            kernel.width = width;
            kernel.center = center;
            kernel.weight = 0;
            kernel.P = 1;

            kernels.at(i) = (kernel);
            center += kernelDistance;
        }
    }

    template <typename Kernel>
    void
    LocallyWeightedRegression<Kernel>::ilearn(const core::DVec& x, double y)
    {
        double x1 = x.at(0);
        for (size_t i = 0; i < kernels.size(); i++)
        {
            if (kernels.at(i).kernel.isInSupp(x1))
            {
                KernelData kernel = kernels.at(i);
                double val = kernel.kernel(x1);
                kernel.P =
                    1 / lambda * (kernel.P - (kernel.P * kernel.P) / (lambda / val + kernel.P));
                kernel.weight += val * kernel.P * (y - kernel.weight);
                kernels.at(i) = kernel;
            }
        }
    }

    template <typename Kernel>
    const core::DVec
    LocallyWeightedRegression<Kernel>::operator()(const core::DVec& x) const
    {
        core::DVec result(1, 0.0);
        double weightedSum = 0;
        double sum = 0;
        double x1 = x.at(0);

        if (std::isnan(x1))
        {
            throw core::Exception("Input is NaN");
        }

        for (int i = kernels.size() - 1; i >= 0; i--)
        {
            double weight = getWeightValue(i, x1 - kernels.at(i).center);

            if (kernels.at(i).kernel.isInSupp(x1)) // compact description
            {
                double value = (kernels.at(i).kernel)(x1);

                math::util::checkValue(value);
                math::util::checkValue(weight);

                weightedSum += value * weight;
                sum += value;
            }
            else
            {
                double value = 1e-7;
                weightedSum += value * weight;
                sum += value;
            }
        }

        if (sum == 0)
        {
            throw core::Exception("sum is zero");
        }

        double factor = 1;

        if (convertToZeroOutOfBoundaries && kernels.size() > 0 && x1 > kernels.rbegin()->center)
        {
            factor = (kernels.rbegin()->kernel)(x1);
        }
        else if (convertToZeroOutOfBoundaries && kernels.size() > 0 && x1 < kernels.begin()->center)
        {
            factor = (kernels.begin()->kernel)(x1);
        }

        result.at(0) = weightedSum / sum * factor;

        math::util::checkValue(result);
        return result;
    }

    template <typename Kernel>
    const core::DVec
    LocallyWeightedRegression<Kernel>::getDeriv(const core::DVec& x) const
    {
        core::DVec result(1, 0.0);
        double weightedSum = 0;
        double sum = 0;
        double weightedDeriv = 0;
        double deriv = 0;
        double x1 = x.at(0);

        if (std::isnan(x1))
        {
            throw core::Exception("Input is NaN");
        }

        for (int i = kernels.size() - 1; i >= 0; i--)
        {
            double weight = getWeightValue(i, x1 - kernels.at(i).center);
            if (kernels.at(i).kernel.isInSupp(x1)) // compact description
            {
                double value = (kernels.at(i).kernel)(x1);
                double derivValue = (kernels.at(i).kernel).getDeriv(x1);

                math::util::checkValue(value);
                math::util::checkValue(derivValue);
                math::util::checkValue(weight);

                weightedSum += value * weight;
                sum += value;

                weightedDeriv += derivValue * weight;
                deriv += derivValue;
            }
            else
            {
                double value = 1e-7;
                weightedSum += value * weight;
                sum += value;

                double derivValue = 0;
                weightedDeriv += derivValue * weight;
                deriv += derivValue;
            }
        }

        if (sum == 0)
        {
            throw core::Exception("sum is zero");
        }

        result.at(0) = ((weightedDeriv * sum) - (weightedSum * deriv)) / pow(sum, 2);
        math::util::checkValue(result);

        return result;
    }

    template <typename Kernel>
    core::DVec
    LocallyWeightedRegression<Kernel>::getBasisFunctionVal(const core::DVec& x)
    {
        core::DVec res;
        for (size_t i = 0; i < kernels.size(); ++i)
        {
            double val = kernels.at(i).kernel(x[0]);
            res.push_back(val);
        }
        return res;
    }

    template <typename Kernel>
    Eigen::VectorXf
    LocallyWeightedRegression<Kernel>::getBasisFunctionVec(const core::DVec& x)
    {
        Eigen::VectorXf res;
        res.resize(kernels.size());

        for (int i = 0; i < res.rows(); ++i)
        {
            res[i] = kernels.at(i).kernel(x[0]);
        }

        return res;
    }

    template <typename Kernel>
    void
    LocallyWeightedRegression<Kernel>::reset()
    {
        kernels.clear();
    }

    template <typename Kernel>
    double
    LocallyWeightedRegression<Kernel>::calcWeight(const Kernel& kernel,
                                                  const core::DVec& x,
                                                  const core::DVec& y)
    {
        double numerator = 0;
        double denominator = 0;
        for (size_t i = 0; i < x.size(); ++i)
        {
            double u = x.at(i);
            numerator += kernel(u) * y[i];
            denominator += kernel(u);
        }

        double res;

        if (denominator != 0)
        {
            res = numerator / denominator;
        }
        else
        {
            res = 0;
        }

        return res;
    }

    class GetLwrWeightGradientTargetParams
    {
    public:
        const kernel::AbstractFunction* basisFunction;
        const double CENTER;
        const double WEIGHT;
        const core::DVec& x;
        const core::DVec& y;

        GetLwrWeightGradientTargetParams(const kernel::AbstractFunction* f,
                                         double center,
                                         double weight,
                                         const core::DVec& x,
                                         const core::DVec& y) :
            basisFunction(f), CENTER(center), WEIGHT(weight), x(x), y(y)
        {
        }
    };

    struct GetLwrWeightGradientFunctor
    {
        GetLwrWeightGradientFunctor(GetLwrWeightGradientTargetParams* p) : p(p)
        {
        }

        double operator()(double const& w);

    private:
        GetLwrWeightGradientTargetParams* p;
    };

    double getLwrWeightGradientTarget(double w, void* params);

    //! Method is untested after changing to boost
    template <typename Kernel>
    double
    LocallyWeightedRegression<Kernel>::calcWeightGradient(const Kernel& kernel,
                                                          double center,
                                                          double weight,
                                                          const core::DVec& x,
                                                          const core::DVec& y)
    {
        double a = -500000.0, b = 500000.0;
        auto params = new GetLwrWeightGradientTargetParams(&kernel, center, weight, x, y);

        boost::uintmax_t max_iter = 200;
        boost::uintmax_t it = max_iter;
        auto r =
            boost::math::tools::brent_find_minima(GetLwrWeightGradientFunctor(params), a, b, 5, it);
        if (it >= max_iter)
        {
            std::cout
                << "Unable to locate solution in chosen iterations: Current best guess is between "
                << r.first << " and " << r.second << std::endl;
        }
        delete params;

        return r.first + (r.second - r.first) / 2;
    }

    template <typename Kernel>
    double
    LocallyWeightedRegression<Kernel>::getWeightValue(unsigned int kernelIndex, double offset) const
    {
        return kernels.at(kernelIndex).weight + offset * kernels.at(kernelIndex).weightGradient;
    }

    template <typename Kernel>
    double
    LocallyWeightedRegression<Kernel>::evaluate(const core::DVec& x,
                                                const core::DVec& y,
                                                double* maxError,
                                                core::DVec* predictedValues)
    {
        if (predictedValues)
        {
            predictedValues->clear();
        }

        if (maxError)
        {
            *maxError = 0;
        }

        double errorSum = 0.0;
        unsigned int size = std::min(x.size(), y.size());
        double minValue = std::numeric_limits<double>::max();
        double maxValue = std::numeric_limits<double>::min();

        for (unsigned int i = 0; i < size; i++)
        {
            double prediction = operator()(x[i]).at(0);

            if (predictedValues)
            {
                predictedValues->push_back(prediction);
            }

            double error = fabs(y[i] - prediction);

            if (maxError)
            {
                *maxError = std::max(error, *maxError);
            }

            minValue = std::min(y[i], minValue);
            maxValue = std::max(y[i], maxValue);

            errorSum += error;
            //        std::cout << "value at " << x[i] << ": " << y[i] << " prediction: " << prediction << " error: <<  " << error << std::endl;
        }

        double meanError = errorSum / size;
        meanError /= maxValue - minValue;

        if (maxError)
        {
            *maxError /= maxValue - minValue;
        }

        return meanError;
    }

    template <typename Kernel>
    core::DVec
    LocallyWeightedRegression<Kernel>::getWeights() const
    {
        core::DVec res;

        for (size_t i = 0; i < kernels.size(); i++)
        {
            res.push_back(getWeightValue(i, 0));
        }

        return res;
    }

    template <typename Kernel>
    void
    LocallyWeightedRegression<Kernel>::setWeights(const core::DVec& weights)
    {
        if (weights.size() != kernels.size())
        {
            std::cout << "weights dim: " << weights.size() << " kernels size: " << kernels.size()
                      << std::endl;
            throw core::Exception("setWeights: the given weights vector has different length from "
                                  "the number of kernels");
        }

        for (size_t i = 0; i < kernels.size(); ++i)
        {
            kernels[i].weight = weights[i];
        }
    }

    template <typename Kernel>
    void
    LocallyWeightedRegression<Kernel>::setupKernels(double xmin, double xmax)
    {
        core::DVec xminv;
        core::DVec xmaxv;
        xminv.push_back(xmin);
        xmaxv.push_back(xmax);
        createKernels(xminv, xmaxv, baseKernelCount);
    }

    template <typename Kernel>
    std::ostream&
    operator<<(std::ostream& out, const LocallyWeightedRegression<Kernel>& lwr)
    {
        out << "Kernel count: " << lwr.getKernels().size() << std::endl;
        out << "Kernel width: " << lwr.getKernelWidth() << std::endl;
        out << "Kernels:" << std::endl;

        for (unsigned int i = 0; i < lwr.getKernels().size(); i++)
        {
            out << "center: " << lwr.getKernels()[i].center
                << " width: " << lwr.getKernels()[i].kernel.m_width
                << " weight: " << lwr.getKernels().at(i).weight
                << " weight gradient: " << lwr.getKernels().at(i).weightGradient << std::endl;
        }

        return out;
    }

    template <typename Kernel>
    LocallyWeightedRegression<Kernel>::LocallyWeightedRegression(int baseKernelCount,
                                                                 double widthFactor,
                                                                 bool useWeightGradients) :
        widthFactor(widthFactor),
        useWeightGradients(useWeightGradients),
        baseKernelCount(baseKernelCount)
    {
        kernels.resize(baseKernelCount);
        lambda = 0.99;
    }

    template <typename Kernel>
    bool
    LocallyWeightedRegression<Kernel>::supportsIncrementalLearning()
    {
        return true;
    }

    template <typename Kernel>
    std::unique_ptr<AbstractFunctionApproximation>
    LocallyWeightedRegression<Kernel>::clone()
    {
        return std::make_unique<LocallyWeightedRegression<Kernel>>(*this);
    }

    template <typename Kernel>
    void
    LocallyWeightedRegression<Kernel>::setKernels(
        const std::vector<LocallyWeightedRegression::KernelData>& kernels)
    {
        this->kernels = kernels;
        baseKernelCount = kernels.size();
    }

    template <typename Kernel>
    void
    LocallyWeightedRegression<Kernel>::setWeightGradients(bool activated)
    {
        useWeightGradients = activated;
    }

    template <typename Kernel>
    void
    LocallyWeightedRegression<Kernel>::setConvertToZeroOutOfBoundaries(bool activated)
    {
        convertToZeroOutOfBoundaries = activated;
    }

    template <typename Kernel>
    size_t
    LocallyWeightedRegression<Kernel>::getKernelSize() const
    {
        return baseKernelCount;
    }

} // namespace mplib::math::function_approximation
