#pragma once

#include "mplib/core/types.h"
#include "mplib/core/Exception.h"

#include <eigen3/Eigen/Geometry>

namespace mplib::math::util
{
    extern Eigen::Quaterniond REFERENCE_UNIT_QUATERNION;

    Eigen::Quaterniond angle2quat(double yaw, double pitch, double roll);

    double* threeaxisrot(double r11, double r12, double r21, double r31, double r32);

    Eigen::Vector3d quat2angle(Eigen::Quaterniond q);

    template <typename T>
    void errorPose(const std::vector<T> vec)
    {
        if (vec.size() != 7)
            throw core::Exception("ERROR! Vector should have 7 dimensions to convert to pose");
    }

    template <typename T>
    void flip(Eigen::Quaternion<T> &orientation)
    {
        orientation.coeffs() = -orientation.coeffs();
    }

    template <typename T>
    bool check(const Eigen::Quaternion<T> &quat, const Eigen::Quaternion<T> &reference) {
        return quat.dot(reference) >= 0.0;
    }

    template <typename T>
    Eigen::Quaternion<T> toQuat(const Eigen::Matrix<T, 4, 4> &pose) {
        Eigen::Quaternion<T> quat = Eigen::Quaternion<T>{ pose.template topLeftCorner<3, 3>() };
        quat.normalize();
        return quat;
    }

    template <typename T>
    Eigen::Quaternion<T> toQuat(const core::DVec &dvec) {
        T qw = dvec[3];
        T qx = dvec[4];
        T qy = dvec[5];
        T qz = dvec[6];
        return Eigen::Quaternion<T>(qw,qx,qy,qz).normalized();
    }

    template <typename T>
    Eigen::Quaternion<T> toQuat(const core::DVec2d &dvec, uint i = 0) {
        T qw = dvec[3][i];
        T qx = dvec[4][i];
        T qy = dvec[5][i];
        T qz = dvec[6][i];
        return Eigen::Quaternion<T>(qw,qx,qy,qz).normalized();
    }

    template <typename T>
    Eigen::Matrix<T, 3, 1> toPosition(const Eigen::Matrix<T, 4, 4> &pose) {
        return pose.block(0,3,3,1);
    }

    template <typename T>
    mplib::core::DVec toDVec(const Eigen::Matrix<T, 3, 1> &position,
                             const Eigen::Quaternion<T> &orientation)
    {
        mplib::core::DVec dvec;
        for(int j = 0; j < 3; ++j)
            dvec.push_back(position(j));
        dvec.push_back(orientation.w());
        dvec.push_back(orientation.x());
        dvec.push_back(orientation.y());
        dvec.push_back(orientation.z());
        return dvec;
    }

    template <typename T>
    mplib::core::DVec toDVec(const Eigen::Matrix<T, 4, 4> &pose)
    {
        return toDVec(toPosition(pose), toQuat(pose));
    }

    template <typename T>
    mplib::core::DVec toDVec(const Eigen::Matrix<T, 4, 4> &pose, const Eigen::Quaternion<T> &reference) {
        Eigen::Quaternion<T> quat = toQuat(pose);
        if (!check(quat, reference))
            flip(quat);
        return toDVec(toPosition(pose), quat);
    }

    template <typename T>
    Eigen::Matrix<T, 3, 1> toPosition(const mplib::core::DVec &dvec) {
        return Eigen::Matrix<T, 3, 1>(dvec[0], dvec[1], dvec[2]);
    }

    template <typename T>
    Eigen::Matrix<T, 4, 4> toPose(const mplib::core::DVec &dvec) {
        errorPose(dvec);

        Eigen::Matrix<T, 4, 4> pose = Eigen::Matrix<T, 4, 4>::Identity();
        pose.block(0,3,3,1) = toPosition<T>(dvec);
        pose.block(0,0,3,3) = toQuat<T>(dvec).toRotationMatrix();
        return pose;
    }

    template <typename T>
    Eigen::Matrix<T, 4, 4> toPose(const mplib::core::DVec2d &dvec, uint i = 0) {
        errorPose(dvec);

        Eigen::Matrix<T, 4, 4> pose = Eigen::Matrix<T, 4, 4>::Identity();
        pose.block(0,3,3,1) = Eigen::Matrix<T, 3, 1>(dvec[0][i], dvec[1][i], dvec[2][i]);
        pose.block(0,0,3,3) = toQuat<T>(dvec).toRotationMatrix();
        return pose;
    }

    template <typename T>
    void changeOrientation(mplib::core::DVec2d &dvec, const Eigen::Quaternion<T> &orientation, uint i = 0)
    {
        errorPose(dvec);

        dvec[3][i] = orientation.w();
        dvec[4][i] = orientation.x();
        dvec[5][i] = orientation.y();
        dvec[6][i] = orientation.z();
    }

    template <typename T>
    void changeOrientation(mplib::core::DVec &dvec, const Eigen::Quaternion<T> &orientation)
    {
        errorPose(dvec);

        dvec[3] = orientation.w();
        dvec[4] = orientation.x();
        dvec[5] = orientation.y();
        dvec[6] = orientation.z();
    }

    template <typename T>
    bool flip(std::vector<T> state, const Eigen::Quaterniond &reference)
    {
        Eigen::Quaterniond orientation = math::util::toQuat(math::util::toPose<double>(state));
        if (math::util::check(orientation, reference))
            return false;
        else
        {
            flip(orientation);
            math::util::changeOrientation(state, orientation);
            return true;
        }
    }

    std::map<double, core::DVec> toDVec(const std::map<double, Eigen::Matrix4d>& data, bool useReferenceQuaternion = true);
} // namespace mplib::math::util
