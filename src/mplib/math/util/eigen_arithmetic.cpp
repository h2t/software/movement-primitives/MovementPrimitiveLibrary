#include "eigen_arithmetic.h"

Eigen::Quaterniond mplib::math::util::REFERENCE_UNIT_QUATERNION = Eigen::Quaterniond(0, 0, 0, 1).normalized(); // 180° rotation around z axis

namespace mplib::math::util
{
    Eigen::Quaterniond
    angle2quat(double yaw, double pitch, double roll)
    {
        double cang[3], sang[3];
        cang[0] = cos(yaw / 2);
        cang[1] = cos(pitch / 2);
        cang[2] = cos(roll / 2);

        sang[0] = sin(yaw / 2);
        sang[1] = sin(pitch / 2);
        sang[2] = sin(roll / 2);

        Eigen::Quaterniond q;
        q.w() = cang[0] * cang[1] * cang[2] + sang[0] * sang[1] * sang[2];
        q.x() = cang[0] * cang[1] * sang[2] - sang[0] * sang[1] * cang[2];
        q.y() = cang[0] * sang[1] * cang[2] + sang[0] * cang[1] * sang[2];
        q.z() = sang[0] * cang[1] * cang[2] - cang[0] * sang[1] * sang[2];
        q.normalize();
        return q;
    }

    double*
    threeaxisrot(double r11, double r12, double r21, double r31, double r32)
    {
        auto* res = new double[3];
        res[0] = atan2(r11, r12);
        res[1] = asin(r21);
        res[2] = atan2(r31, r32);
        return res;
    }

    Eigen::Vector3d
    quat2angle(Eigen::Quaterniond q)
    {
        q.normalize();

        double qin[4];
        qin[0] = q.w();
        qin[1] = q.x();
        qin[2] = q.y();
        qin[3] = q.z();

        double r11, r12, r21, r31, r32;
        double* re;
        r11 = 2 * (qin[1] * qin[2] + qin[0] * qin[3]);
        r12 = qin[0] * qin[0] + qin[1] * qin[1] - qin[2] * qin[2] - qin[3] * qin[3];
        r21 = -2 * (qin[1] * qin[3] - qin[0] * qin[2]);
        r31 = 2 * (qin[2] * qin[3] + qin[0] * qin[1]);
        r32 = qin[0] * qin[0] - qin[1] * qin[1] - qin[2] * qin[2] + qin[3] * qin[3];
        re = threeaxisrot(r11, r12, r21, r31, r32);
        Eigen::Vector3d res;
        res << re[0], re[1], re[2];
        return res;
    }

    Eigen::Quaterniond
    operator+(Eigen::Quaterniond op1, Eigen::Quaterniond op2)
    {
        return Eigen::Quaterniond(
            op1.w() + op2.w(), op1.x() + op2.x(), op1.y() + op2.y(), op1.z() + op2.z());
    }

    Eigen::Quaterniond
    operator-(Eigen::Quaterniond op1, Eigen::Quaterniond op2)
    {
        return Eigen::Quaterniond(
            op1.w() - op2.w(), op1.x() - op2.x(), op1.y() - op2.y(), op1.z() - op2.z());
    }

    Eigen::Quaterniond
    operator*(double op1, Eigen::Quaterniond op2)
    {
        return Eigen::Quaterniond(op1 * op2.w(), op1 * op2.x(), op1 * op2.y(), op1 * op2.z());
    }

    Eigen::Quaterniond
    operator*(Eigen::Quaterniond op2, double op1)
    {
        return op1 * op2;
    }

    std::map<double, core::DVec> toDVec(const std::map<double, Eigen::Matrix4d> &data, bool useReferenceQuaternion)
    {
        std::map<double, core::DVec> convertedData;
        bool first = true;
        Eigen::Quaterniond prevQ;
        for (const auto &d : data)
        {
            core::DVec dvec;
            if (first)
            {
                if (useReferenceQuaternion)
                    dvec = math::util::toDVec(d.second, math::util::REFERENCE_UNIT_QUATERNION);
                else
                    dvec = math::util::toDVec(d.second);
                first = false;
            }
            else dvec = math::util::toDVec(d.second, prevQ);
            convertedData.insert(std::make_pair(d.first, dvec));
            prevQ = toQuat<double>(dvec);
        }
        return convertedData;
    }

} // namespace mplib::math::util
