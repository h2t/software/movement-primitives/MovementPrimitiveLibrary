#pragma once

#include <vector>

#include "mplib/core/Exception.h"

namespace mplib::math::util
{
    template <class T>
    inline void
    checkValue(const T& value)
    {
        if (std::numeric_limits<T>::infinity() == value)
        {
            throw core::Exception("result value is inf");
        }

        if (value != value)
        {
            throw core::Exception("result value is nan");
        }
    }

    template <class T>
    inline void
    checkValues(const std::vector<T>& values)
    {
        for (size_t i = 0; i < values.size(); ++i)
        {
            checkValue(values[i]);
        }
    }
} // namespace mplib::math::util
