#pragma once

#include <map>

#include "mplib/core/Exception.h"
#include "mplib/core/types.h"

namespace mplib::math::util
{
    template <class T>
    T
    fromString(const std::string& s)
    {
        std::istringstream stream(s);
        T t;
        stream >> t;
        return t;
    }

    template <class T>
    std::vector<T>
    operator-(const std::vector<T>& v1, const std::vector<T>& v2)
    {
        std::vector<T> res(std::min(v1.size(), v2.size()), T());
        int j = 0;

        for (auto i = res.begin(); i != res.end(); ++i)
        {
            *i = v1[j] - v2[j];
            ++j;
        }

        return res;
    }

    template <class T>
    std::vector<T>
    operator+(const std::vector<T>& v1, const std::vector<T>& v2)
    {
        std::vector<T> res(std::min(v1.size(), v2.size()), T());
        int j = 0;

        for (typename std::vector<T>::iterator i = res.begin(); i != res.end(); ++i)
        {
            *i = v1[j] + v2[j];
            ++j;
        }

        return res;
    }

    template <class T>
    std::vector<T>&
    operator+=(std::vector<T>& v1, const std::vector<T>& v2)
    {
        int j = 0;

        for (typename std::vector<T>::iterator i = v1.begin(); i != v1.end(); ++i)
        {
            *i += v2[j];
            ++j;
        }

        return v1;
    }

    template <class T>
    std::vector<T>&
    operator-=(std::vector<T>& v1, const std::vector<T>& v2)
    {
        int j = 0;

        for (typename std::vector<T>::iterator i = v1.begin(); i != v1.end(); ++i)
        {
            *i -= v2[j];
            ++j;
        }

        return v1;
    }

    template <class T>
    std::vector<T>&
    operator*=(std::vector<T>& v1, double c)
    {
        int j = 0;

        for (auto i = v1.begin(); i != v1.end(); ++i)
        {
            *i *= c;
            ++j;
        }

        return v1;
    }

    template <class T>
    std::vector<T>&
    operator/=(std::vector<T>& v1, double c)
    {
        int j = 0;

        for (typename std::vector<T>::iterator i = v1.begin(); i != v1.end(); ++i)
        {
            *i /= c;
            ++j;
        }

        return v1;
    }

    template <class T>
    std::vector<T>
    operator*(double c, const std::vector<T>& v)
    {
        std::vector<T> res(v.size(), T());
        int j = 0;

        for (typename std::vector<T>::iterator i = res.begin(); i != res.end(); ++i)
        {
            *i = c * v[j];
            ++j;
        }

        return res;
    }

    template <class T>
    double
    operator*(const std::vector<T>& v, const std::vector<T>& v2)
    {
        double res = 0;

        if (v.size() != v2.size())
        {
            throw core::Exception("vectors do not match in size: ")
                << v.size() << " vs. " << v2.size();
        }

        int j = 0;

        for (typename std::vector<T>::const_iterator i = v.begin(); i != v.end(); ++i)
        {
            res += *i * v2[j];
            ++j;
        }

        return res;
    }

    template <class T>
    std::vector<T>
    operator+(const std::vector<T>& v, double s)
    {
        std::vector<T> res(v.size(), T());
        int j = 0;

        for (typename std::vector<T>::iterator i = res.begin(); i != res.end(); ++i)
        {
            *i = v[j] + s;
            ++j;
        }

        return res;
    }

    template <class T>
    std::vector<T>
    operator-(const std::vector<T>& v, double s)
    {
        std::vector<T> res(v.size(), T());
        int j = 0;

        for (typename std::vector<T>::iterator i = res.begin(); i != res.end(); ++i)
        {
            *i = v[j] - s;
            ++j;
        }

        return res;
    }

    template <class T>
    std::vector<T>
    operator/(const std::vector<T>& v, double c)
    {
        std::vector<T> res(v.size(), T());
        int j = 0;

        for (typename std::vector<T>::iterator i = res.begin(); i != res.end(); ++i)
        {
            *i = v[j] / c;
            ++j;
        }

        return res;
    }

    void pol2cart(double phi, double r, double& x, double& y);

    core::DVec pol2cart(const core::DVec& pol);

    std::map<double, double> convertArraysToMap(const core::DVec& keyVec, const core::DVec& valueVec);
} // namespace mplib::math::util
