#include "vector_arithmetic.h"

#include <cmath>

namespace mplib::math::util
{

    void
    pol2cart(double phi, double r, double& x, double& y)
    {
        x = r * cos(phi);
        y = r * sin(phi);
    }

    core::DVec
    pol2cart(const core::DVec& pol)
    {
        core::DVec res;
        res.resize(2);
        pol2cart(pol[0], pol[1], res[0], res[1]);
        return res;
    }

    std::map<double, double>
    convertArraysToMap(const core::DVec& keyVec, const core::DVec& valueVec)
    {
        std::map<double, double> result;
        unsigned int size = std::min(keyVec.size(), valueVec.size());

        for (unsigned int i = 0; i < size; i++)
        {
            result[keyVec[i]] = valueVec[i];
        }

        return result;
    }
} // namespace mplib::math::util
