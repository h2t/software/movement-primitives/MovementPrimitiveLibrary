#pragma once

// forward declaration
namespace arma
{
    template <typename V>
    struct Col;
    using vec = Col<double>;

    template <typename V>
    struct Mat;
    using mat = Mat<double>;

    template <typename V>
    struct Row;
    using rowvec = Row<double>;

    template <typename V>
    struct Cube;
    using cube = Cube<double>;
} // namespace arma
