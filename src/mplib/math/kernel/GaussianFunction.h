#pragma once

#include <boost/serialization/access.hpp>
#include <boost/serialization/nvp.hpp>

#include "AbstractFunction.h"
#include "mplib/core/types.h"

namespace mplib::math::kernel
{
    class GaussianFunction : public AbstractFunction
    {
    public:
        double mCenter{0}, mWidth{0};
        GaussianFunction();
        GaussianFunction(const GaussianFunction& source);
        GaussianFunction& operator=(const GaussianFunction& source);
        GaussianFunction(double center, double width);

        bool isInSupp(double t) const;
        double operator()(double t) const override;
        virtual double evaluate(double t) const;

        virtual double getDeriv(double t) const;

        virtual double deriEvaluate(double t) const;

        friend class boost::serialization::access;
        template <class Archive>
        void
        serialize(Archive& ar, const unsigned int /*fileVersion*/)
        {
            ar& BOOST_SERIALIZATION_NVP(mCenter);
            ar& BOOST_SERIALIZATION_NVP(mWidth);
        }
    };
} // namespace mplib::math::kernel
