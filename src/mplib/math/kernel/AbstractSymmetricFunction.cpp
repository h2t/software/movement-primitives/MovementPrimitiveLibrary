#include "AbstractSymmetricFunction.h"

namespace mplib::math::kernel
{

    AbstractSymmetricFunction::AbstractSymmetricFunction() = default;

    AbstractSymmetricFunction::AbstractSymmetricFunction(double center, double width)
    {
        mCenter = center;
        mWidth = width;
    }

    AbstractSymmetricFunction::~AbstractSymmetricFunction() = default;

    double
    AbstractSymmetricFunction::center() const
    {
        return mCenter;
    }

    double
    AbstractSymmetricFunction::width() const
    {
        return mWidth;
    }

    double
    AbstractSymmetricFunction::operator()(double t) const
    {
        return evaluate((t - mCenter) / mWidth);
    }

} // namespace mplib::math::kernel
