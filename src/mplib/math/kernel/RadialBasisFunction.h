#pragma once

#include "mplib/core/types.h"

namespace mplib::math::kernel
{
    class RadialBasisFunction
    {
    public:
        RadialBasisFunction();
        RadialBasisFunction(core::DVec center);
        RadialBasisFunction(const RadialBasisFunction& source);

        virtual double operator()(core::DVec x) const;

        virtual double evaluate(double t) const = 0;

        virtual double distance(core::DVec a, core::DVec b) const = 0;

        core::DVec center() const;

    protected:
        core::DVec mCenter;
    };
} // namespace mplib::math::kernel
