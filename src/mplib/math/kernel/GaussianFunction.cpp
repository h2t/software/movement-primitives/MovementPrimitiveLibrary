#include "GaussianFunction.h"

#include <cmath>

#include "mplib/core/Exception.h"

namespace mplib::math::kernel
{

    GaussianFunction::GaussianFunction() = default;

    GaussianFunction::GaussianFunction(const GaussianFunction& source) :
        mCenter(source.mCenter), mWidth(source.mWidth)
    {
    }

    GaussianFunction&
    GaussianFunction::operator=(const GaussianFunction& source)
    {
        this->mCenter = source.mCenter;
        this->mWidth = source.mWidth;
        return *this;
    }

    GaussianFunction::GaussianFunction(double center, double width) : mCenter(center), mWidth(width)
    {
        if (mWidth == 0)
        {
            throw core::Exception("Kernel width must not be zero: ");
        }
    }

    bool
    GaussianFunction::isInSupp(double t) const
    {
        return fabs(t - mCenter) <= mWidth;
    }

    double
    GaussianFunction::operator()(double t) const
    {
        if (!isInSupp(t))
        {
            return 1e-07;
        }

        return evaluate(t - mCenter);
    }

    double
    GaussianFunction::evaluate(double t) const
    {
        double result = exp(-1.0 / pow(2.0 * (mWidth / 2.5 / 2.5), 2) * pow(t, 2.0));
        return result;
    }

    double
    GaussianFunction::getDeriv(double t) const
    {
        if (!isInSupp(t))
        {
            return 0;
        }

        return deriEvaluate(t - mCenter);
    }

    double
    GaussianFunction::deriEvaluate(double t) const
    {
        double a = pow(2.0 * (mWidth / 2.5 / 2.5), 2);
        double result = -(2.0 * t / a) * exp(-pow(t, 2.0) / a);
        return result;
    }

} // namespace mplib::math::kernel
