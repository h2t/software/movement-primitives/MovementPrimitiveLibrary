#pragma once

#include <boost/serialization/access.hpp>
#include <boost/serialization/nvp.hpp>

#include "RadialBasisFunction.h"

namespace mplib::math::kernel
{
    class GaussianRadialBasisFunction : public RadialBasisFunction
    {
    public:
        core::DVec mWidth;
        unsigned int dimension;
        GaussianRadialBasisFunction();
        GaussianRadialBasisFunction(const core::DVec& center, core::DVec width);

        GaussianRadialBasisFunction(const GaussianRadialBasisFunction& source);

        core::DVec width() const;

        int dim();

        bool isInSupp(core::DVec x) const;

        double distance(core::DVec a, core::DVec b) const override;

        double evaluate(double t) const override;

        friend class boost::serialization::access;
        template <class Archive>
        void
        serialize(Archive& ar, const unsigned int /*fileVersion*/)
        {
            ar& boost::serialization::make_nvp("m_center", mCenter);
            ar& boost::serialization::make_nvp("m_width", mWidth);
            ar& boost::serialization::make_nvp("dimension", dimension);
        }
    };
} // namespace mplib::math::kernel
