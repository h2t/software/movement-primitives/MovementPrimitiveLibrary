#include "PeriodicGaussianFunction.h"

#include <cmath>
#include <iostream>

namespace mplib::math::kernel
{

    PeriodicGaussianFunction::PeriodicGaussianFunction() = default;

    PeriodicGaussianFunction::PeriodicGaussianFunction(double center, double width) :
        AbstractSymmetricFunction(center, width)
    {
        if (width > 2 * M_PI)
        {
            std::cout << "WARNING: Invalid support width = " << width << " for PeriodicGaussKernel"
                      << std::endl;
        }

        mW0 = log(0.003) / (cos(width / 2) - 1.0);
    }

    bool
    PeriodicGaussianFunction::isInSupp(double /*phi*/) const
    {
        return true;
    }

    double
    PeriodicGaussianFunction::operator()(double phi) const
    {
        phi -= floor(phi / (2 * M_PI)) * 2 * M_PI;
        return evaluate(phi - mCenter);
    }

    double
    PeriodicGaussianFunction::evaluate(double phi) const
    {
        return exp(mW0 * (cos(phi) - 1));
    }

    double
    PeriodicGaussianFunction::getDeriv(double /*t*/) const
    {
        return 0;
    }

} // namespace mplib::math::kernel
