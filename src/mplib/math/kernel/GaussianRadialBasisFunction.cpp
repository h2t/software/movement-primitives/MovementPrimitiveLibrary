#include "GaussianRadialBasisFunction.h"

#include <cmath>

#include "mplib/core/Exception.h"

namespace mplib::math::kernel
{

    GaussianRadialBasisFunction::GaussianRadialBasisFunction() = default;

    GaussianRadialBasisFunction::GaussianRadialBasisFunction(const core::DVec& center,
                                                             core::DVec width) :
        RadialBasisFunction(center), mWidth(std::move(width))
    {
        dimension = center.size();
    }

    GaussianRadialBasisFunction::GaussianRadialBasisFunction(
        const GaussianRadialBasisFunction& source) :
        RadialBasisFunction(source)
    {
        mWidth = source.mWidth;
        mCenter = source.mCenter;
        dimension = source.dimension;
    }

    core::DVec
    GaussianRadialBasisFunction::width() const
    {
        return mWidth;
    }

    int
    GaussianRadialBasisFunction::dim()
    {
        return dimension;
    }

    bool
    GaussianRadialBasisFunction::isInSupp(core::DVec x) const
    {
        bool res = true;
        for (size_t i = 0; i < x.size(); ++i)
        {
            res = res && fabs(x[i] - mCenter[i]) <= mWidth[i];
        }

        return res;
    }

    double
    GaussianRadialBasisFunction::distance(core::DVec a, core::DVec b) const
    {
        if (a.size() != b.size() || a.size() != mWidth.size())
        {
            throw core::Exception("The dimensions of both vectors are not the same.");
        }

        double res = 0;
        for (size_t i = 0; i < a.size(); ++i)
        {
            res += pow(a[i] - b[i], 2) / pow(2.0 * (mWidth[i] / 2.5 / 2.5), 2);
        }

        return res;
    }

    double
    GaussianRadialBasisFunction::evaluate(double t) const
    {
        return exp(-0.5 * t);
    }

} // namespace mplib::math::kernel
