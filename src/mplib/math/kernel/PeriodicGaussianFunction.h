#pragma once

#include <boost/serialization/access.hpp>
#include <boost/serialization/nvp.hpp>

#include "AbstractSymmetricFunction.h"

namespace mplib::math::kernel
{
    class PeriodicGaussianFunction : public AbstractSymmetricFunction
    {
    public:
        double mW0;
        PeriodicGaussianFunction();
        PeriodicGaussianFunction(double center, double width);

        bool isInSupp(double phi) const;

        double operator()(double phi) const override;

        double evaluate(double phi) const override;

        virtual double getDeriv(double t) const;

        friend class boost::serialization::access;
        template <class Archive>
        void
        serialize(Archive& ar, const unsigned int /*fileVersion*/)
        {
            ar& BOOST_SERIALIZATION_NVP(mCenter);
            ar& BOOST_SERIALIZATION_NVP(mWidth);
            ar& BOOST_SERIALIZATION_NVP(mW0);
        }
    };
} // namespace mplib::math::kernel
