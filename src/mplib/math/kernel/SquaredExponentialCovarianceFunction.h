#include <boost/serialization/access.hpp>
#include <boost/serialization/nvp.hpp>

#include "CovarianceFunction.h"

namespace mplib::math::kernel
{
    class SquaredExponentialCovarianceFunction : public CovarianceFunction
    {
    public:
        double l;
        double sigmaF;
        double sigmaN;

        SquaredExponentialCovarianceFunction();

        SquaredExponentialCovarianceFunction(double l, double sigmaF, double sigmaN);

        double calc(const core::DVec& q1, const core::DVec& q2) const override;

        core::DVec dericalc(const core::DVec& q1, const core::DVec& q2) const;

    private:
        friend class boost::serialization::access;
        template <class Archive>
        void
        serialize(Archive& ar, const unsigned int /*fileVersion*/)
        {
            ar& boost::serialization::make_nvp("l", l);
            ar& boost::serialization::make_nvp("sigma_f", sigmaF);
            ar& boost::serialization::make_nvp("sigma_n", sigmaN);
        }
    };
} // namespace mplib::math::kernel
