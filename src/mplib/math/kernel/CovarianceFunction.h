#pragma once

#include <string>

#include "mplib/core/types.h"

namespace mplib::math::kernel
{
    class CovarianceFunction
    {
    public:
        virtual ~CovarianceFunction();

        virtual double calc(const core::DVec& q1, const core::DVec& q2) const = 0;
    };
} // namespace mplib::math::kernel
