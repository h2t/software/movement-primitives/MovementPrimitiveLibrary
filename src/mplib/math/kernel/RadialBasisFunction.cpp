#include "RadialBasisFunction.h"

namespace mplib::math::kernel
{

    RadialBasisFunction::RadialBasisFunction() = default;

    RadialBasisFunction::RadialBasisFunction(core::DVec center) : mCenter(std::move(center))
    {
    }

    RadialBasisFunction::RadialBasisFunction(const RadialBasisFunction& source) = default;

    double
    RadialBasisFunction::operator()(core::DVec x) const
    {
        return evaluate(distance(mCenter, x));
    }

    core::DVec
    RadialBasisFunction::center() const
    {
        return mCenter;
    }

} // namespace mplib::math::kernel
