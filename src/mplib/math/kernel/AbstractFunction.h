#pragma once

#include <string>

#include "mplib/core/types.h"

namespace mplib::math::kernel
{
    class AbstractFunction
    {
    public:
        virtual ~AbstractFunction() = default;

        virtual double operator()(double t) const = 0;
    };
} // namespace mplib::math::kernel
