#pragma once

#include <Eigen/Core>

#include "mplib/core/types.h"

namespace mplib::math::kernel
{
    class ReceptiveFieldWeightedRegression
    {
    public:
        int dim;
        Eigen::VectorXf c;
        Eigen::MatrixXf M;

        ReceptiveFieldWeightedRegression();

        ReceptiveFieldWeightedRegression(const Eigen::VectorXf& center, const Eigen::MatrixXf& mc);

        double evaluate(const Eigen::VectorXf& x) const;

        double operator()(const core::DVec& x) const;

        double detD();

        bool isInSupp(const core::DVec& x) const;
    };
} // namespace mplib::math::kernel
