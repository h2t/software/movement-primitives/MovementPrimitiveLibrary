#include "SquaredExponentialCovarianceFunction.h"

#include <cmath>
#include <iostream>

namespace mplib::math::kernel
{

    SquaredExponentialCovarianceFunction::SquaredExponentialCovarianceFunction()
    {
        l = 0.1;
        sigmaF = 0.1;
        sigmaN = 0.00001;
    }

    SquaredExponentialCovarianceFunction::SquaredExponentialCovarianceFunction(double l,
                                                                               double sigmaF,
                                                                               double sigmaN)
    {
        this->l = l;
        this->sigmaF = sigmaF;
        this->sigmaN = sigmaN;
    }

    double
    SquaredExponentialCovarianceFunction::calc(const core::DVec& q1, const core::DVec& q2) const
    {
        if (q1.size() != q2.size())
        {
            std::cerr << "Warning: cannot calculate kernel value of two vectors of different sizes "
                      << std::endl;
            return 0;
        }

        double norm2 = 0;
        for (size_t i = 0; i < q1.size(); i++)
        {
            norm2 += pow(q1.at(i) - q2.at(i), 2);
        }

        double res = sqrt(M_PI) * l * pow(sigmaF, 2) * exp(-norm2 / (4 * pow(l, 2)));

        if (norm2 == 0)
        {
            res += sigmaN;
        }

        return res;
    }

    core::DVec
    SquaredExponentialCovarianceFunction::dericalc(const core::DVec& q1, const core::DVec& q2) const
    {
        if (q1.size() != q2.size())
        {
            std::cerr << "Warning: cannot calculate kernel value of two vectors of different sizes "
                      << std::endl;
            return core::DVec(1, 0);
        }

        core::DVec res;
        double norm2 = 0;
        for (size_t i = 0; i < q1.size(); i++)
        {
            norm2 += pow(q1.at(i) - q2.at(i), 2);
        }

        double multfactor = sqrt(M_PI) * pow(sigmaF, 2);
        double exp_part = exp(-norm2 / (4 * pow(l, 2)));
        double deriv_l_1 = multfactor * exp_part;
        double deriv_l_2 = multfactor * l * exp_part * (norm2 / 2) * pow(l, -3);
        double deriv_l = deriv_l_1 + deriv_l_2;

        res.push_back(deriv_l);

        double deriv_sigma_f = 2 * exp_part * sqrt(M_PI) * sigmaF * l;

        res.push_back(deriv_sigma_f);

        double deriv_sigma_n = 0;
        if (norm2 == 0)
        {
            deriv_sigma_n = 1;
        }

        res.push_back(deriv_sigma_n);

        return res;
    }

} // namespace mplib::math::kernel
