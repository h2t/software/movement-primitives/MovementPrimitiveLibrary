#include "ReceptiveFieldWeightedRegression.h"

#include <Eigen/Geometry>

#include "mplib/core/Exception.h"

namespace mplib::math::kernel
{

    ReceptiveFieldWeightedRegression::ReceptiveFieldWeightedRegression() = default;

    ReceptiveFieldWeightedRegression::ReceptiveFieldWeightedRegression(
        const Eigen::VectorXf& center,
        const Eigen::MatrixXf& mc)
    {
        if (center.rows() != mc.rows())
        {
            throw core::Exception("RFWR Kernel cannot be initialized: the dimension of center does "
                                  "not corresponds to the dimension of covariance matrix");
        }
        c = center;
        M = mc;
        dim = center.rows();
    }

    double
    ReceptiveFieldWeightedRegression::evaluate(const Eigen::VectorXf& x) const
    {
        Eigen::MatrixXf D = M.transpose() * M;
        return exp(-0.5 * (x - c).transpose() * D * (x - c));
    }

    double
    ReceptiveFieldWeightedRegression::operator()(const core::DVec& x) const
    {
        Eigen::VectorXd xv = Eigen::VectorXd::Map(x.data(), x.size());
        return evaluate(xv.cast<float>());
    }

    double
    ReceptiveFieldWeightedRegression::detD()
    {
        Eigen::MatrixXf D = M.transpose() * M;
        return D.determinant();
    }

    bool
    ReceptiveFieldWeightedRegression::isInSupp(const core::DVec& /*x*/) const
    {
        return true;
    }

} // namespace mplib::math::kernel
