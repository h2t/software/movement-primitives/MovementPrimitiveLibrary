#pragma once

#include "AbstractFunction.h"

namespace mplib::math::kernel
{
    class AbstractSymmetricFunction : public AbstractFunction
    {
    public:
        double mCenter, mWidth;
        AbstractSymmetricFunction();
        AbstractSymmetricFunction(double center, double width);
        ~AbstractSymmetricFunction() override;

        double center() const;
        double width() const;

        virtual double evaluate(double t) const = 0;
        double operator()(double t) const override;
    };
} // namespace mplib::math::kernel
