#pragma once

#include <boost/serialization/nvp.hpp>

#include "mplib/core/types.h"

namespace mplib::math::ode
{

    class BasicODE
    {
    public:
        BasicODE(unsigned int dim, double epsabs = 1e-15, long maxStepCount = 10000);
        BasicODE(double epsabs = 1e-15, long maxStepCount = 10000);

        virtual ~BasicODE() = default;

        /**
         * @brief integrateForDim calculates the solution for a specific dimension.
         * @param t
         * @param init
         * @param dim
         * @param project
         * @return
         */
        double integrateForDim(double t,
                               const core::DVec& init,
                               const unsigned int dim,
                               bool project = true);

        void integrate(double t,
                       double tInit,
                       const core::DVec& init,
                       core::DVec& out,
                       bool project = true);
        void
        integrateWithEulerMethod(double t, double tInit, const core::DVec& init, core::DVec& out);
        void integrate(double t, const core::DVec& init, core::DVec& out, bool project = true);
        void integrate(const core::DVec& times,
                       const core::DVec& init,
                       std::vector<core::DVec>& out,
                       bool project = true);
        void integrateWithEulerMethod(const core::DVec& times,
                                      const core::DVec& init,
                                      std::vector<core::DVec>& out);

        unsigned int
        dim() const
        {
            return mDim;
        }
        double
        epsabs() const
        {
            return mEpsabs;
        }
        void
        setDim(unsigned int dim)
        {
            mDim = dim;
        }
        void
        setEpsabs(double epsabs)
        {
            mEpsabs = epsabs;
        }

        virtual void flow(double t, const core::DVec& x, core::DVec& out) = 0;

    protected:
        virtual unsigned int dimValuesOfInterest() const;

    private:
        unsigned int mDim{0};
        double mEpsabs;
        long mMaxStepCount;

    private:
        friend class boost::serialization::access;
        template <class Archive>
        void
        serialize(Archive& ar, const unsigned int /*file_version*/)
        {
            ar& boost::serialization::make_nvp("m_dim", mDim);
            ar& boost::serialization::make_nvp("m_epsabs", mEpsabs);
            ar& boost::serialization::make_nvp("m_maxStepCount", mMaxStepCount);
        }
    };
} // namespace mplib::math::ode
