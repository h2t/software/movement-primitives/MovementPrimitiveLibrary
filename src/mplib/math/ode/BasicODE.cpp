#include "BasicODE.h"

#include <boost/numeric/odeint.hpp>

#include "mplib/core/Exception.h"

namespace mplib::math::ode
{

    struct OdeIntegratorFlowFunctor
    {
        OdeIntegratorFlowFunctor(BasicODE* params) : ode(params)
        {
        }

        void
        operator()(const core::DVec& x, core::DVec& f, const double t)
        {
            ode->flow(t, x, f);
        }

    private:
        BasicODE* ode;
    };

    struct OdeObserver
    {
        std::vector<core::DVec>& mStates;
        std::vector<double>& mTimes;
        size_t mSteps;
        size_t maxSteps;

        OdeObserver(std::vector<core::DVec>& states,
                    std::vector<double>& times,
                    size_t maxSteps = 0) :
            mStates(states), mTimes(times), mSteps(0), maxSteps(maxSteps)
        {
        }

        void
        operator()(const core::DVec& x, double t)
        {
            mStates.push_back(x);
            mTimes.push_back(t);
            if (maxSteps > 0)
            {
                ++mSteps;
                if (mSteps > maxSteps)
                {
                    throw core::Exception("ODE error: Too much steps");
                }
            }
        }
    };

    BasicODE::BasicODE(unsigned int dim, double epsabs, long maxStepCount) :
        mDim(dim), mEpsabs(epsabs), mMaxStepCount(maxStepCount)
    {
    }

    BasicODE::BasicODE(double epsabs, long maxStepCount) :
        mEpsabs(epsabs), mMaxStepCount(maxStepCount)
    {
    }

    unsigned int
    BasicODE::dimValuesOfInterest() const
    {
        return dim();
    }

    void
    BasicODE::integrate(double t,
                        double tInit,
                        const core::DVec& init,
                        core::DVec& out,
                        bool project)
    {
        double stepSize = mEpsabs;

        if (t < tInit)
        {
            stepSize *= -1;
        }

        if (dim() != init.size())
        {
            throw core::Exception()
                << "Dimensions of ODE (" << dim() << ") and the initialValue-vector ("
                << init.size() << ") do not match!";
        }

        auto stepper = boost::numeric::odeint::make_controlled(
            mEpsabs, 0.0f, boost::numeric::odeint::runge_kutta_fehlberg78<core::DVec>());
        std::vector<core::DVec> x_vec;
        core::DVec times;
        core::DVec initVec = init;
        boost::numeric::odeint::integrate_adaptive(stepper,
                                                   OdeIntegratorFlowFunctor(this),
                                                   initVec,
                                                   tInit,
                                                   t,
                                                   stepSize,
                                                   OdeObserver(x_vec, times, mMaxStepCount));
        core::DVec output = x_vec.back();

        if (project)
        {
            out = core::DVec(output.begin(), output.begin() + dimValuesOfInterest());
        }
        else
        {
            out = output;
        }
    }

    void
    BasicODE::integrateWithEulerMethod(double t,
                                       double tInit,
                                       const core::DVec& init,
                                       core::DVec& out)
    {

        double tCurr = tInit;
        out = init;
        while (tCurr <= t)
        {

            core::DVec deri_out;
            deri_out = out;
            flow(tCurr, out, deri_out);

            for (size_t j = 0; j < out.size(); j++)
            {
                out[j] += mEpsabs * deri_out[j];
            }

            tCurr += mEpsabs;
        }
    }

    void
    BasicODE::integrate(double t, const core::DVec& init, core::DVec& out, bool project)
    {
        integrate(t, 0, init, out, project);
    }

    void
    BasicODE::integrate(const core::DVec& times,
                        const core::DVec& init,
                        std::vector<core::DVec>& out,
                        bool project)
    {
        double stepSize = mEpsabs;

        if (times.end() < times.begin())
        {
            stepSize *= -1;
        }

        int idim = dimValuesOfInterest();
        core::DVec x_result;
        x_result.resize(idim);

        if (project)
        {
            x_result = core::DVec(init.begin(), init.begin() + idim);
        }
        else
        {
            x_result = init;
        }

        out.push_back(x_result);

        core::DVec vec = init;
        double lastTime = 0;
        for (auto i = times.begin() + 1; i != times.end(); ++i)
        {
            double dt = *i - lastTime;

            auto kutta = boost::numeric::odeint::make_controlled(
                mEpsabs, 0.0f, boost::numeric::odeint::runge_kutta_fehlberg78<core::DVec>{});
            std::vector<core::DVec> x_vec;
            core::DVec times;
            boost::numeric::odeint::integrate_adaptive(kutta,
                                                       OdeIntegratorFlowFunctor(this),
                                                       vec,
                                                       0.0,
                                                       dt,
                                                       stepSize,
                                                       OdeObserver(x_vec, times, mMaxStepCount));
            vec = x_vec.back();

            if (project)
            {
                x_result = core::DVec(vec.begin(), vec.begin() + idim);
            }
            else
            {
                x_result = init;
            }

            out.push_back(x_result);

            lastTime = *i;
        }
    }

    void
    BasicODE::integrateWithEulerMethod(const core::DVec& times,
                                       const core::DVec& init,
                                       std::vector<core::DVec>& out)
    {
        core::DVec curstate = init;
        out.push_back(curstate);
        for (size_t i = 0; i < times.size() - 1; i++)
        {
            double tInit = times.at(i);
            double tEnd = times.at(i + 1);

            core::DVec curout;
            integrateWithEulerMethod(tEnd, tInit, curstate, curout);
            out.push_back(curout);

            curstate = curout;
        }
    }

} // namespace mplib::math::ode
