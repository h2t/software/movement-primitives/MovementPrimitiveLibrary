#include "PeriodicCanonicalSystem.h"

#include <cmath>

namespace mplib::math::canonical_system
{

    PeriodicCanonicalSystem::PeriodicCanonicalSystem(double tau,
                                                     double uMin,
                                                     double initialXValue,
                                                     double initialYValue) :
        CanonicalSystem(tau, uMin, initialXValue, initialYValue)
    {
    }

    std::string
    PeriodicCanonicalSystem::getName()
    {
        return "Periodic";
    }

    void
    PeriodicCanonicalSystem::flow(double /*t*/, const core::DVec& /*x*/, core::DVec& out)
    {
        out[0] = (2 * M_PI - 1e-3) / getTau();
    }

} // namespace mplib::math::canonical_system
