#include "CanonicalSystem.h"

#include <cmath>

namespace mplib::math::canonical_system
{

    CanonicalSystem::CanonicalSystem(double tau,
                                     double uMin,
                                     double initialXValue,
                                     double initialYValue) :
        BasicODE(1, 1e-5),

        uMin(uMin),
        tau(tau),
        alpha(-log(uMin)),
        initialXValue(initialXValue),
        initialYValue(initialYValue)

    {
    }

    void
    CanonicalSystem::flow(double /*t*/, const core::DVec& x, core::DVec& out)
    {
        const double& u = x[0];

        if (out.size() == 0)
        {
            out.resize(1);
        }

        //    if( u > 10 * uMin)
        //        out[0] = - 0.95 / stopPhaseValuetau;
        //    else
        out[0] = -alpha * u / (tau * (1 + phaseStopValue));
    }

    std::string
    CanonicalSystem::getName()
    {
        return "Exponential Decay";
    }

    double
    CanonicalSystem::getInitialValue() const
    {
        return initialYValue;
    }

    double
    CanonicalSystem::getAlpha()
    {
        return alpha;
    }

    double
    CanonicalSystem::getUMin()
    {
        return uMin;
    }

    double
    CanonicalSystem::getTau() const
    {
        return tau;
    }

    void
    CanonicalSystem::setTau(double tau)
    {
        this->tau = tau;
    }

    double
    CanonicalSystem::getAmpl()
    {
        return 1;
    }

    void
    CanonicalSystem::setAmpl(double r)
    {
    }

    double
    CanonicalSystem::getPhaseStopValue()
    {
        return phaseStopValue;
    }

    void
    CanonicalSystem::setPhaseStopValue(double pv)
    {
        phaseStopValue = pv;
    }

    core::DVec
    CanonicalSystem::getCanonicalValue(double t)
    {
        return core::DVec(1, exp(-alpha * t));
    }

} // namespace mplib::math::canonical_system
