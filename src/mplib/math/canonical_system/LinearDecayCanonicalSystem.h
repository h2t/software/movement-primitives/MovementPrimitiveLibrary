/**
* This file is part of DMP.
*
* DMP is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* DMP is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MPLIB::
* @author     H2T @ KIT
* @date       2021
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#pragma once

#include "CanonicalSystem.h"

namespace mplib::math::canonical_system
{
    class LinearDecayCanonicalSystem : public CanonicalSystem
    {
    public:
        LinearDecayCanonicalSystem(double tau = 0.0, double uMin = 1e-8);

        virtual std::unique_ptr<CanonicalSystem> clone() override {
            return std::make_unique<LinearDecayCanonicalSystem>(*this);
        }

        std::string getName() override;

        void flow(double t, const core::DVec& x, core::DVec& out) override;
        double step(double canVal, double deltaT);

    private:
        friend class boost::serialization::access;
        template <class Archive>
        void
        serialize(Archive& ar, const unsigned int /*fileVersion*/)
        {
            auto& base = boost::serialization::base_object<CanonicalSystem>(*this);
            ar& boost::serialization::make_nvp("LinearDecayCanonicalSystem_base", base);
        }
    };
} // namespace mplib::math::canonical_system
