#include "LinearSigmoidalCanonicalSystem.h"

namespace mplib::math::canonical_system
{

    LinearSigmoidalCanonicalSystem::LinearSigmoidalCanonicalSystem(double tau) :
        CanonicalSystem(tau)
    {
    }

    void
    LinearSigmoidalCanonicalSystem::flow(double /*t*/, const core::DVec& x, core::DVec& out)
    {
        double u = x.at(0);

        if (u > getInitialValue() * 0.55)
        {
            out[0] = -getTau() / 2;
        }
        else
        {
            out[0] = -getAlpha() * u / getTau();
        }
    }

} // namespace mplib::math::canonical_system
