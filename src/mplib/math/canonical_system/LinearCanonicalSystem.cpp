#include "LinearCanonicalSystem.h"

namespace mplib::math::canonical_system
{

    LinearCanonicalSystem::LinearCanonicalSystem(double tau) : CanonicalSystem(tau, 1e-8, 0.0, 0.0)
    {
        slowDown = false;
    }

    std::string
    LinearCanonicalSystem::getName()
    {
        return "Linear";
    }

    void
    LinearCanonicalSystem::flow(double /*t*/, const core::DVec& /*x*/, core::DVec& out)
    {
        float tau = getTau();
        if (out.size() == 0)
        {
            out.resize(1);
        }

        out[0] = 1.0 / tau;
    }

    void
    LinearCanonicalSystem::setSlowDown(bool on)
    {
        slowDown = on;
    }

} // namespace mplib::math::canonical_system
