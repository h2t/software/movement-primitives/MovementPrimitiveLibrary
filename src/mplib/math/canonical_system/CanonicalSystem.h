/**
* This file is part of DMP.
*
* DMP is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* DMP is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MPLIB::
* @author     H2T @ KIT
* @date       2021
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#pragma once

#include <memory>

#include "mplib/core/types.h"
#include "mplib/math/ode/BasicODE.h"

namespace mplib::math::canonical_system
{
    class CanonicalSystem : public math::ode::BasicODE
    {
    public:
        CanonicalSystem(double tau = 0.0,
                        double min = 0.001,
                        double initialXValue = 0.0,
                        double initialYValue = 1.0);

        virtual std::unique_ptr<CanonicalSystem> clone() {
            return std::make_unique<CanonicalSystem>(*this);
        }

        void flow(double t, const core::DVec& x, core::DVec& out) override;

        virtual std::string getName();
        double getInitialValue() const;
        double getAlpha();
        double getUMin();
        double getTau() const;
        void setTau(double tau);

        virtual double getAmpl();

        virtual void setAmpl(double r);

        virtual double getPhaseStopValue();

        virtual void setPhaseStopValue(double pv);

        core::DVec getCanonicalValue(double t);

    protected:
        double phaseStopValue{0};

    private:
        /**
         * @brief uMin is value of convergence of the canonical system
         */
        double uMin;
        /**
         * @brief tau is the temporal factor.
         */
        double tau;

        /**
         * @brief alpha is a factor for the ODE, that determines the relation
         * between u and u'.
         */
        double alpha;

        double initialXValue;
        double initialYValue;
        /**
         * @brief initialXValue is value of the canonical system at t = initialXValue
         * @see initialXValue
         */

        friend class boost::serialization::access;
        template <class Archive>
        void
        serialize(Archive& ar, const unsigned int /*fileVersion*/)
        {
            ar& boost::serialization::make_nvp("phaseStopValue", phaseStopValue);
            ar& boost::serialization::make_nvp("uMin", uMin);
            ar& boost::serialization::make_nvp("tau", tau);
            ar& boost::serialization::make_nvp("alpha", alpha);
            ar& boost::serialization::make_nvp("initialXValue", initialXValue);
            ar& boost::serialization::make_nvp("initialYValue", initialYValue);

            auto& base = boost::serialization::base_object<BasicODE>(*this);
            ar& boost::serialization::make_nvp("CanonicalSystem_base", base);
        }
    };
} // namespace mplib::math::canonical_system
