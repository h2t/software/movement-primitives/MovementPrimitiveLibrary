#include "LinearDecayCanonicalSystem.h"

namespace mplib::math::canonical_system
{

    LinearDecayCanonicalSystem::LinearDecayCanonicalSystem(double tau, double uMin) :
        CanonicalSystem(tau, uMin)
    {
    }

    std::string
    LinearDecayCanonicalSystem::getName()
    {
        return "Linear Decay";
    }

    void
    LinearDecayCanonicalSystem::flow(double /*t*/, const core::DVec& /*x*/, core::DVec& out)
    {
        float tau = getTau();
        if (out.size() == 0)
        {
            out.resize(1);
        }
        out[0] = -1.0 / tau;
    }

    double
    LinearDecayCanonicalSystem::step(double canVal, double deltaT)
    {
        return canVal - deltaT * 1.0 / getTau();
    }

} // namespace mplib::math::canonical_system
