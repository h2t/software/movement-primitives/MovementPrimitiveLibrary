/**
* This file is part of DMP.
*
* DMP is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* DMP is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MPLIB::
* @author     H2T @ KIT
* @date       2021
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#pragma once

#include <cmath>
#include <memory>

#include "mplib/core/types.h"
#include "mplib/math/ode/BasicODE.h"

namespace mplib::math::canonical_system
{
    class PeriodicDiscreteCanonicalSystem : public math::ode::BasicODE
    {
    public:
        double mAlpha, mBeta, mEta;
        double mOmega, mMu, mMu1;

        PeriodicDiscreteCanonicalSystem(double omega = M_PI,
                                        double alpha = 1.0 / 6,
                                        double beta = 1.0 / 1000,
                                        double eta = 35,
                                        double mu = 1.0,
                                        double mu1 = 1.2);

        double getMu1() const;
        double getOmega() const;
        double getMu() const;

        void flow(double t, const core::DVec& x, core::DVec& out) override;

        double explicitSolutionForR(double t, const core::DVec& init);
        double explicitSolutionForPhi(double t, const core::DVec& init);

        //! Method is untested after changing to boost
        double getTransientLengthForInitialR(double r0, double upperLimit = 10.0);
        //! Method is untested after changing to boost
        void getInitialConditionsForTransientLength(double t,
                                                    double phi1,
                                                    core::DVec& init,
                                                    double limitR = -1.0);

        friend std::ostream& operator<<(std::ostream& out,
                                        const PeriodicDiscreteCanonicalSystem& osc);

    private:
        friend class boost::serialization::access;
        template <class Archive>
        void
        serialize(Archive& ar, const unsigned int /*fileVersion*/)
        {
            auto& base = boost::serialization::base_object<BasicODE>(*this);
            ar& boost::serialization::make_nvp("PeriodicDiscreteCanonicalSystem_base", base);
        }
    };

} // namespace mplib::math::canonical_system
