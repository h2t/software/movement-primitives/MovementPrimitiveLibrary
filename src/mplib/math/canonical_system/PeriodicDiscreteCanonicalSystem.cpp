/**
* This file is part of DMP.
*
* DMP is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* DMP is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MPLIB::
* @author     H2T @ KIT
* @date       2021
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#include "PeriodicDiscreteCanonicalSystem.h"

#include <iostream>

#include <boost/math/tools/roots.hpp>

#include "mplib/math/util/vector_arithmetic.h"

namespace mplib::math::canonical_system
{

    struct FindR0Params
    {
        PeriodicDiscreteCanonicalSystem* osc;
        double T;
        double limitR;
    };

    double findR0Target(double r0, void* params);

    struct FindQParams
    {
        PeriodicDiscreteCanonicalSystem* osc;
        core::DVec init;
        core::DVec pM1;
        double deltaM1;
    };
    double findQTarget(double t, void* params);

    struct FindTForGivenR0Params
    {
        PeriodicDiscreteCanonicalSystem* osc;
        double limitR, r0;
    };

    struct FindR0TargetFunctor
    {
        FindR0TargetFunctor(FindR0Params* p) : p(p)
        {
        }

        double
        operator()(double const& x)
        {
            core::DVec init;
            init.resize(2);

            init[0] = 0;
            init[1] = x;

            double res = p->osc->explicitSolutionForR(p->T, init) - p->limitR;
            return res;
        }

    private:
        FindR0Params* p;
    };

    struct FindTForGivenR0TargetFunctor
    {
        FindTForGivenR0TargetFunctor(FindTForGivenR0Params* p) : p(p)
        {
        }

        double
        operator()(double const& x)
        {
            core::DVec init;
            init.resize(2);

            init[0] = 0;
            init[1] = p->r0;

            return p->osc->explicitSolutionForR(x, init) - p->limitR;
        }

    private:
        FindTForGivenR0Params* p;
    };

    double
    findQTarget(double t, void* params)
    {
        auto* p = (FindQParams*)params;
        core::DVec tmp;
        tmp.resize(2);
        p->osc->integrate(t, p->init, tmp);
        using namespace math::util;
        core::DVec diff = pol2cart(tmp) - pol2cart(p->pM1);
        return sqrt(diff[0] * diff[0] + diff[1] * diff[1]) - p->deltaM1;
    }

    PeriodicDiscreteCanonicalSystem::PeriodicDiscreteCanonicalSystem(double omega,
                                                                     double alpha,
                                                                     double beta,
                                                                     double eta,
                                                                     double mu,
                                                                     double mu1) :
        BasicODE(1, 1e-5)
    {
        mAlpha = alpha;
        mBeta = beta;
        mEta = eta;

        mOmega = omega;
        mMu = mu;
        mMu1 = mu1;
    }

    double
    PeriodicDiscreteCanonicalSystem::getMu1() const
    {
        return mMu1;
    }

    double
    PeriodicDiscreteCanonicalSystem::getOmega() const
    {
        return mOmega;
    }

    double
    PeriodicDiscreteCanonicalSystem::getMu() const
    {
        return mMu;
    }

    void
    PeriodicDiscreteCanonicalSystem::flow(double /*t*/, const core::DVec& x, core::DVec& out)
    {
        //double phi = x[0];
        double r = x[1];

        out[0] = mOmega;
        out[1] = mEta * (pow(mMu, mAlpha) - pow(r, mAlpha)) * pow(r, mBeta);
    }

    double
    PeriodicDiscreteCanonicalSystem::explicitSolutionForR(double t, const core::DVec& init)
    {
        core::DVec res;
        res.resize(dim());
        integrate(t, init, res);
        return res[1];
    }

    double
    PeriodicDiscreteCanonicalSystem::explicitSolutionForPhi(double t, const core::DVec& init)
    {
        core::DVec res;
        res.resize(dim());
        integrate(t, init, res);
        return res[0];
    }

    double
    PeriodicDiscreteCanonicalSystem::getTransientLengthForInitialR(double r0, double upperLimit)
    {
        if (r0 <= getMu1())
        {
            return 0.0;
        }

        double T_lo = 0.0, T_hi = upperLimit;
        FindTForGivenR0Params params = {this, getMu1(), r0};

        boost::uintmax_t maxIt = 100;
        boost::uintmax_t it = maxIt;
        // TODO maybe add custom condition |a - b| < 1e-8 + 1e-8 * min(|a|,|b|)
        auto r = boost::math::tools::bisect(FindTForGivenR0TargetFunctor(&params),
                                            T_lo,
                                            T_hi,
                                            boost::math::tools::eps_tolerance<double>(8),
                                            it);
        if (it >= maxIt)
        {
            std::cout
                << "Unable to locate solution in chosen iterations: Current best guess is between "
                << r.first << " and " << r.second << std::endl;
        }
        return r.first + (r.second - r.first) / 2;
    }

    void
    PeriodicDiscreteCanonicalSystem::getInitialConditionsForTransientLength(double t,
                                                                            double phi1,
                                                                            core::DVec& init,
                                                                            double limitR)
    {
        if (limitR < 0)
        {
            limitR = this->getMu1();
        }

        if (t > 0)
        {
            double r_lo = mMu,
                   r_hi = 1000 * mMu; // TODO: upper limit for r should be configureable
            FindR0Params params = {this, t, limitR};

            boost::uintmax_t maxIt = 100;
            boost::uintmax_t it = maxIt;
            auto r = boost::math::tools::bisect(FindR0TargetFunctor(&params),
                                                r_lo,
                                                r_hi,
                                                boost::math::tools::eps_tolerance<double>(8),
                                                it);
            if (it >= maxIt)
            {
                std::cout << "Unable to locate solution in chosen iterations: Current best guess "
                             "is between "
                          << r.first << " and " << r.second << std::endl;
            }
            init[0] = phi1 - mOmega * t;
            init[1] = r.first + (r.second - r.first) / 2;
        }
        else
        {
            init[0] = 0.0;
            init[1] = mMu;
        }
    }

} // namespace mplib::math::canonical_system
