#include "VMPFactory.h"

#include <boost/algorithm/string.hpp>

#include "mplib/core/Exception.h"
#include "mplib/representation/AbstractMovementPrimitive.h"
#include "mplib/representation/vmp/GaussianVMP.h"
#include "mplib/representation/vmp/PrincipalComponentVMP.h"
#include "mplib/representation/vmp/TaskSpaceGaussianVMP.h"
#include "mplib/representation/vmp/TaskSpacePrincipalComponentVMP.h"

namespace mplib::factories
{

    VMPFactory::VMPFactory() = default;

    VMPFactory::~VMPFactory() = default;

    std::unique_ptr<representation::AbstractMovementPrimitive>
    VMPFactory::createMP(const std::string& mptype)
    {
        const auto type = representation::VMPType::_from_string_nocase_nothrow(mptype.c_str());
        if (type)
        {
            return createMP(type.value());
        }
        return nullptr;
    }

    std::unique_ptr<representation::AbstractMovementPrimitive>
    VMPFactory::createMP(representation::VMPType type)
    {
        return createDerivedMP(type);
    }

    std::unique_ptr<representation::vmp::VMP>
    VMPFactory::createDerivedMP(representation::VMPType type) {
        using namespace representation;
        using namespace representation::vmp;

        int kernelSize = representation::vmp::VMP::defaultKernelSize;
        int baseMode = representation::vmp::VMP::defaultBaseMode;
        int componentSize = representation::vmp::VMP::defaultComponentSize;
        std::string name = std::string(type._to_string());
        parseConfig("kernelSize", kernelSize);
        parseConfig("baseMode", baseMode);
        parseConfig("mpname", name);

        switch (type)
        {
            case VMPType::Gaussian:
                parseConfig("componentSize", componentSize);
                return std::make_unique<GaussianVMP>(componentSize, name, kernelSize);
            case VMPType::TaskSpaceGaussian:
                parseConfig("componentSize", componentSize);
                return std::make_unique<TaskSpaceGaussianVMP>(componentSize, name, kernelSize);
            case VMPType::PrincipalComponent:
                return std::make_unique<PrincipalComponentVMP>(name, kernelSize);
            case VMPType::TaskSpacePrincipalComponent:
                return std::make_unique<TaskSpacePrincipalComponentVMP>(name, kernelSize);
        }
        return nullptr;
    }

} // namespace mplib::factories
