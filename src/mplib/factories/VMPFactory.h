#pragma once

#include "AbstractMPFactory.h"
#include "mplib/representation/types.h"

namespace mplib
{

// forward declaration
namespace representation::vmp
{
    class VMP;
} // namespace representation::vmp

namespace factories
{
    class VMPFactory : public AbstractMPFactory

    {
    public:
        using Type = representation::VMPType;
        using MovementPrimitive = representation::vmp::VMP;

        VMPFactory();
        ~VMPFactory();

        std::unique_ptr<representation::AbstractMovementPrimitive>
        createMP(const std::string& mptype) override;

        std::unique_ptr<representation::AbstractMovementPrimitive>
        createMP(Type type);

        std::unique_ptr<MovementPrimitive>
        createDerivedMP(Type type);
    };

} // namespace factories

} // namespace mplib
