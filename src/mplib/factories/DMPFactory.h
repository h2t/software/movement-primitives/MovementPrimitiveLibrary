#pragma once

#include "AbstractMPFactory.h"

#include "mplib/representation/types.h"

namespace mplib
{

// forward declaration
namespace representation::dmp
{
    class DiscreteDMP;
} // namespace representation::dmp

namespace factories
{
    class DMPFactory : public AbstractMPFactory
    {

    public:
        using Type = representation::DMPType;
        using MovementPrimitive = representation::dmp::DiscreteDMP;

        DMPFactory();
        ~DMPFactory();

        std::unique_ptr<representation::AbstractMovementPrimitive>
        createMP(const std::string& mptype) override;

        std::unique_ptr<representation::AbstractMovementPrimitive>
        createMP(Type type);

        std::unique_ptr<MovementPrimitive>
        createDerivedMP(Type type);
    };

} // namespace factories

} // namespace mplib
