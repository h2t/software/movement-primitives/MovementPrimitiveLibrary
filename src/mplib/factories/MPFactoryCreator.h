#pragma once

#include <memory>

#include "AbstractMPFactory.h"
#include "mplib/representation/types.h"

namespace mplib::factories
{
    std::unique_ptr<AbstractMPFactory> getFactory(const std::string& factoryName);
    std::unique_ptr<AbstractMPFactory> getFactory(representation::MovementPrimitiveType type);
} // namespace mplib::factories
