#include "MPFactoryCreator.h"

#include <boost/algorithm/string.hpp>

#include "DMPFactory.h"
#include "mplib/representation/types.h"
#ifdef ARMADILLO_FOUND
#include "VMPFactory.h"
#endif
#include "mplib/core/Exception.h"

namespace mplib::factories
{
    std::unique_ptr<AbstractMPFactory>
    getFactory(const std::string& factoryName)
    {
        const auto type = representation::MovementPrimitiveType::_from_string_nocase_nothrow(factoryName.c_str());
        if (type)
        {
            return getFactory(type.value());
        }
        return nullptr;
    }

    std::unique_ptr<AbstractMPFactory>
    getFactory(representation::MovementPrimitiveType type)
    {
        switch (type)
        {
            case representation::MovementPrimitiveType::DMP:
                return std::make_unique<DMPFactory>();
            case representation::MovementPrimitiveType::VMP:
#ifdef ARMADILLO_FOUND
                return std::make_unique<VMPFactory>();
#else
                std::err << "ARMADILLO library required for VMP" << std::endl;
                return nullptr
#endif
        }
        return nullptr;
    }

} // namespace mplib::factories
