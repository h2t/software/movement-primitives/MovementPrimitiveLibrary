#include "DMPFactory.h"

#include <boost/algorithm/string.hpp>

#include "mplib/core/Exception.h"
#include "mplib/representation/dmp/DiscreteDMP.h"
#include "mplib/representation/dmp/PeriodicDMP.h"
#include "mplib/math/canonical_system/CanonicalSystem.h"

namespace mplib::factories
{

    DMPFactory::DMPFactory() = default;

    DMPFactory::~DMPFactory() = default;

    std::unique_ptr<representation::AbstractMovementPrimitive>
    DMPFactory::createMP(const std::string& mptype)
    {
        const auto type = representation::DMPType::_from_string_nocase_nothrow(mptype.c_str());
        if (type)
        {
            return createMP(type.value());
        }
        return nullptr;
    }
    
    std::unique_ptr<representation::AbstractMovementPrimitive>
    DMPFactory::createMP(representation::DMPType type)
    {
        return createDerivedMP(type);
    }

    std::unique_ptr<representation::dmp::DiscreteDMP>
    DMPFactory::createDerivedMP(representation::DMPType type) {
        using namespace representation;
        using namespace representation::dmp;

        int kernelSize = representation::dmp::DiscreteDMP::defaultKernelSize;
        double K = representation::dmp::DiscreteDMP::defaultK;
        double D = representation::dmp::DiscreteDMP::defaultD;
        std::string name = std::string(type._to_string());
        parseConfig("K", K);
        parseConfig("D", D);
        parseConfig("kernelSize", kernelSize);
        parseConfig("mpname", name);

        switch (type)
        {
            case DMPType::Discrete:
                return std::make_unique<DiscreteDMP>(name, kernelSize, K, D);
            case DMPType::Periodic:
                return std::make_unique<PeriodicDMP>(name, kernelSize, K, D);
        }
        return nullptr;
    }

} // namespace mplib::factories
