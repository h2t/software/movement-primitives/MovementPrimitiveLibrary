#pragma once

#include <map>
#include <memory>
#include <sstream>
#include <string>

namespace mplib
{

    // forward declaration
    namespace representation
    {
        class AbstractMovementPrimitive;
    }

    namespace factories
    {

        class AbstractMPFactory
        {

        public:
            AbstractMPFactory() = default;
            ~AbstractMPFactory() = default;

            virtual std::unique_ptr<representation::AbstractMovementPrimitive>
            createMP(const std::string& mptype) = 0;

            template <typename T>
            void
            addConfig(const std::string& key, T value)
            {
                config.insert(std::make_pair(key, std::to_string(value)));
            }

        protected:
            template <typename T>
            bool
            parseConfig(const std::string& key, T& res)
            {
                if (config.find(key) == config.end())
                {
                    return false;
                }

                std::istringstream ss(config.at(key));
                ss >> res;
                return true;
            }

            std::map<std::string, std::string> config;
        };

        using AbstractMPFactoryPtr = std::shared_ptr<AbstractMPFactory>;

    } // namespace factories

} // namespace mplib
