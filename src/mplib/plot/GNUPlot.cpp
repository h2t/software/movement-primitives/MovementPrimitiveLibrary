#include "GNUPlot.h"

#include <fstream>

#include "mplib/core/Exception.h"
#include "mplib/core/SampledTrajectory.h"

namespace mplib::plot
{

    GNUPlot::GNUPlot(const std::string& baseDir) : baseDir(baseDir)
    {
        std::string cmd = "mkdir " + baseDir;
        int r = system(cmd.c_str());
        (void) r;
    }

    void
    GNUPlot::plotPoint(double x,
                       double y,
                       const std::string& color,
                       GNUPlot::PointType pointType,
                       int pointSize)
    {

        if (gnuplotCmd.str().size() != 0)
        {
            gnuplotCmd << ",";
        }

        gnuplotCmd << "\\\"<echo '" << std::to_string(x) << ", " << std::to_string(y) << "'\\\" "
                   << "with points "
                   << "lc rgb \\\"" << color << "\\\""
                   << " pt " << std::to_string(pointType) << " ps " << std::to_string(pointSize)
                   << " notitle";
    }

    void
    GNUPlot::plotPoints(const core::DVec& xs,
                        const core::DVec& ys,
                        const std::string& color,
                        GNUPlot::PointType pointType,
                        int pointSize)
    {
        if (xs.size() != ys.size())
        {
            throw core::Exception() << "xs has different size as ys";
        }

        for (size_t i = 0; i < xs.size(); ++i)
        {
            plotPoint(xs[i], ys[i], color, pointType, pointSize);
        }
    }

    void
    GNUPlot::plotLine(const core::DVec& xs,
                      const core::DVec& ys,
                      const std::string& title,
                      const std::string& color,
                      GNUPlot::LineType lineType,
                      int lineWidth)
    {
        std::string tmpFileName = baseDir + "/tmp_file_" + std::to_string(fileNum) + ".gp";
        fileNum++;
        writeTempPlotFile(xs, ys, tmpFileName);
        if (gnuplotCmd.str().size() != 0)
        {
            gnuplotCmd << ",";
        }

        gnuplotCmd << "\\\"" << tmpFileName << "\\\" "
                   << "with lines "
                   << "lc \\\"" << color << "\\\""
                   << " lt " << std::to_string(lineType) << " lw " << std::to_string(lineWidth);

        if (title == "unknown")
        {
            gnuplotCmd << " notitle";
        }
        else
        {
            gnuplotCmd << " title \\\"" << title << "\\\"";
        }
    }

    void
    GNUPlot::plotLine(const core::SampledTrajectory& traj,
                      const std::string& title,
                      const std::string& color,
                      GNUPlot::LineType lineType,
                      int lineWidth)
    {
        core::DVec timestamps = traj.getTimestamps();
        for (unsigned int i = 0; i < traj.dim(); ++i)
        {
            core::DVec dimData = traj.getDimensionData(i);
            plotLine(timestamps, dimData, title, color, lineType, lineWidth);
        }
    }

    void
    GNUPlot::show()
    {

        char buf[10240];
        sprintf(buf, GNUPLOT_COMMAND, gnuplotCmd.str().c_str());
        int r = system(buf);
        (void) r;
    }

    void
    GNUPlot::clear()
    {
        fileNum = 0;
        gnuplotCmd.str("");
    }

    void
    GNUPlot::writeTempPlotFile(const core::DVec& xs,
                               const core::DVec& ys,
                               const std::string& filename)
    {
        std::ofstream ofs(filename);
        for (size_t i = 0; i < xs.size(); ++i)
        {
            ofs << xs[i] << "\t" << ys[i];
            if (i != xs.size() - 1)
            {
                ofs << "\n";
            }
        }
        ofs.close();
    }

} // namespace mplib::plot
