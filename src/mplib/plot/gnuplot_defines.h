#pragma once

namespace mplib::plot
{

#ifndef _WIN32
#define GNUPLOT_COMMAND "gnuplot -e \"plot %s \" -persistent"
#define GNUPLOT_COMMAND_NORMAL "gnuplot -e \" %s \" -persistent"
#define GNUPLOT_COMMAND_FROM_FILE "gnuplot %s -persistent"
#define _TYPENAME typename
#define KILL_ALL_PLOT_WINDOWS { int r = system("killall gnuplot_qt"); (void) r; }
#else
#define GNUPLOT_COMMAND "pgnuplot -e \"plot %s \" -"
#define INFINITY std::numeric_limits<double>::infinity()
#define _TYPENAME
#define KILL_ALL_PLOT_WINDOWS { int r = system("taskkill /FI \"IMAGENAME eq wgnuplot.exe\""); (void) r; }
#endif

} // namespace mplib::plot
