/**
* This file is part of MPLIB.
*
* MPLIB is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* MPLIB is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MPLIB::
* @author     H2T @ KIT
* @date       2021
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#pragma once

#include <sstream>
#include <string>
#include <vector>

#include "gnuplot_defines.h"
#include "mplib/core/types.h"

namespace mplib::plot
{
    class GNUPlot
    {
    public:
        enum PointType
        {
            Plus = 1,
            Cross = 2,
            Start = 3,
            Circle = 7
        };
        enum LineType
        {
            Dash = 0,
            Solid = 1
        };
        GNUPlot(const std::string& baseDir = "plots");
        void plotPoint(double x,
                       double y,
                       const std::string& color = "blue",
                       PointType pointType = PointType::Circle,
                       int pointSize = 2);

        void plotPoints(const core::DVec& xs,
                        const core::DVec& ys,
                        const std::string& color = "blue",
                        PointType pointType = PointType::Circle,
                        int pointSize = 2);

        void plotLine(const core::DVec& xs,
                      const core::DVec& ys,
                      const std::string& title = "unknown",
                      const std::string& color = "blue",
                      LineType lineType = LineType::Solid,
                      int lineWidth = 3);

        void plotLine(const core::SampledTrajectory& traj,
                      const std::string& title = "unknown",
                      const std::string& color = "blue",
                      LineType lineType = LineType::Solid,
                      int lineWidth = 3);
        void show();
        void clear();

    private:
        std::string baseDir;
        std::stringstream gnuplotCmd;
        int fileNum{0};

        void
        writeTempPlotFile(const core::DVec& xs, const core::DVec& ys, const std::string& filename);
    };
} // namespace mplib::plot
