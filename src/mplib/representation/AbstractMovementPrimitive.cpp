#include "AbstractMovementPrimitive.h"

#include <fstream>

#include <boost/tokenizer.hpp>

#include "MPState.h"
#include "mplib/math/canonical_system/CanonicalSystem.h"

namespace mplib::representation
{

    AbstractMovementPrimitive::AbstractMovementPrimitive(
            std::unique_ptr<math::function_approximation::AbstractFunctionApproximation> baseFunctionApproximator,
            std::unique_ptr<math::canonical_system::CanonicalSystem> canonicalSystem,
            const std::string &name) :
        canonicalSystem(std::move(canonicalSystem)),
        mpName(name),
        baseFunctionApproximator(std::move(baseFunctionApproximator))
    {
        this->baseFunctionApproximator->setupKernels(0.0, 1.0);
    }

    std::string AbstractMovementPrimitive::getTypeStr() const {
        return getType()._to_string();
    }

    std::string AbstractMovementPrimitive::getRepresentationType() const {
        return getSpecificTypeStr() + getTypeStr();
    }

    std::string
    AbstractMovementPrimitive::getInstanceName() const
    {
        return mpName;
    }

    void
    AbstractMovementPrimitive::learnFromTrajectory(
        const core::SampledTrajectory& trainingTrajectory)
    {
        std::vector<core::SampledTrajectory> trainingTrajectories{trainingTrajectory};
        learnFromTrajectories(trainingTrajectories);
    }

    core::DVec2d
    AbstractMovementPrimitive::calculateTrajectoryPoint(double t,
                                                        const core::DVec& goal,
                                                        double tCurrent,
                                                        const core::DVec2d& currentStates,
                                                        core::DVec& currentCanonicalValues,
                                                        double temporalFactor)
    {
        NdimMPState currentStateVec;
        for (const core::DVec& stateVec : currentStates)
        {
            currentStateVec.emplace_back(stateVec);
        }
        NdimMPState newStates = calculateTrajectoryPoint(
            t, goal, tCurrent, currentStateVec, currentCanonicalValues, temporalFactor);
        core::DVec2d result;
        for (const MPState& state : newStates)
        {
            result.push_back(state.getValues());
        }
        return result;
    }

    core::SampledTrajectory
    AbstractMovementPrimitive::calculateTrajectory(const core::DVec& timestamps,
                                                   const core::DVec& goal,
                                                   const core::DVec2d& initialStates,
                                                   const core::DVec& initialCanonicalValues,
                                                   double temporalFactor)
    {

        auto it = timestamps.begin();
        core::DVec2d previousState = initialStates;

        std::map<double, core::DVec> resultingSystemStatesMap;

        core::DVec canonicalValues = initialCanonicalValues;
        double prevTimestamp = *it;

        for (; it != timestamps.end(); it++)
        {
            previousState = calculateTrajectoryPoint(
                *it, goal, prevTimestamp, previousState, canonicalValues, temporalFactor);
            prevTimestamp = *it;
            core::DVec positions;

            for (auto& d : previousState)
            {
                positions.push_back(d[0]);
            }

            resultingSystemStatesMap[prevTimestamp] = positions;
        }

        return core::SampledTrajectory(resultingSystemStatesMap);
    }

    void
    AbstractMovementPrimitive::setBaseFunctionApproximator(
        std::unique_ptr<math::function_approximation::AbstractFunctionApproximation> newBaseInstance)
    {
        baseFunctionApproximator = std::move(newBaseInstance);
        baseFunctionApproximator->setupKernels(0.0, 1.0);
        functionApproximators.clear();
    }

    void
    AbstractMovementPrimitive::setFunctionApproximator(
        size_t trajDim,
        std::unique_ptr<math::function_approximation::AbstractFunctionApproximation> newInstance)
    {
        if (trajDim > functionApproximators.size() || !functionApproximators.at(trajDim))
        {
            throw core::Exception("OutOfBounds: ")
                << trajDim << " size: " << functionApproximators.size();
        }

        if (trajDim == functionApproximators.size())
            functionApproximators.push_back(std::move(newInstance));
        else
            functionApproximators.at(trajDim) = std::move(newInstance);
    }

    int
    AbstractMovementPrimitive::getSizeOfFunctionApproximator()
    {
        return functionApproximators.size();
    }

    double
    AbstractMovementPrimitive::evaluateReproduction(double /*initialCanonicalValue*/,
                                                    double& /*maxErrorOut*/)
    {
        return 0;
    }

    core::DVec2d
    AbstractMovementPrimitive::getWeights() const
    {
        core::DVec2d res;
        for (const auto &funcApprox : functionApproximators)
        {
            core::DVec cur(funcApprox->getWeights());
            res.push_back(cur);
        }
        return res;
    }

    void
    AbstractMovementPrimitive::saveWeightsToFile(const std::string& fileName)
    {
        core::DVec2d weights = getWeights();

        std::ofstream ofs(fileName);
        for (auto& weight : weights)
        {
            for (size_t j = 0; j < weight.size(); ++j)
            {
                ofs << weight.at(j);
                if (j == weight.size() - 1)
                {
                    ofs << "\n";
                }
                else
                {
                    ofs << ",";
                }
            }
        }

        ofs.close();
    }

    void
    AbstractMovementPrimitive::loadWeightsFromCSV(const std::string& filename)
    {
        using Tokenizer = boost::tokenizer<boost::escaped_list_separator<char>>;
        std::vector<std::string> stringVec;
        std::string line;
        std::ifstream ifs(filename);

        int i = 0;
        while (getline(ifs, line))
        {
            Tokenizer tok(line);
            stringVec.assign(tok.begin(), tok.end());
            core::DVec weights;
            for (const auto& j : stringVec)
            {
                weights.push_back(math::util::fromString<double>(j));
            }

            setWeights(i, weights);
            i++;
        }
        trajDim = i;
        ifs.close();
    }

    void
    AbstractMovementPrimitive::setWeights(const core::DVec2d& weights)
    {
        for (size_t i = 0; i < weights.size(); i++)
        {
            setWeights(i, weights[i]);
        }
    }

    void
    AbstractMovementPrimitive::setKernels(int dim, double xmin, double xmax)
    {
        for (size_t i = 0; i < (size_t)dim; ++i)
        {
            setupKernels(i, xmin, xmax);
        }
    }

    size_t
    AbstractMovementPrimitive::getKernelSize() const
    {
        return baseFunctionApproximator->getKernelSize();
    }

    int
    AbstractMovementPrimitive::getDim() const
    {
        return trajDim;
    }

    double AbstractMovementPrimitive::getUmin() const
    {
        return canonicalSystem->getUMin();
    }

    void
    AbstractMovementPrimitive::setWeights(int i, const core::DVec& weights)
    {
        auto funcApprox = getFunctionApproximator(i);
        funcApprox->setWeights(weights);
    }

    void
    AbstractMovementPrimitive::setupKernels(int i, double xmin, double xmax)
    {
        auto funcApprox = getFunctionApproximator(i);
        funcApprox->setupKernels(xmin, xmax);
    }

    double
    AbstractMovementPrimitive::getLearnableTerm(unsigned int trajDim,
                                                core::DVec canonicalStates) const
    {
        if (canonicalStates.at(0) < 0)
        {
            std::cout << "WARNING: found canonical value smaller than 0";
        }

        double uval = uprocess(canonicalStates.at(0));
        return (*getFunctionApproximator(trajDim))(core::DVec(1, uval)).at(0);
    }

    double
    AbstractMovementPrimitive::uprocess(double uval) const
    {
        if (canonicalSystem->getName() == "Exponential Decay")
        {
            return -log(uval + 1e-6) * 6.90776;
        }
        if (canonicalSystem->getName() == "Periodic")
        {
            uval -= floor(uval / (2 * M_PI)) * 2 * M_PI;
            return uval;
        }

        return uval;
    }

    void
    AbstractMovementPrimitive::trainFunctionApproximator(
        unsigned int trajDim,
        const std::map<double, double>& perturbationForceSamples)
    {
        auto approximator = getFunctionApproximator(trajDim);
        std::map<double, double> perturbationForceSamplesInv;
        auto it = perturbationForceSamples.begin();

        for (; it != perturbationForceSamples.end(); ++it)
        {
            perturbationForceSamplesInv[uprocess(it->first)] = it->second;
        }

        core::DVec2d uValues;
        core::DVec2d values;

        for (it = perturbationForceSamplesInv.begin(); it != perturbationForceSamplesInv.end();
             ++it)
        {
            uValues.push_back(core::DVec{it->first});
            values.push_back(core::DVec{it->second});
        }

        approximator->learn(uValues, values);
    }

    math::function_approximation::AbstractFunctionApproximation*
    AbstractMovementPrimitive::getBaseFunctionApproximator() const {
        return baseFunctionApproximator.get();
    }

    void AbstractMovementPrimitive::appendTo(AbstractMovementPrimitive *mp, bool deep) const
    {
        if (auto approx = getBaseFunctionApproximator())
            mp->setBaseFunctionApproximator(approx->clone());
        if (canonicalSystem)
            mp->canonicalSystem = canonicalSystem->clone();
        mp->mpName = mpName;
        if (deep)
        {
            mp->trajDim = trajDim;
            for (auto &approx : functionApproximators)
                mp->functionApproximators.push_back(approx->clone());
        }
    }

    math::function_approximation::AbstractFunctionApproximation*
    AbstractMovementPrimitive::getFunctionApproximator(unsigned long trajDim) const
    {
        if (trajDim >= functionApproximators.size() || !functionApproximators.at(trajDim))
        {
            functionApproximators.resize(std::max(trajDim + 1, functionApproximators.size()));
            functionApproximators.at(trajDim) = baseFunctionApproximator->clone();
        }

        return functionApproximators.at(trajDim).get();
    }

} // namespace mplib::representation
