#include "MPState.h"

namespace mplib::representation
{

    MPState::MPState()
    {
        pos = vel = 0;
    }

    MPState::MPState(const MPState& state)
    {
        pos = state.pos;
        vel = state.vel;
    }

    MPState::MPState(const core::DVec& status)
    {
        checkSize(status);
        pos = status[0];
        vel = status[1];
    }

    MPState::MPState(double pos, double vel)
    {
        this->pos = pos;
        this->vel = vel;
    }

    double&
    MPState::getValue(unsigned int i)
    {
        switch (i)
        {
            case 0:
                return pos;
            case 1:
                return vel;
            default:
                throw core::Exception("out of bounds: ") << i;
        }
    }

    const double&
    MPState::getValue(unsigned int i) const
    {
        switch (i)
        {
            case 0:
                return pos;
            case 1:
                return vel;
            default:
                throw core::Exception("out of bounds: ") << i;
        }
    }

    unsigned int
    MPState::size() const
    {
        return 2;
    }

    std::ostream&
    operator<<(std::ostream& str, const MPState& value)
    {
        str << "Pos: " << value.pos << ", Vel: " << value.vel;
        return str;
    }

} // namespace mplib::representation
