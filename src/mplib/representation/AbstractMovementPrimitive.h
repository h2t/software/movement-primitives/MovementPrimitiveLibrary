/**
* This file is part of MPLIB.
*
* MPLIB is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* MPLIB is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MPLIB::
* @author     H2T @ KIT
* @date       2021
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#pragma once

#include <memory>

#include "mplib/core/types.h"
#include "types.h"

#include "MPState.h"

#include <boost/serialization/unique_ptr.hpp>

// TODO remove from header
#include "mplib/math/function_approximation/LocallyWeightedRegression.h"
#include "mplib/math/kernel/PeriodicGaussianFunction.h"
#include "mplib/math/canonical_system/CanonicalSystem.h"

#ifdef ARMADILLO_FOUND
#include "mplib/math/function_approximation/GaussianProcessRegression.h"
#endif

namespace mplib
{

    // forward declaration
    namespace math
    {
        namespace function_approximation
        {
            class AbstractFunctionApproximation;
        } // namespace function_approximation
        namespace canonical_system
        {
            class CanonicalSystem;
        }
    } // namespace math

    namespace representation
    {

        enum class Space
        {
            JointSpace,
            //! position [x,y,z] and orientation [qw, qx, qy, qz]
            TaskSpace7D,
            Custom
        };

        class AbstractMovementPrimitive
        {
        public:
            AbstractMovementPrimitive(std::unique_ptr<math::function_approximation::AbstractFunctionApproximation> baseFunctionApproximator,
                                      std::unique_ptr<math::canonical_system::CanonicalSystem> canonicalSystem,
                                      const std::string &name = "Unknown");

            AbstractMovementPrimitive() : canonicalSystem(nullptr), baseFunctionApproximator(nullptr)
            {
            }

            virtual ~AbstractMovementPrimitive() = default;

            virtual std::unique_ptr<AbstractMovementPrimitive> clone(bool deep = false) const = 0;

            virtual MovementPrimitiveType getType() const = 0;

            virtual Space getSpace() const = 0;

            //! Returns the specific implementation of the movement primitive (e.g. "Periodic")
            virtual std::string getSpecificTypeStr() const = 0;

            //! Returns the abstract movement primitve representation (e.g. "DMP")
            virtual std::string getTypeStr() const;

            //! Returns the composed representation type (e.g. "PeriodicDMP")
            virtual std::string getRepresentationType() const;

            std::string getInstanceName() const;
            virtual void save(const std::string& filename) = 0;
            virtual void load(const std::string& filename) = 0;

            /**
         * @brief calculateTrajectoryPoint calculates the next trajectory point
         * @param t: next time point
         * @param goal: target
         * @param tCurrent: current time
         * @param currentStates: current state (pos, vel)
         * @param currentCanonicalValues: determine the learnable function
         * @param temporalFactor: temporal factor
         *
         */
            virtual NdimMPState
            calculateTrajectoryPoint(double t,
                                     const core::DVec& goal,
                                     double tCurrent,
                                     const NdimMPState& currentStates,
                                     core::DVec& currentCanonicalValues,
                                     double temporalFactor) = 0;

            /**
         * @brief learnFromTrajectories is the same as learnFromTrajectory(), but
         * takes a vector of trajectories. The behaviour of the learning depends
         * on the actual dmp-implementation (e.g. average of all trajectories may
         * be learned)
         * @param trainingTrajectories vector of characteristic trajectories for the DMP
         *
         * @see learnFromTrajectory()
         */
            virtual void learnFromTrajectories(
                const std::vector<core::SampledTrajectory>& trainingTrajectories) = 0;

            virtual void learnFromTrajectory(const core::SampledTrajectory& trainingTrajectory);

            /**
        * @brief calculateTrajectoryPoint is a convenient wrapper function for calculateTrajectoryPoint
        * @see calculateTrajectoryPoint
        */
            virtual core::DVec2d calculateTrajectoryPoint(double t,
                                                          const core::DVec& goal,
                                                          double tCurrent,
                                                          const core::DVec2d& currentStates,
                                                          core::DVec& currentCanonicalValues,
                                                          double temporalFactor);

            /**
         * @brief Calculates and return the full trajectory based on trained MP and given configuration.
         * @param timestamps The timestamps for which the trajectory points are calculated.
         * @param goal The goal position of the DMP
         * @param initialStates The initial state at the beginning of the MP.
         * @param temporalFactor The temporal factor for the duration of the MP. Below 1 means faster than original trajectory.
         * @return new trajectory
         */
            virtual core::SampledTrajectory
            calculateTrajectory(const core::DVec& timestamps,
                                const core::DVec& goal,
                                const core::DVec2d& initialStates,
                                const core::DVec& initialCanonicalValues,
                                double temporalFactor);

            /**
         * @brief setFunctionApproximator sets a new type of function
         * approximator. All old instances will be deleted and recreated
         * with this type on request. Therefore a retraining is needed!
         * @param newBaseInstance instance of a function approximator,
         * from which all other instances will be cloned.
         */
            void setBaseFunctionApproximator(
                std::unique_ptr<math::function_approximation::AbstractFunctionApproximation> newBaseInstance);

            void setFunctionApproximator(
                size_t trajDim,
                std::unique_ptr<math::function_approximation::AbstractFunctionApproximation> newInstance);

            int getSizeOfFunctionApproximator();

            void setGoals(const core::DVec& goals)
            {
                this->goals = goals;
            }

            void setInitialState(const NdimMPState& startState)
            {
                this->startState = startState;
            }

            const core::DVec& getGoals() const
            {
                return goals;
            }

            const NdimMPState& getStartState() const
            {
                return startState;
            }

            const core::DVec getStart() const
            {
                return core::SystemState::convertStatesToArray(getStartState(), 0);
            }

            virtual double evaluateReproduction(double initialCanonicalValue, double& maxErrorOut);

            virtual core::DVec2d getWeights() const;

            virtual void saveWeightsToFile(const std::string& fileName);

            virtual void loadWeightsFromCSV(const std::string& filename);

            virtual void setWeights(const core::DVec2d& weights);

            virtual void setWeights(int i, const core::DVec& weights);

            void setKernels(int dim, double xmin = 0, double xmax = 1);

            size_t getKernelSize() const;
            int getDim() const;
            double getUmin() const;

        protected:

            /**
         * @brief _calcLearnableTerm calculates the perturbation terms to be learned.
         * It is different for different MP (e.g. for DMP, the secondary perturbation is to be calculated.)
         * @param trajDim the dimension to be considered;
         * @param canonicalStates: canonical values for the
         */
            virtual std::map<double, double>
            calcLearnableTerm(unsigned int trajDim,
                              const core::DVec& canonicalStates,
                              const core::SampledTrajectory& exampleTraj) = 0;

            virtual void setupKernels(int i, double xmin = 0, double xmax = 1);

            /**
         * @brief _getLearnableTerm needs to be implemented and should
         * return the perturbation force value at the given canonical state.
         * @param trajDim Dimension (e.g. joint5) for which the perturbation
         * force should be calculated.
         * @param canonicalStates State of the canonical system for which thecore::DVec2d
         * perturbation force should be calculated.
         * @return returns perturbation force for given canonical state.
         */
            virtual double getLearnableTerm(unsigned int trajDim, core::DVec canonicalStates) const;

            /**
         * @brief _uprocess preprocesses the canonical variable which is the input of the learning model.
         */
            virtual double uprocess(double uval) const;

            virtual void trainFunctionApproximator(unsigned int trajDim,
                                                   const std::map<double, double>& perturbationForceSamples);

            virtual math::function_approximation::AbstractFunctionApproximation*
            getBaseFunctionApproximator() const;

            void appendTo(AbstractMovementPrimitive* mp, bool deep) const;

            /**
         * @brief getFunctionApproximator returns an instance to a
         * function approximator for a given trajectory dimensions.
         * Each dimension has it's own instance.
         * @param trajDim dimension of the trajectory (e.g. joint5), that this
         * FunctionApproximator should represent.
         * @return shared pointer to the FunctionApproximator
         */
            virtual math::function_approximation::AbstractFunctionApproximation*
            getFunctionApproximator(unsigned long trajDim) const;

            friend class boost::serialization::access;
            template <class Archive>
            void
            serialize(Archive& ar, const unsigned int /*fileVersion*/)
            {
                ar.template register_type<math::function_approximation::LocallyWeightedRegression<
                    math::kernel::GaussianFunction>>();
                ar.template register_type<math::function_approximation::LocallyWeightedRegression<
                    math::kernel::PeriodicGaussianFunction>>();

#ifdef ARMADILLO_FOUND
                ar.template register_type<math::function_approximation::GaussianProcessRegression<
                    math::kernel::SquaredExponentialCovarianceFunction>>();
#endif

                ar& boost::serialization::make_nvp("trajDim", trajDim);
                ar& boost::serialization::make_nvp("mpName", mpName);
                ar& boost::serialization::make_nvp("canonicalSystem", canonicalSystem);
                ar& boost::serialization::make_nvp("goals", goals);
                ar& boost::serialization::make_nvp("startState", startState);
                ar& boost::serialization::make_nvp("functionApproximators",
                                                   functionApproximators);
                ar& boost::serialization::make_nvp("baseFunctionApproximator",
                                                   baseFunctionApproximator);
            }

            /**
            * @brief canonicalSystem is the canonical system for DMP
            */
            std::unique_ptr<math::canonical_system::CanonicalSystem> canonicalSystem;

            size_t trajDim;
            std::string mpName;

            /**
             * @brief goal specifies the desired end position of the trajectory.
             *
             * Must differ from \ref startPosition to have a trajectory.
             */
            core::DVec goals;

            NdimMPState startState;

            mutable std::vector<std::unique_ptr<math::function_approximation::AbstractFunctionApproximation>>
                functionApproximators;

        private:
            std::unique_ptr<math::function_approximation::AbstractFunctionApproximation> baseFunctionApproximator;
        };

    } // namespace representation

} // namespace mplib
