/**
* This file is part of MPLIB.
*
* MPLIB is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* MPLIB is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MPLIB::
* @author     H2T @ KIT
* @date       2021
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#pragma once

#include <boost/serialization/nvp.hpp>

#include "mplib/core/SystemState.h"

namespace mplib::representation
{
    class MPState : public core::SystemState
    {
    public:
        MPState();

        MPState(const MPState& state);
        MPState(const core::DVec& status);
        MPState(double pos, double vel);

        double pos;
        double vel;
        double& getValue(unsigned int i) override;
        const double& getValue(unsigned int i) const override;
        unsigned int size() const override;

        template <class Archive>
        void
        serialize(Archive& ar, const unsigned int /*fileVersion*/)
        {
            ar& boost::serialization::make_nvp("pos", pos);
            ar& boost::serialization::make_nvp("vel", vel);
        }
    };
    using NdimMPState = std::vector<MPState>;

    std::ostream& operator<<(std::ostream& str, const MPState& value);

} // namespace mplib::representation
