/**
* This file is part of DMP.
*
* DMP is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* DMP is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MPLIB::
* @author     H2T @ KIT
* @date       2021
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#pragma once

#include <boost/serialization/nvp.hpp>

#include "DiscreteDMP.h"

namespace mplib::representation::dmp
{
    class PeriodicDMP : public DiscreteDMP
    {
    public:
        PeriodicDMP(const std::string& name,
                    int kernelSize = defaultKernelSize,
                    double k = defaultK,
                    double d = defaultD,
                    double tau = 1.0,
                    double timestep = 1e-3);

        PeriodicDMP() = default;

        virtual std::unique_ptr<AbstractMovementPrimitive> clone(bool deep = false) const override;

        virtual DMPType getSpecificType() const override {
            return DMPType::Periodic;
        }

        void setAmpl(const unsigned int i, const double ampl);

        double getAmpl(const unsigned int i) const;

    protected:
        /**
         * @brief amplitudes are a vector of all amplitudes for different dimension of trajectories
         */
        core::DVec amplitudes;
        void getTrajectoryInfo(core::SampledTrajectory& trainingTrajectory) override;

    protected:
        // inherited from ODE
        void flow(double t, const core::DVec& x, core::DVec& out) override;

        // inherited from DiscreteDMP
        std::map<double, double> calcLearnableTerm(unsigned int trajDim,
                                          const core::DVec& canonicalValues,
                                          const core::SampledTrajectory& exampleTraj) override;

        friend class boost::serialization::access;
        template <class Archive>
        void
        serialize(Archive& ar, const unsigned int /*fileVersion*/)
        {
            ar& boost::serialization::make_nvp("amplitudes", amplitudes);
            auto& base = boost::serialization::base_object<DiscreteDMP>(*this);
            ar& boost::serialization::make_nvp("base", base);
        }
    };

} // namespace mplib::representation::dmp
