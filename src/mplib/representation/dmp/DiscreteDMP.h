/**
* This file is part of DMP.
*
* DMP is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* DMP is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MPLIB::
* @author     H2T @ KIT
* @date       2021
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#pragma once

#include <boost/serialization/nvp.hpp>

#include "mplib/math/ode/BasicODE.h"
#include "mplib/representation/AbstractMovementPrimitive.h"

namespace mplib::representation::dmp
{
    class DiscreteDMP : public AbstractMovementPrimitive, public math::ode::BasicODE
    {
    public:
        /**
         * @brief DiscreteDMP
         * @param D is the damping factor
         * @param tau is the temporal length factor used for training
         */
        DiscreteDMP(const std::string& name,
                    int kernelSize = defaultKernelSize,
                    double k = defaultK,
                    double d = defaultD,
                    double tau = 1.0,
                    double timestep = 1e-5);

        DiscreteDMP() = default;

        virtual std::unique_ptr<AbstractMovementPrimitive> clone(bool deep = false) const override;

        virtual MovementPrimitiveType getType() const override {
            return MovementPrimitiveType::DMP;
        }

        virtual Space getSpace() const override {
            return Space::Custom;
        }

        //! Returns the specific implementation of the movement primitive (e.g. "Periodic")
        virtual std::string getSpecificTypeStr() const override {
            return getSpecificType()._to_string();
        }

        virtual DMPType getSpecificType() const {
            return DMPType::Discrete;
        }

        void learnFromTrajectories(
            const std::vector<core::SampledTrajectory>& trainingTrajectories) override;
        NdimMPState calculateTrajectoryPoint(double t,
                                                      const core::DVec& goal,
                                                      double tCurrent,
                                                      const NdimMPState& currentStates,
                                                      core::DVec& canonicalValues,
                                                      double temporalFactor) override;
        core::SampledTrajectory calculateTrajectory(const core::DVec& timestamps,
                                                    const core::DVec& goal,
                                                    const core::DVec2d& initialStates,
                                                    const core::DVec& initialCanonicalValues,
                                                    double temporalFactor) override;

        void save(const std::string& /*filename*/) override
        {
            throw core::Exception("Not Implemented");
        }

        void load(const std::string& /*filename*/) override
        {
            throw core::Exception("Not Implemented");
        }

        void setSpringConstant(const double& k);

        void setDampingConstant(const double& d);

        void setDampingAndSpringConstant(const double& d);

        void setTemporalFactor(const double& tau);

        double getTemporalFactor() const;
        double getDampingFactor() const;
        double getSpringFactor() const;

        static constexpr int defaultKernelSize = 100;
        static constexpr double defaultK = 100;
        static constexpr double defaultD = 20;

        using Type = VMPType;

    protected:
        DiscreteDMP(std::unique_ptr<math::function_approximation::AbstractFunctionApproximation> baseFunctionApproximator,
                    std::unique_ptr<math::canonical_system::CanonicalSystem> canonicalSystem,
                    const std::string& name,
                    double k,
                    double d,
                    double timestep);

        virtual void getTrajectoryInfo(core::SampledTrajectory& trainingTrajectory);

        // inherited from ODE
        void flow(double t, const core::DVec& x, core::DVec& out) override;

        /**
         * @brief calcPerturbationForceSamples calculates the perturbation function dependent on the canonical system
         * that determines the characteristic shape of the result trajectory.
         * @param t
         * @return
         * @see learnFromTrajectory(), flow()
         */
        std::map<double, double> calcLearnableTerm(unsigned int trajDim,
                                          const core::DVec& canonicalStates,
                                          const core::SampledTrajectory& exampleTraj) override;

        void appendTo(DiscreteDMP* mp, bool deep) const;

        core::DVec scalar;

        /**
         * @brief K is the spring constant of the damped-spring-mass-system.
         */
        double K;

        /**
         * @brief D is damping of the damped-spring-mass-system.
         */
        double D;

        friend class boost::serialization::access;
        template <class Archive>
        void
        serialize(Archive& ar, const unsigned int /*fileVersion*/)
        {
            ar& boost::serialization::make_nvp("K", K);
            ar& boost::serialization::make_nvp("D", D);
            ar& boost::serialization::make_nvp("scalar", scalar);

            auto& mpbase = boost::serialization::base_object<AbstractMovementPrimitive>(*this);
            ar& boost::serialization::make_nvp("mpbase", mpbase);
            auto& odebase = boost::serialization::base_object<BasicODE>(*this);
            ar& boost::serialization::make_nvp("odebase", odebase);
        }
    };

} // namespace mplib::representation::dmp
