#include "DiscreteDMP.h"

#include "mplib/core/SampledTrajectory.h"
#include "mplib/math/canonical_system/CanonicalSystem.h"
#include "mplib/math/function_approximation/LocallyWeightedRegression.h"
#include "mplib/math/kernel/GaussianFunction.h"
#include "mplib/math/util/check_value.h"
#include "mplib/representation/MPState.h"

namespace mplib::representation::dmp
{

    DiscreteDMP::DiscreteDMP(const std::string& name,
                             int kernelSize,
                             double k,
                             double d,
                             double tau,
                             double timestep) :
        DiscreteDMP(std::make_unique<math::function_approximation::LocallyWeightedRegression<
                    math::kernel::GaussianFunction>>(kernelSize),
                    std::make_unique<math::canonical_system::CanonicalSystem>(tau),
                    name, k, d, timestep)
    {
        canonicalSystem->setEpsabs(timestep);
    }

    std::unique_ptr<AbstractMovementPrimitive> DiscreteDMP::clone(bool deep) const
    {
        auto mp = std::make_unique<DiscreteDMP>();
        appendTo(mp.get(), deep);
        return mp;
    }

    void
    DiscreteDMP::getTrajectoryInfo(core::SampledTrajectory& trainingTrajectory)
    {
        trajDim = trainingTrajectory.dim();
        startState.clear();
        goals.clear();
        scalar.clear();

        startState.resize(trajDim);
        goals.resize(trajDim);
        scalar.resize(trajDim);
        for (size_t i = 0; i < trajDim; i++)
        {
            startState[i].pos = trainingTrajectory.begin()->getPosition(i);
            startState[i].vel = trainingTrajectory.begin()->getDeriv(i, 1);
            goals[i] = trainingTrajectory.rbegin()->getPosition(i);
            scalar[i] = goals[i] - startState[i].pos;
        }
    }

    void
    DiscreteDMP::learnFromTrajectories(
        const std::vector<core::SampledTrajectory>& trainingTrajectories)
    {
        if (trainingTrajectories.size() > 1)
        {
            throw core::Exception()
                << "this DMP can only be trained with one trajectory (but with several dimensions)";
        }

        if (trainingTrajectories.size() == 0)
        {
            return;
        }

        core::SampledTrajectory trainingTrajectory = trainingTrajectories[0];
        getTrajectoryInfo(trainingTrajectory);
        trainingTrajectory =
            core::SampledTrajectory::normalizeTimestamps(trainingTrajectory, 0.0, 1.0);
        trainingTrajectory.differentiateDiscretly(2);
        core::DVec normTimestamps = trainingTrajectory.getTimestamps();

        std::vector<core::DVec> uValuesOrig;
        canonicalSystem->integrate(
            normTimestamps, core::DVec(1, canonicalSystem->getInitialValue()), uValuesOrig);
        core::DVec uValues;

        for (auto& i : uValuesOrig)
        {
            uValues.push_back(i[0]);
        }

        for (unsigned int dim = 0; dim < trajDim; dim++)
        {

            std::map<double, double> perturbationForceSamples =
                calcLearnableTerm(dim, uValues, trainingTrajectory);
            trainFunctionApproximator(dim, perturbationForceSamples);
        }
    }

    void
    DiscreteDMP::flow(double t, const core::DVec& x, core::DVec& out)
    {
        double u = x.at(0);
        core::DVec canonicalState(1);
        canonicalState[0] = u;
        canonicalSystem->flow(t, canonicalState, out);
        double tau = canonicalSystem->getTau();
        unsigned int dimensions = trajDim;

        if (dimensions > goals.size())
        {
            throw core::Exception("There are less trajectory goals than dimensions");
        }

        if (dimensions > startState.size())
        {
            throw core::Exception("There are less trajectory start positions than dimensions");
        }

        unsigned int offset = canonicalState.size();
        unsigned int curDim = 0;

        core::DVec uVec = core::DVec(1, u);
        while (curDim < dimensions)
        {
            double force = getLearnableTerm(curDim, uVec);
            double epsilon = 1e-5;
            double origDist = scalar[curDim] + epsilon;
            double currDist = goals[curDim] - startState[curDim].pos;
            double scaleFactor = currDist / origDist;

            unsigned int i = curDim * 2 + offset;
            double pos = x[i];
            double vel = x[i + 1];
            out[i++] = 1.0 / tau * (vel);
            out[i++] =
                1.0 / tau * (K * (goals[curDim] - pos) - (D * vel) + scaleFactor * force * u);
            curDim++;
        }
    }

    std::map<double, double>
    DiscreteDMP::calcLearnableTerm(unsigned int trajDim,
                                   const core::DVec& canonicalValues,
                                   const core::SampledTrajectory& exampleTraj)
    {
        std::map<double, double> result;

        if (exampleTraj.size() == 0)
        {
            throw core::Exception("Sample traj is empty");
        }

        if (canonicalValues.size() != exampleTraj.size())
        {
            throw core::Exception(
                "The amount of the canonicalValues do not match the exampleTrajectory");
        }

        // end position of example traj
        double xT = exampleTraj.rbegin()->getPosition(trajDim);
        auto itCanon = canonicalValues.begin();
        core::SampledTrajectory::ordered_view::const_iterator itTraj = exampleTraj.begin();

        for (int i = 0; itCanon != canonicalValues.end(); ++itCanon, ++i, itTraj++)
        {
            const double& pos = itTraj->getPosition(trajDim);
            const double& vel = itTraj->getDeriv(trajDim, 1);
            const double& acc = itTraj->getDeriv(trajDim, 2);

            double value = (-K * (xT - pos) + D * vel + acc) / *itCanon;
            math::util::checkValue(value);
            result[*itCanon] = value;
        }

        return result;
    }

    void DiscreteDMP::appendTo(DiscreteDMP* mp, bool deep) const
    {
        AbstractMovementPrimitive::appendTo(mp, deep);

        mp->K = K;
        mp->D = D;
        mp->setEpsabs(epsabs());

        if (deep)
        {
            mp->goals = goals;
            mp->scalar = scalar;
            mp->startState = startState;
        }
    }

    NdimMPState
    DiscreteDMP::calculateTrajectoryPoint(double t,
                                          const core::DVec& goal,
                                          double tInit,
                                          const NdimMPState& currentStates,
                                          core::DVec& canonicalValues,
                                          double temporalFactor)
    {
        math::ode::BasicODE::setDim(1 + trajDim * 2);
        core::DVec initialODEConditions = canonicalValues;
        core::DVec initialStatesVec = core::SystemState::convertStatesToArray(currentStates);
        initialODEConditions.insert(
            initialODEConditions.end(), initialStatesVec.begin(), initialStatesVec.end());

        setTemporalFactor(temporalFactor);
        setGoals(goal);

        core::DVec rawResult;
        math::ode::BasicODE::integrate(t, tInit, initialODEConditions, rawResult, false);

        canonicalValues.assign(rawResult.begin(), rawResult.begin() + 1);
        rawResult.erase(rawResult.begin());
        NdimMPState result = core::SystemState::convertArrayToStates<MPState>(rawResult);
        return result;
    }

    core::SampledTrajectory
    DiscreteDMP::calculateTrajectory(const core::DVec& timestamps,
                                     const core::DVec& goal,
                                     const core::DVec2d& initialStates,
                                     const core::DVec& initialCanonicalValues,
                                     double temporalFactor)
    {
        NdimMPState initState =
            core::SystemState::convert2DArrayToStates<MPState>(initialStates);
        setInitialState(initState);

        return AbstractMovementPrimitive::calculateTrajectory(
            timestamps, goal, initialStates, initialCanonicalValues, temporalFactor);
    }

    void
    DiscreteDMP::setSpringConstant(const double& k)
    {
        this->K = k;
    }

    void
    DiscreteDMP::setDampingConstant(const double& d)
    {
        this->D = d;
    }

    void
    DiscreteDMP::setDampingAndSpringConstant(const double& d)
    {
        this->D = d;
        K = (d / 2) * (d / 2);
    }

    void
    DiscreteDMP::setTemporalFactor(const double& tau)
    {
        canonicalSystem->setTau(tau);
    }

    double
    DiscreteDMP::getTemporalFactor() const
    {
        return canonicalSystem->getTau();
    }

    double
    DiscreteDMP::getDampingFactor() const
    {
        return D;
    }

    double
    DiscreteDMP::getSpringFactor() const
    {
        return K;
    }

    DiscreteDMP::DiscreteDMP(std::unique_ptr<math::function_approximation::AbstractFunctionApproximation> baseFunctionApproximator,
                             std::unique_ptr<math::canonical_system::CanonicalSystem> canonicalSystem,
                             const std::string &name, double k, double d, double timestep) :
        AbstractMovementPrimitive(std::move(baseFunctionApproximator), std::move(canonicalSystem), name),
        K(k), D(d)
    {
        this->setEpsabs(timestep);
    }

} // namespace mplib::representation::dmp
