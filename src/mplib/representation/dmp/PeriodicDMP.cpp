#include "PeriodicDMP.h"

#include "mplib/core/SampledTrajectory.h"
#include "mplib/math/canonical_system/PeriodicCanonicalSystem.h"
#include "mplib/math/function_approximation/LocallyWeightedRegression.h"
#include "mplib/math/kernel/PeriodicGaussianFunction.h"
#include "mplib/representation/MPState.h"

namespace mplib::representation::dmp
{

    PeriodicDMP::PeriodicDMP(const std::string& name,
                             int kernelSize,
                             double k,
                             double d,
                             double tau,
                             double timestep) :
        DiscreteDMP(std::make_unique<math::function_approximation::LocallyWeightedRegression<
                    math::kernel::PeriodicGaussianFunction>>(kernelSize),
                    std::make_unique<math::canonical_system::PeriodicCanonicalSystem>(tau),
                    name, k, d, timestep)
    {
    }

    std::unique_ptr<AbstractMovementPrimitive> PeriodicDMP::clone(bool deep) const
    {
        auto mp = std::make_unique<PeriodicDMP>();
        appendTo(mp.get(), deep);
        if (deep) mp->amplitudes = amplitudes;
        return mp;
    }

    void
    PeriodicDMP::setAmpl(const unsigned int i, const double ampl)
    {
        if (i >= amplitudes.size())
        {
            std::cout << "Warning: out of the range of amplitudes" << std::endl;
            amplitudes.push_back(ampl);
            return;
        }
        amplitudes[i] = ampl;
    }

    double
    PeriodicDMP::getAmpl(const unsigned int i) const
    {
        return amplitudes[i];
    }

    void
    PeriodicDMP::getTrajectoryInfo(core::SampledTrajectory& trainingTrajectory)
    {
        trajDim = trainingTrajectory.dim();

        startState.clear();
        scalar.clear();
        startState.resize(trajDim);
        scalar.resize(trajDim);
        for (size_t i = 0; i < trajDim; i++)
        {
            startState[i].pos = trainingTrajectory.begin()->getPosition(i);
            startState[i].vel = trainingTrajectory.begin()->getDeriv(i, 1);
            scalar[i] = trainingTrajectory.rbegin()->getPosition(i) -
                        trainingTrajectory.begin()->getPosition(i);
        }

        amplitudes = core::DVec(core::DVec(trajDim, 1.0));
        goals = trainingTrajectory.calcAnchorPoint();
    }

    void
    PeriodicDMP::flow(double t, const core::DVec& x, core::DVec& out)
    {
        double u = x.at(0);
        core::DVec canonicalState(1);
        canonicalState[0] = u;
        canonicalSystem->flow(t, canonicalState, out);
        double tau = canonicalSystem->getTau();
        unsigned int dimensions = trajDim;

        if (dimensions > goals.size())
        {
            throw core::Exception("There are less trajectory goals than dimensions");
        }

        if (dimensions > startState.size())
        {
            throw core::Exception("There are less trajectory start positions than dimensions");
        }

        unsigned int offset = canonicalState.size();
        unsigned int curDim = 0;

        while (curDim < dimensions)
        {
            double force = getLearnableTerm(curDim, core::DVec(1, u));
            unsigned int i = curDim * 2 + offset;
            double pos = x[i];
            double vel = x[i + 1];
            out[i++] = 1.0 / tau * (vel);
            out[i++] =
                1.0 / tau * (K * (goals[curDim] - pos) - (D * vel) + amplitudes[curDim] * force);
            curDim++;
        }
    }

    std::map<double, double>
    PeriodicDMP::calcLearnableTerm(unsigned int trajDim,
                                   const core::DVec& canonicalValues,
                                   const core::SampledTrajectory& exampleTraj)
    {
        std::map<double, double> result;

        if (exampleTraj.size() == 0)
        {
            throw core::Exception("Sample traj is empty");
        }

        if (canonicalValues.size() != exampleTraj.size())
        {
            throw core::Exception(
                "The amount of the canonicalValues do not match the exampleTrajectory");
        }

        double tau = canonicalSystem->getTau();

        auto itCanon = canonicalValues.begin();
        core::SampledTrajectory::ordered_view::const_iterator itTraj = exampleTraj.begin();

        for (int i = 0; itCanon != canonicalValues.end(); ++itCanon, ++i, itTraj++)
        {
            const double& pos = itTraj->getPosition(trajDim);
            const double& vel = itTraj->getDeriv(trajDim, 1);
            const double& acc = itTraj->getDeriv(trajDim, 2);
            double value =
                (-K * (goals[trajDim] - pos) + D * vel + tau * acc) / amplitudes[trajDim];
            math::util::checkValue(value);
            result[*itCanon] = value;
        }

        return result;
    }

} // namespace mplib::representation::dmp
