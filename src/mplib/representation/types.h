#pragma once

#include <mplib/external/enum.h>

namespace mplib::representation
{

    BETTER_ENUM(MovementPrimitiveType, int, DMP, VMP)

    BETTER_ENUM(DMPType, int, Discrete, Periodic)
    BETTER_ENUM(VMPType, int, Gaussian, PrincipalComponent, TaskSpaceGaussian, TaskSpacePrincipalComponent)

}
