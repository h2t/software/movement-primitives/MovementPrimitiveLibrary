#include "TaskSpaceGaussianVMP.h"

namespace mplib::representation::vmp
{
    TaskSpaceGaussianVMP::TaskSpaceGaussianVMP(int componentSize,
                                               const std::string& name,
                                               int kernelSize,
                                               BaseMode baseMode,
                                               double tau,
                                               double timestep) :
        VMP(name, kernelSize, baseMode, tau, timestep), GaussianVMP(componentSize)
    {
    }

    std::unique_ptr<AbstractMovementPrimitive> TaskSpaceGaussianVMP::clone(bool deep) const
    {
        auto mp = std::make_unique<TaskSpaceGaussianVMP>();
        appendTo(mp.get(), deep);
        if (deep) mp->viaPoses = viaPoses;
        return mp;
    }

    VMPType TaskSpaceGaussianVMP::getSpecificType() const {
        return VMPType::TaskSpaceGaussian;
    }

} // namespace mplib::representation::vmp
