#include "TaskSpacePrincipalComponentVMP.h"

namespace mplib::representation::vmp
{

    TaskSpacePrincipalComponentVMP::TaskSpacePrincipalComponentVMP(const std::string& name,
                                                                   int kernelSize,
                                                                   BaseMode baseMode,
                                                                   double tau,
                                                                   double timestep) :
        VMP(name, kernelSize, baseMode, tau, timestep)
    {
    }

    TaskSpacePrincipalComponentVMP::TaskSpacePrincipalComponentVMP(const std::string &name, const core::DVec2d &weights) :
        VMP(name, weights[0].size(), defaultBaseMode, 1.0, 1e-3)
    {
        trajDim = weights.size();
        numOfDemos = 1; // TODO
        setWeights(weights);
    }

    std::unique_ptr<AbstractMovementPrimitive> TaskSpacePrincipalComponentVMP::clone(bool deep) const
    {
        auto mp = std::make_unique<TaskSpacePrincipalComponentVMP>();
        appendTo(mp.get(), deep);
        if (deep) mp->viaPoses = viaPoses;
        return mp;
    }

    VMPType TaskSpacePrincipalComponentVMP::getSpecificType() const {
        return VMPType::TaskSpacePrincipalComponent;
    }

} // namespace mplib::representation::vmp
