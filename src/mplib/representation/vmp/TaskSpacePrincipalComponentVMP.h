#pragma once

#include <map>

#include <boost/type_traits/is_virtual_base_of.hpp>

#include "PrincipalComponentVMP.h"
#include "TaskSpaceVMP.h"

namespace mplib::representation::vmp
{
    class TaskSpacePrincipalComponentVMP : public PrincipalComponentVMP, public TaskSpaceVMP
    {

    public:
        using PrincipalComponentVMP::getForce;
        using PrincipalComponentVMP::learnModelFromData;

        /**
         * @brief TSPCVMP: Taskspace Principal Components Viapoints Movement Primitive
         * @param kernelSize: the kernel size for LocallyWeightedRegression
         * (If using other regression model, it needs to be explicitly set with setBaseFunctionApproximator (check learnVMP example for GPR).)
         * @param baseMode: the mode for the elementary trajectory (VMP_BASE_LINEAR and VMP_BASE_MINJERK)
         * @param tau is the temporal length factor used for training
         * @param timestep is the timestep of the canonical system
         */
        TaskSpacePrincipalComponentVMP(const std::string& name,
                                       int kernelSize = defaultKernelSize,
                                       BaseMode baseMode = defaultBaseMode,
                                       double tau = 1.0,
                                       double timestep = 1e-3);

        TaskSpacePrincipalComponentVMP(const std::string& name, const core::DVec2d& weights);

        TaskSpacePrincipalComponentVMP() = default;

        virtual std::unique_ptr<AbstractMovementPrimitive> clone(bool deep = false) const override;

        virtual Space getSpace() const override {
            return Space::TaskSpace7D;
        }

        virtual VMPType getSpecificType() const override;

    private:
        friend class boost::serialization::access;
        template <class Archive>
        void
        serialize(Archive& ar, const unsigned int /*file_version*/)
        {
            auto& pcvmpbase = boost::serialization::base_object<PrincipalComponentVMP>(*this);
            ar& boost::serialization::make_nvp("pcvmpbase", pcvmpbase);
            ar& boost::serialization::make_nvp("viaPoses", viaPoses);
            ar& boost::serialization::make_nvp("overFlyFactor", overFlyFactor);
        }
    };

} // namespace mplib::representation::vmp

namespace boost
{
    template <>
    struct is_virtual_base_of<mplib::representation::vmp::TaskSpaceVMP,
                              mplib::representation::vmp::TaskSpacePrincipalComponentVMP> :
        public mpl::true_
    {
    };
    template <>
    struct is_virtual_base_of<mplib::representation::vmp::PrincipalComponentVMP,
                              mplib::representation::vmp::TaskSpacePrincipalComponentVMP> :
        public mpl::true_
    {
    };
} // namespace boost
