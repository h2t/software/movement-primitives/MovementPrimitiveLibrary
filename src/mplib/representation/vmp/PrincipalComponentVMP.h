#pragma once

#include "VMP.h"

namespace mplib::representation::vmp
{
    class PrincipalComponentVMP : virtual public VMP
    {

    public:
        using AbstractMovementPrimitive::calculateTrajectory;

        /**
         * @brief PCVMP: Principal Components Via-points Movement Primitive
         * Learn the principal components of all trajectories
         * @param kernelSize: the kernel size for LocallyWeightedRegression
         * (If using other regression model, it needs to be explicitly set with setBaseFunctionApproximator (check learnVMP example for GPR).)
         * @param baseMode: the mode for the elementary trajectory (VMP_BASE_LINEAR and VMP_BASE_MINJERK)
         * @param tau is the temporal length factor used for training
         * @param timestep is the timestep of the canonical system
         */
        PrincipalComponentVMP(const std::string& name,
                              int kernelSize = defaultKernelSize,
                              BaseMode baseMode = defaultBaseMode,
                              double tau = 1.0,
                              double timestep = 1e-3);

        PrincipalComponentVMP() = default;

        virtual std::unique_ptr<AbstractMovementPrimitive> clone(bool deep = false) const override;

        virtual Space getSpace() const override;

        virtual VMPType getSpecificType() const override;

        void setStyleRatios(const core::DVec& ratios);

    protected:
        arma::vec getForce(double u, arma::vec& derivative) override;
        void learnModelFromData(const core::DVec& uValues,
                                std::vector<arma::mat> fMatList) override;

        void appendTo(PrincipalComponentVMP* mp, bool deep) const;

        std::vector<arma::mat> scoreMatList;
        std::vector<arma::vec> styleParas;
        std::vector<arma::vec> getStyleParasWithRatio(const core::DVec& ratios);

    private:
        friend class boost::serialization::access;
        template <class Archive>
        void
        serialize(Archive& ar, const unsigned int /*file_version*/)
        {
            if (Archive::is_saving::value)
            {
                std::vector<std::string> vector;
                vector.reserve(styleParas.size());
                for (auto& t : styleParas)
                {
                    vector.push_back(saveArma(t));
                }
                ar& boost::serialization::make_nvp("styleParas", vector);
            }
            else
            {
                std::vector<std::string> vector;
                ar& boost::serialization::make_nvp("styleParas", vector);
                styleParas.resize(vector.size());
                for (std::size_t idx = 0; idx < vector.size(); idx++)
                {
                    loadArma(styleParas.at(idx), vector.at(idx));
                }
            }

            if (Archive::is_saving::value)
            {
                std::vector<std::string> vector;
                vector.reserve(scoreMatList.size());
                for (auto& t : scoreMatList)
                {
                    vector.push_back(saveArma(t));
                }
                ar& boost::serialization::make_nvp("scoreMatList", vector);
            }
            else
            {
                std::vector<std::string> vector;
                ar& boost::serialization::make_nvp("scoreMatList", vector);
                scoreMatList.resize(vector.size());
                for (std::size_t idx = 0; idx < vector.size(); idx++)
                {
                    loadArma(scoreMatList.at(idx), vector.at(idx));
                }
            }

            auto& base = boost::serialization::base_object<VMP>(*this);
            ar& boost::serialization::make_nvp("vmpbase", base);
        }
    };

} // namespace mplib::representation::vmp
