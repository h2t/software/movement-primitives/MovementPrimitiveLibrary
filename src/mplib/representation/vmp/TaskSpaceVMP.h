#pragma once

#include <Eigen/Dense>

#include "VMP.h"
#include "ViaPose.h"

namespace mplib::representation::vmp
{
    bool flip(NdimMPState& state, const Eigen::Quaterniond &reference);

    class TaskSpaceVMP : virtual public VMP
    {

    public:
        using VMP::setViaPoint;
        using VMP::calculateDesiredState;
        using VMP::calculateTrajectory;

        TaskSpaceVMP() = default;

        virtual ~TaskSpaceVMP() = default;

        void learnFromTrajectories(
            const std::vector<core::SampledTrajectory>& trainingTrajectories) override;

        void learnFromTaskSpaceTrajectory(const std::map<double, Eigen::Matrix4d>& trainingTrajectories,
                                          bool useReferenceQuaternion = true);

        void learnFromTaskSpaceTrajectories(const std::vector<std::map<double, Eigen::Matrix4d>>& trainingTrajectories,
                                            bool useReferenceQuaternion = true);

        virtual core::SampledTrajectory
        calculateTrajectory(const core::DVec& timestamps,
                            const Eigen::Matrix4d& goalPose,
                            const Eigen::Matrix4d& initialPose,
                            const core::DVec& initialCanonicalValues,
                            double temporalFactor);

        void setViaPoint(double u, const Eigen::Matrix4d &pose);

        void setViaPoint(double u, const core::DVec2d& point) override;

        const std::map<double, NdimMPState>& getViaPoints() const;

        void removeViaPoints() override;

        NdimMPState
        calculateDesiredState(double canVal, const NdimMPState& currentState) override;

        Eigen::Matrix4d calculatePose(double canVal);

        Eigen::Matrix4d calculatePose(double canVal, const Eigen::Matrix4d &currentPose);

        Eigen::Matrix4d getStartPose() const;

        Eigen::Matrix4d getGoalPose() const;

        void setStartPose(const Eigen::Matrix4d &start, bool prepareExecution = true);

        void setGoalPose(const Eigen::Matrix4d &goal, bool prepareExecution = true);

        const std::map<double, std::shared_ptr<ViaPose>>& getViaPoses() const {
            return viaPoses;
        }
        double overFlyFactor = 0;

    protected:
        std::map<double, std::shared_ptr<ViaPose>> viaPoses;

        arma::mat calcQuatForce(int dim,
                                const core::DVec& canonicalValues,
                                const core::DVec& timestamps,
                                const core::SampledTrajectory& exampleTraj);

    private:
        Eigen::Quaterniond
        quatSlerp(double t, const Eigen::Quaterniond& q0, const Eigen::Quaterniond& q1);
        Eigen::Quaterniond
        dquatSlerp(double t, const Eigen::Quaterniond& q0, const Eigen::Quaterniond& q1);

        friend class boost::serialization::access;
        template <class Archive>
        void
        serialize(Archive& ar, const unsigned int /*file_version*/)
        {
            auto& vmpbase = boost::serialization::base_object<VMP>(*this);
            ar& boost::serialization::make_nvp("vmpbase", vmpbase);

            ar& boost::serialization::make_nvp("viaPoses", viaPoses);
            ar& boost::serialization::make_nvp("overFlyFactor", overFlyFactor);
        }
    };

} // namespace mplib::representation::vmp
