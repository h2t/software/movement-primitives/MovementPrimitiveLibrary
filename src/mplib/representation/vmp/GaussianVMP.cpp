#include "GaussianVMP.h"

#include <armadillo>

#include "mplib/core/Exception.h"

namespace mplib::representation::vmp
{
    GaussianVMP::GaussianVMP(int numOfComps,
                             const std::string& name,
                             int kernelSize,
                             BaseMode baseMode,
                             double tau,
                             double timestep) :
        VMP(name, kernelSize, baseMode, tau, timestep), numOfComps(numOfComps)
    {
    }

    GaussianVMP::GaussianVMP(int numOfComps) : numOfComps(numOfComps)
    {
    }

    std::unique_ptr<AbstractMovementPrimitive> GaussianVMP::clone(bool deep) const
    {
        auto mp = std::make_unique<GaussianVMP>();
        appendTo(mp.get(), deep);
        return mp;
    }

    Space GaussianVMP::getSpace() const {
        return Space::Custom;
    }

    VMPType GaussianVMP::getSpecificType() const {
        return VMPType::Gaussian;
    }

    void
    GaussianVMP::setIsSample(bool isSample)
    {
        this->isSample = isSample;
    }

    void
    GaussianVMP::setCurrCompId(int compId)
    {
        if (viaPoints.size() > 2)
        {
            std::cout << "WARNING: please change the component ID before setting viapoints. "
                      << "Otherwise, it cannot guarantee that viapoints are gone through"
                      << std::endl;
        }
        if (compId > numOfComps)
        {
            throw core::Exception() << "CompId exceeds the number of components";
        }
        currCompId = compId;
    }

    arma::vec
    GaussianVMP::getForce(double u, arma::vec& derivative)
    {
        arma::vec fVec(trajDim);
        derivative.clear();
        derivative.resize(trajDim);

        for (size_t dim = 0; dim < trajDim; ++dim)
        {
            arma::vec weight;
            if (!isSample)
            {
                weight = meanVecs[dim].col(currCompId);
            }
            else
            {
                arma::gmm_full gmm;
                gmm.set_means(meanVecs[dim]);
                gmm.set_fcovs(covMats[dim]);
                gmm.set_hefts(mixingCoeffs[dim]);
                weight = gmm.generate();
            }

            core::DVec dweight = cvtArmaVecToDVec(weight);
            setWeights(dim, dweight);
            core::DVec uVec = core::DVec(1, u);
            fVec(dim) = getLearnableTerm(dim, uVec);
            derivative(dim) = getLearnableTermDeriv(dim, uVec);
        }

        return fVec;
    }

    void
    GaussianVMP::learnModelFromData(const core::DVec& uValues, std::vector<arma::mat> fMatList)
    {
        meanVecs.clear();
        covMats.clear();
        mixingCoeffs.clear();

        //For each dimension, learn a Gaussian Mixture Model;
        for (size_t dim = 0; dim < trajDim; ++dim)
        {
            arma::mat wdata(getKernelSize(), numOfDemos);
            arma::running_stat_vec<arma::vec> stats(true);
            for (size_t demoId = 0; demoId < numOfDemos; ++demoId)
            {
                arma::mat fMat = fMatList[demoId];
                std::map<double, double> forceSamples = createDoubleMap(uValues, fMat.row(dim).t());
                trainFunctionApproximator(dim, forceSamples);
                core::DVec2d weights = getWeights();
                arma::vec weight(weights.at(dim));
                wdata.col(demoId) = weight;
                stats(weight);
            }

            if (numOfComps == 1)
            {
                arma::vec meanVec = stats.mean();
                arma::mat meanMat(meanVec.n_rows, 1, arma::fill::zeros);
                meanMat.col(0) = meanVec;
                meanVecs.push_back(meanMat);
                arma::cube covMat(meanVec.n_rows, meanVec.n_rows, 1, arma::fill::zeros);
                arma::mat cov_mat = stats.cov();
                covMat.slice(0) = cov_mat;
                covMats.push_back(covMat);
                arma::rowvec mixing(1, 1, arma::fill::ones);
                mixingCoeffs.push_back(mixing);
            }
            else
            {
                arma::gmm_full gmm;
                gmm.learn(
                    wdata, numOfComps, arma::eucl_dist, arma::random_subset, 10, 10, 1e-10, true);
                meanVecs.push_back(gmm.means);
                covMats.push_back(gmm.fcovs);
                mixingCoeffs.push_back(gmm.hefts);
            }
        }
    }

    void GaussianVMP::appendTo(GaussianVMP *mp, bool deep) const
    {
        VMP::appendTo(mp, deep);
        mp->numOfComps = numOfComps;
        if (deep)
        {
            mp->meanVecs = meanVecs;
            mp->covMats = covMats;
            mp->mixingCoeffs = mixingCoeffs;
            mp->isSample = isSample;
            mp->currCompId = currCompId;
        }
    }

} // namespace mplib::representation::vmp
