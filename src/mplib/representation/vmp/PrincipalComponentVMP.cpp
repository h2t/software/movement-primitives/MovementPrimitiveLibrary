#include "PrincipalComponentVMP.h"

#include <armadillo>

#include "mplib/core/Exception.h"

namespace mplib::representation::vmp
{

    std::vector<arma::vec>
    PrincipalComponentVMP::getStyleParasWithRatio(const core::DVec& ratios)
    {
        if (ratios.size() != numOfDemos)
        {
            throw core::Exception(
                "getStyeParasWithRatio: ratios size should be the same as number of demos");
        }

        std::vector<arma::vec> result;
        for (auto sMat : scoreMatList)
        {
            arma::vec coef(trajDim, arma::fill::zeros);
            for (size_t j = 0; j < numOfDemos; ++j)
            {
                coef += ratios[j] * sMat.col(j);
            }
            result.push_back(coef);
        }

        return result;
    }

    PrincipalComponentVMP::PrincipalComponentVMP(const std::string& name,
                                                 int kernelSize,
                                                 BaseMode baseMode,
                                                 double tau,
                                                 double timestep) :
        VMP(name, kernelSize, baseMode, tau, timestep)
    {
    }

    std::unique_ptr<AbstractMovementPrimitive> PrincipalComponentVMP::clone(bool deep) const
    {
        auto mp = std::make_unique<PrincipalComponentVMP>();
        appendTo(mp.get(), deep);
        return mp;
    }

    Space PrincipalComponentVMP::getSpace() const {
        return Space::Custom;
    }

    VMPType PrincipalComponentVMP::getSpecificType() const {
        return VMPType::PrincipalComponent;
    }

    void
    PrincipalComponentVMP::setStyleRatios(const core::DVec& ratios)
    {
        styleParas = getStyleParasWithRatio(ratios);
    }

    arma::vec
    PrincipalComponentVMP::getForce(double u, arma::vec& derivative)
    {
        arma::vec fVec(trajDim);
        derivative.clear();
        derivative.resize(trajDim);
        unsigned int funcId = 0;
        core::DVec uVec = core::DVec(1, u);
        for (size_t dim = 0; dim < trajDim; ++dim)
        {
            double avgForce = getLearnableTerm(funcId, uVec);
            double avgDerivForce = getLearnableTermDeriv(funcId, uVec);

            funcId++;
            fVec(dim) = avgForce;
            derivative(dim) = avgDerivForce;
        }

        if (numOfDemos == 1)
        {
            return fVec;
        }

        if (styleParas.size() == 0)
        {
            core::DVec ratios;
            for (size_t i = 0; i < numOfDemos; ++i)
            {
                ratios.push_back(1 / (float)numOfDemos);
            }
            styleParas = getStyleParasWithRatio(ratios);
        }

        size_t numOfPCs = scoreMatList.size();
        for (size_t i = 0; i < numOfPCs; ++i)
        {
            double pcForce = getLearnableTerm(funcId, uVec);
            double pcForceDeriv = getLearnableTermDeriv(funcId, uVec);
            funcId++;

            fVec += styleParas[i] * pcForce;
            derivative += styleParas[i] * pcForceDeriv;
        }

        return fVec;
    }

    void
    PrincipalComponentVMP::learnModelFromData(const core::DVec& uValues,
                                              std::vector<arma::mat> fMatList)
    {
        arma::mat avgfMat(trajDim, uValues.size(), arma::fill::zeros);
        for (size_t i = 0; i < numOfDemos; ++i)
        {
            avgfMat = avgfMat + fMatList[i];
        }

        avgfMat /= float(numOfDemos);
        unsigned int funcId = 0;

        for (size_t dim = 0; dim < trajDim; ++dim)
        {
            std::map<double, double> forceSamples = createDoubleMap(uValues, avgfMat.row(dim).t());
            trainFunctionApproximator(funcId, forceSamples);
            funcId++;
        }

        if (numOfDemos > 1)
        {
            std::cout << "More than one trajectories are found, PCA is now being called ..."
                      << std::endl;

            arma::mat G(uValues.size(), uValues.size(), arma::fill::zeros);
            for (size_t i = 0; i < numOfDemos; ++i)
            {
                arma::mat fMat = fMatList[i] - avgfMat;
                fMatList[i] = fMat;
                G = G + fMat.t() * fMat;
            }
            G /= double(numOfDemos);

            arma::mat U;
            arma::vec SVec;
            arma::mat V;
            svd(U, SVec, V, G);
            double totalsVal = sum(SVec);

            double ratio = 0;
            double sVal = 0;
            int idOfPCs = 0;
            scoreMatList.clear();
            while (ratio < 0.99)
            {
                sVal += SVec(idOfPCs);
                ratio = sVal / totalsVal;

                int colId = idOfPCs;
                arma::mat sMat(trajDim, numOfDemos);
                for (size_t i = 0; i < numOfDemos; ++i)
                {
                    sMat.col(i) = fMatList[i] * V.col(colId);
                }
                scoreMatList.push_back(sMat);

                std::map<double, double> forceSamples = createDoubleMap(uValues, V.col(colId));
                trainFunctionApproximator(funcId, forceSamples);
                funcId++;
                idOfPCs++;
            }

            std::cout << "number of PCs: " << idOfPCs << std::endl;
        }
    }

    void PrincipalComponentVMP::appendTo(PrincipalComponentVMP *mp, bool deep) const
    {
        VMP::appendTo(mp, deep);
        if (deep)
        {
            mp->scoreMatList = scoreMatList;
            mp->styleParas = styleParas;
        }
    }

} // namespace mplib::representation::vmp
