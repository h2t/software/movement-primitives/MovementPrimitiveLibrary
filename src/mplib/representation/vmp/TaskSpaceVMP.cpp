#include "TaskSpaceVMP.h"

#include <armadillo>

#include "ViaPose.h"
#include "mplib/core/Exception.h"
#include "mplib/core/SampledTrajectory.h"
#include "mplib/math/canonical_system/CanonicalSystem.h"
#include "mplib/representation/MPState.h"
#include <eigen3/Eigen/Dense>

namespace mplib::representation::vmp
{
    void
    TaskSpaceVMP::learnFromTrajectories(
        const std::vector<core::SampledTrajectory>& trainingTrajectories)
    {
        if (trainingTrajectories.size() == 0)
        {
            throw core::Exception() << "The training trajectories is empty ... ";
        }

        numOfDemos = trainingTrajectories.size();
        core::SampledTrajectory trainingTrajectory = trainingTrajectories[0];
        trainingTrajectory = core::SampledTrajectory::normalizeTimestamps(trainingTrajectory, 0, 1);
        getTrajectoryInfo(trainingTrajectory);

        if (trajDim != 7)
        {
            throw core::Exception() << "The training trajectories should have 7 dimensions";
        }

        core::DVec normTimestamps = trainingTrajectory.getTimestamps();
        std::vector<core::DVec> uValuesOrig;
        canonicalSystem->integrate(normTimestamps, core::DVec(1, 1.0), uValuesOrig);

        core::DVec uValues;
        for (auto& i : uValuesOrig)
        {
            uValues.push_back(i[0]);
        }

        std::vector<arma::mat> fMatList;
        fMatList.reserve(numOfDemos);
        arma::mat avgfMat(trajDim, uValues.size(), arma::fill::zeros);
        for (size_t i = 0; i < numOfDemos; ++i)
        {
            core::SampledTrajectory trainingTrajectory = trainingTrajectories[i];
            trainingTrajectory =
                core::SampledTrajectory::normalizeTimestamps(trainingTrajectory, 0, 1);

            arma::mat fMat(trajDim,
                           uValues.size()); // forceMat: D x T (D: dimension, T: sample points)
            for (size_t dim = 0; dim < 3; ++dim)
            {
                arma::vec force = calcForce(dim, uValues, normTimestamps, trainingTrajectory);
                fMat.row(dim) = force.t();
            }

            arma::mat forceMat = calcQuatForce(3, uValues, normTimestamps, trainingTrajectory);
            fMat.rows(3, 6) = forceMat.t();
            fMatList.push_back(fMat);
            avgfMat = avgfMat + fMat;
        }

        learnModelFromData(uValues, fMatList);
    }

    void TaskSpaceVMP::learnFromTaskSpaceTrajectory(const std::map<double, Eigen::Matrix4d> &trainingTajectory,
                                                    bool useReferenceQuaternion) {
        learnFromTrajectories({ core::SampledTrajectory(trainingTajectory, useReferenceQuaternion) });
    }

    void TaskSpaceVMP::learnFromTaskSpaceTrajectories(const std::vector<std::map<double, Eigen::Matrix4d> > &trainingTrajectories,
                                                      bool useReferenceQuaternion) {
        std::vector<core::SampledTrajectory> trajectories;
        for (const auto &trainingTajectory : trainingTrajectories) {
            trajectories.push_back(core::SampledTrajectory(trainingTajectory, useReferenceQuaternion));
        }
        learnFromTrajectories(trajectories);
    }

    core::SampledTrajectory TaskSpaceVMP::calculateTrajectory(const core::DVec &timestamps,
                                                              const Eigen::Matrix4d &goalPose,
                                                              const Eigen::Matrix4d &initialPose,
                                                              const core::DVec &initialCanonicalValues,
                                                              double temporalFactor) {
        return AbstractMovementPrimitive::calculateTrajectory(timestamps, math::util::toDVec<double>(goalPose),
                                   core::SystemState::convertStatesTo2DArray(
                                       core::SystemState::convertPosArrayToStates<MPState>(
                                           math::util::toDVec(initialPose))),
                                   initialCanonicalValues, temporalFactor);
    }

    void TaskSpaceVMP::setViaPoint(double u, const Eigen::Matrix4d &pose) {
        setViaPoint(u, math::util::toDVec(pose));
    }

    void
    TaskSpaceVMP::setViaPoint(double u, const core::DVec2d& point)
    {
        arma::vec dfVec;
        arma::vec force = getForce(u, dfVec);

        viaPoses.insert_or_assign(u, std::make_shared<ViaPose>(point, force));
        realViaPoints.insert_or_assign(u, core::SystemState::convert2DArrayToStates<MPState>(point));

        // flip all via points from the front to back (1.0 -> 0.0)
        for (auto it = std::next(viaPoses.rbegin()); it != viaPoses.rend(); it++)
        {
            if (!math::util::check(it->second->viaOrientation, std::prev(it)->second->viaOrientation))
                it->second->negateViaOrientation();
        }
    }

    const std::map<double, NdimMPState>&
    TaskSpaceVMP::getViaPoints() const
    {
        return realViaPoints;
    }

    void
    TaskSpaceVMP::removeViaPoints()
    {
        VMP::removeViaPoints();
        viaPoses.clear();
    }

    Eigen::Quaterniond
    TaskSpaceVMP::quatSlerp(double t, const Eigen::Quaterniond& q0, const Eigen::Quaterniond& q1)
    {
        double cosHalfTheta = q0.w() * q1.w() + q0.x() * q1.x() + q0.y() * q1.y() + q0.z() * q1.z();

        Eigen::Quaterniond q1x = q1;
        if (cosHalfTheta < 0)
        {
            q1x = q1.coeffs() * (-1);
        }

        if (fabs(cosHalfTheta) >= 1.0)
        {
            return q0;
        }

        double halfTheta = acos(cosHalfTheta);
        double sinHalfTheta = sqrt(1.0 - cosHalfTheta * cosHalfTheta);

        Eigen::Quaterniond result;
        if (fabs(sinHalfTheta) < 0.001)
        {
            result = (1 - t) * q0.coeffs() + t * q1x.coeffs();
        }
        else
        {
            double ratioA = sin((1 - t) * halfTheta) / sinHalfTheta;
            double ratioB = sin(t * halfTheta) / sinHalfTheta;

            result = ratioA * q0.coeffs() + ratioB * q1x.coeffs();
        }

        return result;
    }

    Eigen::Quaterniond
    TaskSpaceVMP::dquatSlerp(double t, const Eigen::Quaterniond& q0, const Eigen::Quaterniond& q1)
    {
        double cosHalfTheta = q0.w() * q1.w() + q0.x() * q1.x() + q0.y() * q1.y() + q0.z() * q1.z();

        Eigen::Quaterniond q1x = q1;
        if (cosHalfTheta < 0)
        {
            q1x = q1.coeffs() * (-1);
        }

        Eigen::Quaterniond result;
        if (fabs(cosHalfTheta) >= 1.0)
        {
            result = result.coeffs() * 0;
            return result;
        }

        double halfTheta = acos(cosHalfTheta);
        double sinHalfTheta = sqrt(1.0 - cosHalfTheta * cosHalfTheta);

        if (fabs(sinHalfTheta) < 0.001)
        {
            result = q1x.coeffs() - q0.coeffs();
        }
        else
        {
            double ratioA = -halfTheta * cos((1 - t) * halfTheta) / sinHalfTheta;
            double ratioB = halfTheta * cos(t * halfTheta) / sinHalfTheta;
            result = ratioA * q0.coeffs() + ratioB * q1x.coeffs();
        }

        return result;
    }

    arma::mat
    TaskSpaceVMP::calcQuatForce(int dim,
                                const core::DVec& canonicalValues,
                                const core::DVec& timestamps,
                                const core::SampledTrajectory& exampleTraj)
    {
        Eigen::Quaterniond q0, qT;
        q0.w() = exampleTraj.begin()->getPosition(dim);
        q0.x() = exampleTraj.begin()->getPosition(dim + 1);
        q0.y() = exampleTraj.begin()->getPosition(dim + 2);
        q0.z() = exampleTraj.begin()->getPosition(dim + 3);
        qT.w() = exampleTraj.rbegin()->getPosition(dim);
        qT.x() = exampleTraj.rbegin()->getPosition(dim + 1);
        qT.y() = exampleTraj.rbegin()->getPosition(dim + 2);
        qT.z() = exampleTraj.rbegin()->getPosition(dim + 3);

        auto itCanon = canonicalValues.begin();

        arma::mat result(canonicalValues.size(), 4);
        int i = 0;

        arma::vec mjcoef_t(6, arma::fill::zeros);
        if (baseMode == VMP_BASE_MINJERK)
        {
            arma::vec cond = {1, 0, 0, 0, 0, 0};
            mjcoef_t = calcMinJerkCoef(canonicalValues[0], cond);
        }

        for (; itCanon != canonicalValues.end(); ++itCanon, ++i)
        {
            Eigen::Quaterniond currentQ;
            currentQ.w() = exampleTraj.getState(timestamps.at(i), dim);
            currentQ.x() = exampleTraj.getState(timestamps.at(i), dim + 1);
            currentQ.y() = exampleTraj.getState(timestamps.at(i), dim + 2);
            currentQ.z() = exampleTraj.getState(timestamps.at(i), dim + 3);

            double u = *itCanon;

            Eigen::Quaterniond lpart;

            if (baseMode == VMP_BASE_MINJERK)
            {
                arma::vec u_vec = {1, u, pow(u, 2), pow(u, 3), pow(u, 4), pow(u, 5)};
                double tratio = dot(u_vec, mjcoef_t);
                lpart = quatSlerp(tratio, q0, qT);
            }
            else
            {
                double tratio = timestamps.at(i) / timestamps.at(timestamps.size() - 1);
                lpart = quatSlerp(tratio, q0, qT);
            }

            Eigen::Quaterniond value = lpart.inverse() * currentQ;
            result(i, 0) = value.w();
            result(i, 1) = value.x();
            result(i, 2) = value.y();
            result(i, 3) = value.z();
        }

        if (result.has_nan() || result.has_inf())
        {
            throw core::Exception("calculated quaternion force is nan or inf");
        }

        return result;
    }

    NdimMPState
    TaskSpaceVMP::calculateDesiredState(double u, const NdimMPState& currentState)
    {
        if (u < canonicalSystem->getUMin())
        {
            u = canonicalSystem->getUMin() + 1e-10;
        }

        auto it_next = viaPoses.lower_bound(u);
        if (it_next == viaPoses.end())
        {
            return currentState;
        }

        auto it_begin = std::next(it_next);

        std::shared_ptr<ViaPose> cinits = it_next->second;
        double u0 = it_next->first;
        std::shared_ptr<ViaPose> cgoals = (--it_next)->second;
        double u1 = it_next->first;

        arma::vec dfVec;
        arma::vec fVec = getForce(u, dfVec);
        NdimMPState desiredState = currentState;

        // calculate force for position:
        if (overFlyFactor == 0 || baseMode == VMP_BASE_MINJERK)
        {
            for (size_t i = 0; i < 3; ++i)
            {
                double force = fVec(i);
                double dforce = dfVec(i);
                double x0 = cinits->viaPosition(i) - cinits->viaLinearForce(i);
                double x1 = cgoals->viaPosition(i) - cgoals->viaLinearForce(i);
                double lpart, ctarget, cdtarget;

                if (baseMode == VMP_BASE_MINJERK)
                {
                    arma::vec cond = {x1, 0, 0, x0, 0, 0};
                    arma::vec mjcoef_t = calcMinJerkCoef(u0 - u1, cond);
                    double cu = u - u1;
                    arma::vec u_vec = {1, cu, pow(cu, 2), pow(cu, 3), pow(cu, 4), pow(cu, 5)};
                    lpart = dot(u_vec, mjcoef_t);
                    ctarget = lpart + force;

                    arma::vec u_dvec = {
                        0, 1, 2 * cu, 3 * pow(cu, 2), 4 * pow(cu, 3), 5 * pow(cu, 4)};
                    cdtarget = -dot(u_dvec, mjcoef_t) - dforce;
                }
                else
                {
                    double b = (x1 * u0 - x0 * u1) / (u0 - u1);
                    double a;
                    if (u0 != 0)
                    {
                        a = (x0 - b) / u0;
                    }
                    else
                    {
                        a = (x1 - b) / u1;
                    }

                    lpart = a * u + b;
                    ctarget = lpart + force;
                    cdtarget = -a - dforce;
                }

                desiredState[i].pos = ctarget;
                desiredState[i].vel = cdtarget;
            }
        }
        else // use overfly.
        {
            Eigen::Vector3f x0 = cinits->viaPosition - cinits->viaLinearForce;
            Eigen::Vector3f x1 = cgoals->viaPosition - cgoals->viaLinearForce;

            bool existPrevious = true;
            bool existNext = true;
            if (it_next == viaPoses.begin())
            {
                existNext = false;
            }
            else
            {
                it_next--;
            }

            if (it_begin == viaPoses.end())
            {
                existPrevious = false;
            }

            Eigen::Vector3f vel = (x1 - x0) / (u1 - u0);
            Eigen::Vector3f lpartvec = (u - u0) * vel + x0;

            if (u > u0 - overFlyFactor && existPrevious)
            {
                double upre = it_begin->first;
                std::shared_ptr<ViaPose> prePoint = it_begin->second;
                Eigen::Vector3f xpre = prePoint->viaPosition - prePoint->viaLinearForce;

                Eigen::Vector3f vel0 = (x0 - xpre) / (u0 - upre);
                Eigen::Vector3f vel1 = (x1 - x0) / (u1 - u0);

                double a = (u0 + overFlyFactor - u) / (2 * overFlyFactor);
                vel = a * vel1 + (1 - a) * vel0;

                double um = u0 + overFlyFactor;

                Eigen::Vector3f linearPart =
                    (um * vel1 + 2 * um * vel0 - 2 * u0 * vel0 - um * vel0) * (u - um);
                Eigen::Vector3f quadPart = 0.5 * (vel0 - vel1) * (u - um) * (u + um);
                Eigen::Vector3f integralPart = (linearPart + quadPart) / (2 * overFlyFactor);

                lpartvec = xpre + (um - upre) * vel0 + integralPart;
            }

            if (u < u1 + overFlyFactor && existNext)
            {
                std::shared_ptr<ViaPose> nextPoint = it_next->second;
                Eigen::Vector3f x2 = nextPoint->viaPosition - nextPoint->viaLinearForce;
                double u2 = it_next->first;

                Eigen::Vector3f vel0 = (x1 - x0) / (u1 - u0);
                Eigen::Vector3f vel1 = (x2 - x1) / (u2 - u1);

                double a = (u1 + overFlyFactor - u) / (2 * overFlyFactor);
                vel = a * vel1 + (1 - a) * vel0;

                double um = u1 + overFlyFactor;

                Eigen::Vector3f linearPart =
                    (um * vel1 + 2 * um * vel0 - 2 * u1 * vel0 - um * vel0) * (u - um);
                Eigen::Vector3f quadPart = 0.5 * (vel0 - vel1) * (u - um) * (u + um);
                Eigen::Vector3f integralPart = (linearPart + quadPart) / (2 * overFlyFactor);

                lpartvec = x0 + (um - u0) * vel0 + integralPart;
            }

            for (size_t i = 0; i < 3; ++i)
            {
                desiredState[i].pos = lpartvec(i) + fVec(i);
                desiredState[i].vel = -vel(i) - dfVec(i);
            }
        }

        // calculate force for quaternion.
        Eigen::Quaterniond quatForce;
        Eigen::Quaterniond dquatForce;
        Eigen::Quaterniond q0 = cinits->viaOrientation * cinits->viaRotationForce.inverse();
        Eigen::Quaterniond q1 = cgoals->viaOrientation * cgoals->viaRotationForce.inverse();

        quatForce.w() = fVec(3);
        dquatForce.w() = dfVec(3);
        quatForce.x() = fVec(4);
        dquatForce.x() = dfVec(4);
        quatForce.y() = fVec(5);
        dquatForce.y() = dfVec(5);
        quatForce.z() = fVec(6);
        dquatForce.z() = dfVec(6);

        double tratio;
        double dtratio;

        if (baseMode == VMP_BASE_MINJERK)
        {
            arma::vec cond = {1, 0, 0, 0, 0, 0};
            arma::vec mjcoef_t = calcMinJerkCoef(u0 - u1, cond);
            double cu = u - u1;
            arma::vec u_vec = {1, cu, pow(cu, 2), pow(cu, 3), pow(cu, 4), pow(cu, 5)};
            tratio = dot(u_vec, mjcoef_t);

            arma::vec u_dvec = {0, 1, 2 * cu, 3 * pow(cu, 2), 4 * pow(cu, 3), 5 * pow(cu, 4)};
            dtratio = -dot(u_dvec, mjcoef_t);
        }
        else
        {
            tratio = (u - u0) / (u1 - u0);
            dtratio = -1 / (u1 - u0);
        }

        Eigen::Quaterniond dqslerp(dtratio * dquatSlerp(tratio, q0, q1).coeffs());
        Eigen::Quaterniond qslerp = quatSlerp(tratio, q0, q1);

        Eigen::Quaterniond qderi(dqslerp.coeffs().cwiseProduct(quatForce.coeffs()) + qslerp.coeffs().cwiseProduct(dquatForce.coeffs()));
        Eigen::Quaterniond qtarget = qslerp * quatForce;
        qtarget.normalize();

        desiredState[3].pos = qtarget.w();
        desiredState[3].vel = qderi.w();

        desiredState[4].pos = qtarget.x();
        desiredState[4].vel = qderi.x();

        desiredState[5].pos = qtarget.y();
        desiredState[5].vel = qderi.y();

        desiredState[6].pos = qtarget.z();
        desiredState[6].vel = qderi.z();

        return desiredState;
    }

    Eigen::Matrix4d TaskSpaceVMP::calculatePose(double canVal) {
        core::DVec2d dvec2d = calculateDesiredState(canVal);
        return math::util::toPose<double>(dvec2d);
    }

    Eigen::Matrix4d TaskSpaceVMP::calculatePose(double canVal, const Eigen::Matrix4d &currentPose) {
        representation::NdimMPState currentState = core::SystemState::convertPosArrayToStates<representation::MPState>(math::util::toDVec(currentPose));
        representation::NdimMPState state = calculateDesiredState(canVal, currentState);
        return math::util::toPose<double>(core::SystemState::convertStatesTo2DArray(state));
    }

    Eigen::Matrix4d TaskSpaceVMP::getStartPose() const
    {
        return math::util::toPose<double>(getStart());
    }

    Eigen::Matrix4d TaskSpaceVMP::getGoalPose() const
    {
        return math::util::toPose<double>(getGoals());
    }

    void TaskSpaceVMP::setStartPose(const Eigen::Matrix4d &start, bool prepare) {
        setInitialState(core::SystemState::convertPosArrayToStates<MPState>(math::util::toDVec(start)));
        if (prepare)
            this->prepareExecution();
    }

    void TaskSpaceVMP::setGoalPose(const Eigen::Matrix4d &goal, bool prepare) {
        setGoals(math::util::toDVec(goal));
        if (prepare)
            this->prepareExecution();
    }

    bool flip(NdimMPState& state, const Eigen::Quaterniond &reference)
    {
        core::DVec2d dvec = core::SystemState::convertStatesTo2DArray(state);
        if (math::util::flip(dvec, reference))
        {
            state = core::SystemState::convert2DArrayToStates<MPState>(dvec);
            return true;
        }
        else return false;
    }

} // namespace mplib::representation::vmp
