#include "ViaPose.h"

#include <armadillo>

#include "mplib/math/util/eigen_arithmetic.h"

namespace mplib::representation::vmp
{

    ViaPose::ViaPose()
    {
        viaPosition.setZero();
        viaLinearForce.setZero();
        viaOrientation.setIdentity();
        viaLinearForce.setIdentity();
    }

    ViaPose::ViaPose(const core::DVec2d &viaPoseVec, const arma::vec &force) :
        ViaPose(math::util::toPose<double>(viaPoseVec), force)
    {
    }

    ViaPose::ViaPose(const core::DVec& viaPoseVec, const arma::vec& force) :
        ViaPose(math::util::toPose<double>(viaPoseVec), force)
    {
    }

    ViaPose::ViaPose(const Eigen::Matrix4d &viaPose, const arma::vec &force)
    {
        viaPosition = math::util::toPosition(viaPose).cast<float>();
        viaOrientation = math::util::toQuat(viaPose);
        setForce(force);
    }

    void
    ViaPose::negateViaOrientation()
    {
        math::util::flip(viaOrientation);
    }

    void ViaPose::setForce(const arma::vec &force) {
        if (force.size() != 7)
        {
            throw core::Exception("Error: ViaPoseVec should have 7 dimensions !!!");
        }
        viaLinearForce << force(0), force(1), force(2);
        viaRotationForce.w() = force(3);
        viaRotationForce.x() = force(4);
        viaRotationForce.y() = force(5);
        viaRotationForce.z() = force(6);
    }

} // namespace mplib::representation::vmp
