#pragma once

#include <boost/type_traits/is_virtual_base_of.hpp>

#include "GaussianVMP.h"
#include "TaskSpaceVMP.h"

namespace mplib::representation::vmp
{
    class TaskSpaceGaussianVMP : public GaussianVMP, public TaskSpaceVMP
    {

    public:
        using GaussianVMP::getForce;
        using GaussianVMP::learnModelFromData;

        /**
         * @brief TSGaussVMP: Taskspace Gaussian Viapoints Movement Primitive
         * @param componentSize: the number of Gaussian components
         * @param kernelSize: the kernel size for LocallyWeightedRegression
         * (If using other regression model, it needs to be explicitly set with setBaseFunctionApproximator (check learnVMP example for GPR).)
         * @param baseMode: the mode for the elementary trajectory (VMP_BASE_LINEAR and VMP_BASE_MINJERK)
         * @param tau is the temporal length factor used for training
         * @param timestep is the timestep of the canonical system
         */
        TaskSpaceGaussianVMP(int componentSize,
                             const std::string& name,
                             int kernelSize = defaultKernelSize,
                             BaseMode baseMode = defaultBaseMode,
                             double tau = 1.0,
                             double timestep = 1e-3);

        TaskSpaceGaussianVMP() = default;

        virtual std::unique_ptr<AbstractMovementPrimitive> clone(bool deep = false) const override;

        virtual Space getSpace() const override {
            return Space::TaskSpace7D;
        }

        virtual VMPType getSpecificType() const override;

    private:
        friend class boost::serialization::access;
        template <class Archive>
        void
        serialize(Archive& ar, const unsigned int /*file_version*/)
        {
            auto& gvmpbase = boost::serialization::base_object<GaussianVMP>(*this);
            ar& boost::serialization::make_nvp("gvmpbase", gvmpbase);
            ar& boost::serialization::make_nvp("viaPoses", viaPoses);
            ar& boost::serialization::make_nvp("overFlyFactor", overFlyFactor);
        }
    };

} // namespace mplib::representation::vmp

namespace boost
{
    template <>
    struct is_virtual_base_of<mplib::representation::vmp::TaskSpaceVMP,
                              mplib::representation::vmp::TaskSpaceGaussianVMP> : public mpl::true_
    {
    };
    template <>
    struct is_virtual_base_of<mplib::representation::vmp::GaussianVMP,
                              mplib::representation::vmp::TaskSpaceGaussianVMP> : public mpl::true_
    {
    };
} // namespace boost
