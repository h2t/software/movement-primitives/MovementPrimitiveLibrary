#pragma once

#include "mplib/math/canonical_system/LinearDecayCanonicalSystem.h"
#include "mplib/representation/AbstractMovementPrimitive.h"

namespace mplib::representation::vmp
{
    enum BaseMode {
        VMP_BASE_LINEAR = 1,
        VMP_BASE_MINJERK = 2
    };

    class VMP : public AbstractMovementPrimitive
    {

    public:
        using AbstractMovementPrimitive::calculateTrajectory;

        /**
         * @brief VMP: Viapoints Movement Primitive
         */
        VMP(const std::string& name,
            int kernelSize = defaultKernelSize,
            BaseMode baseMode = defaultBaseMode,
            double tau = 1.0,
            double timestep = 1e-3);

        VMP() = default;

        virtual ~VMP() = default;

        virtual MovementPrimitiveType getType() const override;
        virtual std::string getSpecificTypeStr() const override;
        virtual VMPType getSpecificType() const = 0;

        void save(const std::string& filename) override;

        void load(const std::string& filename) override;

        void learnFromTrajectories(
            const std::vector<core::SampledTrajectory>& trainingTrajectories) override;

        NdimMPState calculateTrajectoryPoint(double t,
                                                      const core::DVec& goal,
                                                      double tCurrent,
                                                      const NdimMPState& currentStates,
                                                      core::DVec& currentCanonicalValues,
                                                      double temporalFactor) override;

        void setTemporalFactor(const double& tau);

        virtual void setViaPoint(double u, const core::DVec2d& point);

        void setViaPoint(double u, const core::DVec& point);

        void setViaPoint(const std::pair<double, NdimMPState>& viapoint);
        void setViaPoint(double u, const NdimMPState& viapoint);
        const std::map<double, NdimMPState>& getViaPoints() const;

        void prepareExecution(const core::DVec& goal,
                              const NdimMPState& initialStates,
                              double temporalFactor = 1.0);

        void prepareExecution(const core::DVec& goal,
                              const core::DVec2d& initialStates,
                              double temporalFactor = 1.0);
        void prepareExecution();

        /**
         * @brief getForceTerm gets the value of the target model on the canonical value.
         */
        core::DVec getForceTerm(double u);

        virtual void removeViaPoints();

        virtual NdimMPState
        calculateDesiredState(double canVal, const NdimMPState& currentState);

        core::DVec2d calculateDesiredState(double u,
                                           const core::DVec2d& currentState = core::DVec2d());

        static constexpr int defaultKernelSize = 100;
        static constexpr BaseMode defaultBaseMode = representation::vmp::BaseMode::VMP_BASE_LINEAR;
        static constexpr int defaultComponentSize = 1;

        using Type = VMPType;
    protected:
        VMP(const std::string& name,
            std::unique_ptr<math::function_approximation::AbstractFunctionApproximation> baseFunctionApproximator,
            std::unique_ptr<math::canonical_system::CanonicalSystem> canonicalSystem,
            BaseMode baseMode);

        std::map<double, double> calcLearnableTerm(unsigned int trajDim,
                                          const core::DVec& canonicalStates,
                                          const core::SampledTrajectory& exampleTraj) override;

        void getTrajectoryInfo(core::SampledTrajectory& trainingTrajectory);

        BaseMode baseMode;

        // realViaPoints: realvp(x)= f(x) + h(x) is the real points
        // that the trajectory goes through
        std::map<double, NdimMPState> realViaPoints;

        //viaPoints: vp = realvp(x) - f(x) is the viapoint that
        // the elementary trajectory should go through
        std::map<double, NdimMPState> viaPoints;

        /**
         * @brief getForce gets the value of the target model on the canonical value.
         */
        virtual arma::vec getForce(double u, arma::vec& derivative) = 0;

        /**
         * @brief learnModelFromData learns a model from data
         * @param fMatList is a list of target matrix fMat
         * fMat is a D x T matrix (D: trajectory dimension, T: total timesteps.
         * Each fMat is corresponding to one demonstration
         */
        virtual void learnModelFromData(const core::DVec& uValues,
                                        std::vector<arma::mat> fMatList) = 0;

        virtual arma::vec calcForce(unsigned int dim,
                                    const core::DVec& canonicalValues,
                                    const core::DVec& timestamps,
                                    const core::SampledTrajectory& exampleTraj);
        double getLearnableTermDeriv(unsigned int funcID, core::DVec canonicalStates) const;

        size_t numOfDemos;

        template <class T>
        std::string
        saveArma(T& t) const
        {
            std::stringstream stream(std::stringstream::out | std::stringstream::binary);
            t.save(stream, arma::file_type::raw_ascii);
            return stream.str();
        }

        template <class T>
        void
        loadArma(T& t, std::string s)
        {
            std::stringstream stream(s, std::stringstream::in | std::stringstream::binary);
            t.load(stream, arma::file_type::raw_ascii);
        }

        void appendTo(VMP* mp, bool deep) const;

    private:
        friend class boost::serialization::access;
        template <class Archive>
        void
        serialize(Archive& ar, const unsigned int /*file_version*/)
        {
            ar.template register_type<math::canonical_system::LinearDecayCanonicalSystem>();

            ar& boost::serialization::make_nvp("numOfDemos", numOfDemos);
            ar& boost::serialization::make_nvp("baseMode", baseMode);
            ar& boost::serialization::make_nvp("viaPoints", viaPoints);

            auto& base = boost::serialization::base_object<AbstractMovementPrimitive>(*this);
            ar& boost::serialization::make_nvp("mpbase", base);
        }
    };

    arma::vec calcMinJerkCoef(double terminalTime, const arma::vec& condition);

    std::map<double, double> createDoubleMap(const core::DVec& canonicalVals, const arma::vec& sampleVec);

    core::DVec cvtArmaVecToDVec(const arma::vec& avec);
} // namespace mplib::representation::vmp
