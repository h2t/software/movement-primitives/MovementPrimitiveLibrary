#pragma once

#include <boost/serialization/nvp.hpp>

#include <Eigen/Geometry>

#include "mplib/core/types.h"
#include "mplib/math/util/armadillo_fwd.h"

namespace mplib::representation::vmp
{
    class ViaPose
    {
    public:
        ViaPose();

        ViaPose(const core::DVec2d& viaPoseVec, const arma::vec& force);

        ViaPose(const core::DVec& viaPoseVec, const arma::vec& force);

        ViaPose(const Eigen::Matrix4d& viaPose, const arma::vec& force);

        void negateViaOrientation();

        Eigen::Vector3f viaPosition;
        Eigen::Vector3f viaLinearForce;
        Eigen::Quaterniond viaOrientation;
        Eigen::Quaterniond viaRotationForce;

        void setForce(const arma::vec& force);

    private:        
        friend class boost::serialization::access;
        template <class Archive>
        void
        serialize(Archive& ar, const unsigned int /*file_version*/)
        {
            if (Archive::is_saving::value)
            {
                core::DVec vector = {viaPosition.x(), viaPosition.y(), viaPosition.z()};
                ar& boost::serialization::make_nvp("viaPosition", vector);
            }
            else
            {
                core::DVec vector;
                ar& boost::serialization::make_nvp("viaPosition", vector);
                viaPosition = Eigen::Vector3f(vector[0], vector[1], vector[2]);
            }

            if (Archive::is_saving::value)
            {
                core::DVec vector = {viaLinearForce.x(), viaLinearForce.y(), viaLinearForce.z()};
                ar& boost::serialization::make_nvp("viaLinearForce", vector);
            }
            else
            {
                core::DVec vector;
                ar& boost::serialization::make_nvp("viaLinearForce", vector);
                viaLinearForce = Eigen::Vector3f(vector[0], vector[1], vector[2]);
            }

            if (Archive::is_saving::value)
            {
                core::DVec vector = {
                    viaOrientation.w(), viaOrientation.x(), viaOrientation.y(), viaOrientation.z()};
                ar& boost::serialization::make_nvp("viaOrientation", vector);
            }
            else
            {
                core::DVec vector;
                ar& boost::serialization::make_nvp("viaOrientation", vector);
                viaOrientation = Eigen::Quaterniond(vector[0], vector[1], vector[2], vector[3]);
            }

            if (Archive::is_saving::value)
            {
                core::DVec vector = {viaRotationForce.w(),
                                     viaRotationForce.x(),
                                     viaRotationForce.y(),
                                     viaRotationForce.z()};
                ar& boost::serialization::make_nvp("viaRotationForce", vector);
            }
            else
            {
                core::DVec vector;
                ar& boost::serialization::make_nvp("viaRotationForce", vector);
                viaRotationForce = Eigen::Quaterniond(vector[0], vector[1], vector[2], vector[3]);
            }
        }
    };

} // namespace mplib::representation::vmp
