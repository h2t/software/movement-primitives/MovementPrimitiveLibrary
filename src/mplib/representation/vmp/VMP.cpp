#include "VMP.h"

#include <armadillo>

#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/vector.hpp>

#include "mplib/core/SampledTrajectory.h"
#include "mplib/math/function_approximation/LocallyWeightedRegression.h"
#include "mplib/representation/MPState.h"
#include <eigen3/Eigen/Dense>

namespace mplib::representation::vmp
{

    VMP::VMP(const std::string& name, int kernelSize, BaseMode baseMode, double tau, double timestep) :
        VMP(name,
            std::make_unique<math::function_approximation::LocallyWeightedRegression<
            math::kernel::GaussianFunction>>(kernelSize),
            std::make_unique<math::canonical_system::LinearDecayCanonicalSystem>(tau),
            baseMode)
    {
        canonicalSystem->setEpsabs(timestep);
    }

    VMP::VMP(const std::string& name,
             std::unique_ptr<math::function_approximation::AbstractFunctionApproximation> baseFunctionApproximator,
             std::unique_ptr<math::canonical_system::CanonicalSystem> canonicalSystem,
             BaseMode baseMode) :
        AbstractMovementPrimitive(std::move(baseFunctionApproximator), std::move(canonicalSystem), name),
        baseMode(baseMode)
    {
    }

    MovementPrimitiveType VMP::getType() const {
        return MovementPrimitiveType::VMP;
    }

    std::string VMP::getSpecificTypeStr() const {
        return getSpecificType()._to_string();
    }

    void
    VMP::save(const std::string& filename)
    {
        std::ofstream ofs(filename);
        boost::archive::xml_oarchive ar(ofs);
        ar << boost::serialization::make_nvp("vmp", *this);
    }

    void
    VMP::load(const std::string& filename)
    {
        std::ifstream ifs(filename);
        boost::archive::xml_iarchive ai(ifs);
        ai >> boost::serialization::make_nvp("vmp", *this);
    }

    void
    VMP::learnFromTrajectories(const std::vector<core::SampledTrajectory>& trainingTrajectories)
    {
        if (trainingTrajectories.size() == 0)
        {
            throw core::Exception() << "The training trajectories is empty ... ";
        }

        numOfDemos = trainingTrajectories.size();
        core::SampledTrajectory trainingTrajectory = trainingTrajectories[0];
        trainingTrajectory = core::SampledTrajectory::normalizeTimestamps(trainingTrajectory, 0, 1);
        getTrajectoryInfo(trainingTrajectory);

        core::DVec normTimestamps = trainingTrajectory.getTimestamps();
        std::vector<core::DVec> uValuesOrig;
        canonicalSystem->integrate(normTimestamps, core::DVec(1, 1.0), uValuesOrig);

        core::DVec uValues;
        for (auto& i : uValuesOrig)
        {
            uValues.push_back(i[0]);
        }

        // calculate forces for each demonstrations and each dimensions
        std::vector<arma::mat> fMatList;
        for (size_t i = 0; i < numOfDemos; ++i)
        {
            core::SampledTrajectory trainingTrajectory = trainingTrajectories[i];
            trainingTrajectory =
                core::SampledTrajectory::normalizeTimestamps(trainingTrajectory, 0, 1);

            arma::mat fMat(trajDim,
                           uValues.size()); // forceMat: D x T (D: dimension, T: sample points)
            for (size_t dim = 0; dim < trajDim; ++dim)
            {
                arma::vec force = calcForce(dim, uValues, normTimestamps, trainingTrajectory);
                fMat.row(dim) = force.t();
            }
            fMatList.push_back(fMat);
        }

        learnModelFromData(uValues, fMatList);

        core::DVec2d initialState;
        for (size_t i = 0; i < trainingTrajectories[0].dim(); i++)
        {
            core::DVec state;
            state.push_back(trainingTrajectories[0].begin()->getPosition(i));
            state.push_back(0.0);
            initialState.push_back(state);
        }
        prepareExecution(goals, initialState);
    }

    arma::vec
    VMP::calcForce(unsigned int dim,
                   const core::DVec& canonicalValues,
                   const core::DVec& timestamps,
                   const core::SampledTrajectory& exampleTraj)
    {
        double x0 = exampleTraj.begin()->getPosition(dim);
        double xT = exampleTraj.rbegin()->getPosition(dim);

        auto itCanon = canonicalValues.begin();
        int i = 0;
        arma::vec result(canonicalValues.size());
        arma::vec mjcoef_t(6, arma::fill::zeros);
        if (baseMode == VMP_BASE_MINJERK)
        {
            arma::vec cond{xT, 0, 0, x0, 0, 0};
            mjcoef_t = calcMinJerkCoef(canonicalValues[0], cond);
        }

        for (; itCanon != canonicalValues.end(); ++itCanon, ++i)
        {
            const double& pos = exampleTraj.getState(timestamps[i], dim);

            double u = *itCanon;
            double lpart;

            if (baseMode == VMP_BASE_MINJERK)
            {
                arma::vec u_vec = {1, u, pow(u, 2), pow(u, 3), pow(u, 4), pow(u, 5)};
                lpart = dot(u_vec, mjcoef_t);
            }
            else
            {
                lpart = -(xT - x0) * u + xT;
            }

            double value = (pos - lpart);
            math::util::checkValue(value);
            result(i) = value;
        }

        return result;
    }

    double
    VMP::getLearnableTermDeriv(unsigned int funcID, core::DVec canonicalStates) const
    {
        if (canonicalStates.at(0) == 0)
        {
            throw core::Exception("The canonical state must not be 0!");
        }

        return (*getFunctionApproximator(funcID))
            .getDeriv(core::DVec(1, uprocess(canonicalStates.at(0))))
            .at(0);
    }

    void VMP::appendTo(VMP* mp, bool deep) const
    {
        AbstractMovementPrimitive::appendTo(mp, deep);

        mp->baseMode = baseMode;

        if (deep)
        {
            mp->realViaPoints = realViaPoints;
            mp->viaPoints = viaPoints;
            mp->goals = goals;
            mp->startState = startState;
            mp->numOfDemos = numOfDemos;
        }
    }

    NdimMPState
    VMP::calculateTrajectoryPoint(double t,
                                  const core::DVec& goal,
                                  double tInit,
                                  const NdimMPState& currentStates,
                                  core::DVec& canonicalValues,
                                  double temporalFactor)
    {
        if (goal.size() != trajDim)
        {
            throw core::Exception("The goal's dimension is not correct");
        }

        if (currentStates.size() != trajDim)
        {
            throw core::Exception("The startPositions' dimension is not correct.");
        }

        if (canonicalValues[0] == canonicalSystem->getInitialValue())
        {
            prepareExecution(goal, currentStates, temporalFactor);
        }

        NdimMPState res = calculateDesiredState(canonicalValues[0], currentStates);
        core::DVec canValOut;
        canonicalSystem->integrate(t, tInit, canonicalValues, canValOut);
        canonicalValues = canValOut;

        return res;
    }

    void
    VMP::setTemporalFactor(const double& tau)
    {
        canonicalSystem->setTau(tau);
    }

    NdimMPState
    VMP::calculateDesiredState(double u, const NdimMPState& currentState)
    {
        if (u < canonicalSystem->getUMin())
        {
            u = canonicalSystem->getUMin() + 1e-10;
        }

        auto it_next = viaPoints.lower_bound(u);
        if (it_next == viaPoints.end())
        {
            return currentState;
        }
        NdimMPState cinits = it_next->second;
        double u0 = it_next->first;
        NdimMPState cgoals = (--it_next)->second;
        double u1 = it_next->first;

        arma::vec dfVec;
        arma::vec fVec = getForce(u, dfVec);

        NdimMPState res;
        for (size_t dim = 0; dim < trajDim; ++dim)
        {
            double force = fVec(dim);
            double dforce = dfVec(dim);
            double target, dtarget, lpart;
            double x0 = cinits[dim].pos;
            double x1 = cgoals[dim].pos;

            switch (baseMode)
            {
                case VMP_BASE_MINJERK:
                {
                    double v0 = cinits[dim].vel;
                    double v1 = cgoals[dim].vel;
                    arma::vec cond = {x1, v1, 0, x0, v0, 0};
                    arma::vec mjcoef_t = calcMinJerkCoef(u0 - u1, cond);
                    double cu = u - u1;
                    arma::vec u_vec = {1, cu, pow(cu, 2), pow(cu, 3), pow(cu, 4), pow(cu, 5)};
                    arma::vec u_dvec = {
                        0, 1, 2 * cu, 3 * pow(cu, 2), 4 * pow(cu, 3), 5 * pow(cu, 4)};
                    lpart = dot(u_vec, mjcoef_t);

                    target = lpart + force;
                    dtarget = -dot(u_dvec, mjcoef_t) - dforce;
                    break;
                }
                case VMP_BASE_LINEAR:
                {
                    double b = (x1 * u0 - x0 * u1) / (u0 - u1);
                    double a;
                    if (u0 != 0)
                    {
                        a = (x0 - b) / u0;
                    }
                    else
                    {
                        a = (x1 - b) / u1;
                    }

                    lpart = a * u + b;
                    target = lpart + force;
                    dtarget = -a - dforce;
                    break;
                }
                default:
                    throw core::Exception() << "unknow mode";
            }
            MPState state(target, dtarget);
            res.push_back(state);
        }

        return res;
    }

    core::DVec2d
    VMP::calculateDesiredState(double u, const core::DVec2d& currentState)
    {
        NdimMPState stateVec;
        if (currentState.empty())
        {
            stateVec = startState;
        }
        else
        {
            stateVec = core::SystemState::convert2DArrayToStates<MPState>(currentState);
        }

        NdimMPState desiredStateVec = calculateDesiredState(u, stateVec);
        core::DVec2d res = core::SystemState::convertStatesTo2DArray<MPState>(desiredStateVec);
        return res;
    }

    std::map<double, double>
    VMP::calcLearnableTerm(unsigned int trajDim,
                           const core::DVec& canonicalStates,
                           const core::SampledTrajectory& exampleTraj)
    {
        arma::vec force =
            calcForce(trajDim, canonicalStates, exampleTraj.getTimestamps(), exampleTraj);
        return createDoubleMap(canonicalStates, force);
    }

    void
    VMP::getTrajectoryInfo(core::SampledTrajectory& trainingTrajectory)
    {
        trajDim = trainingTrajectory.dim();
        startState.clear();
        goals.clear();

        startState.resize(trajDim);
        goals.resize(trajDim);
        for (size_t i = 0; i < trajDim; i++)
        {
            startState[i].pos = trainingTrajectory.begin()->getPosition(i);
            startState[i].vel = trainingTrajectory.begin()->getDeriv(i, 1);
            goals[i] = trainingTrajectory.rbegin()->getPosition(i);
        }
    }

    core::DVec VMP::getForceTerm(double u) {
        arma::vec derivative;
        core::DVec forceVec;
        arma::vec force = getForce(u, derivative);
        for (double f : force) {
            forceVec.push_back(f);
        }
        return forceVec;
    }

    void
    VMP::setViaPoint(double u, const core::DVec2d& pointVec)
    {
        arma::vec dfVec;
        arma::vec force = getForce(u, dfVec);

        NdimMPState point = core::SystemState::convert2DArrayToStates<MPState>(pointVec);
        NdimMPState origPoint = point;

        if (point.size() != force.n_rows)
        {
            throw core::Exception("Error: setViaPoint does not allow insertion of a point with "
                                  "different dimensions !!!");
        }

        for (size_t i = 0; i < point.size(); ++i)
        {
            point[i].pos -= force(i);
        }

        viaPoints.insert_or_assign(u, point);
        realViaPoints.insert_or_assign(u, origPoint);
    }

    void
    VMP::setViaPoint(double u, const core::DVec& point)
    {
        setViaPoint(u, core::SystemState::convertPosArrayToStates<MPState>(point));
    }

    void
    VMP::setViaPoint(const std::pair<double, NdimMPState>& viapoint)
    {
        setViaPoint(viapoint.first, viapoint.second);
    }

    void
    VMP::setViaPoint(double u, const NdimMPState& viapoint)
    {
        core::DVec2d point = core::SystemState::convertStatesTo2DArray(viapoint);
        setViaPoint(u, point);
    }

    const std::map<double, NdimMPState>&
    VMP::getViaPoints() const
    {
        return realViaPoints;
    }

    void VMP::prepareExecution(const core::DVec &goal, const NdimMPState &initialStates, double temporalFactor) {
        core::DVec2d states = core::SystemState::convertStatesTo2DArray(initialStates);
        prepareExecution(goal, states, temporalFactor);
    }

    void
    VMP::prepareExecution(const core::DVec& goal,
                          const core::DVec2d& initialStates,
                          double temporalFactor)
    {
        setTemporalFactor(temporalFactor);
        if (goal.size() != trajDim)
        {
            throw core::Exception(
                "The goal's dimension is not correct: goal.size: " + std::to_string(goal.size()) +
                " expected size:" + std::to_string(trajDim));
        }

        if (initialStates.size() != trajDim)
        {
            throw core::Exception(
                "The current states' dimension is not correct. initialState.size: " +
                std::to_string(initialStates.size()) +
                " expected size: " + std::to_string(trajDim));
        }

        setGoals(goal);
        NdimMPState initialStatesVec =
            core::SystemState::convert2DArrayToStates<MPState>(initialStates);
        setInitialState(initialStatesVec);
        prepareExecution();
    }

    void
    VMP::prepareExecution()
    {
        NdimMPState goalState = core::SystemState::convertPosArrayToStates<MPState>(goals);
        setViaPoint(canonicalSystem->getUMin(), goalState);
        setViaPoint(canonicalSystem->getInitialValue(), startState);
    }

    void
    VMP::removeViaPoints()
    {
        viaPoints.clear();
        realViaPoints.clear();
    }

    arma::vec
    calcMinJerkCoef(double terminalTime, const arma::vec& condition)
    {
        double T = terminalTime;

        arma::mat A(6, 6, arma::fill::zeros);

        A(0, 0) = 1;
        A(1, 1) = 1;
        A(2, 2) = 2;

        for (size_t i = 0; i < 6; ++i)
        {
            A(3, i) = pow(T, i);
            A(4, i) = i * pow(T, i - 1);
            A(5, i) = i * (i - 1) * pow(T, i - 2);
        }

        arma::vec a = inv(A) * condition;
        return a;
    }

    std::map<double, double>
    createDoubleMap(const core::DVec& canonicalVals, const arma::vec& sampleVec)
    {
        std::map<double, double> result;
        for (size_t i = 0; i < canonicalVals.size(); ++i)
        {
            result[canonicalVals[i]] = sampleVec(i);
        }
        return result;
    }

    core::DVec
    cvtArmaVecToDVec(const arma::vec& avec)
    {
        core::DVec res;
        res.resize(avec.n_rows);
        for (size_t i = 0; i < res.size(); ++i)
        {
            res[i] = avec(i);
        }
        return res;
    }

} // namespace mplib::representation::vmp
