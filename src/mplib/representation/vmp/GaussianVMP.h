#pragma once

#include "VMP.h"

namespace mplib::representation::vmp
{
    /**
 * @brief GaussVMP: Gaussian Via-points Movement Primitive
 * Assume all trajectories follow Gaussian (Mixture) distribution.
 * @param componentSize: the number of Gaussian Components are used
 * @param kernelSize: the kernel size for LocallyWeightedRegression
 * (If using other regression model, it needs to be explicitly set with setBaseFunctionApproximator (check learnVMP example for GPR).)
 * @param baseMode: the mode for the elementary trajectory (VMP_BASE_LINEAR and VMP_BASE_MINJERK)
 * @param tau is the temporal length factor used for training
 */
    class GaussianVMP : virtual public VMP
    {

    public:
        using AbstractMovementPrimitive::calculateTrajectory;

        GaussianVMP(int numOfComps,
                    const std::string& name,
                    int kernelSize = defaultKernelSize,
                    BaseMode baseMode = defaultBaseMode,
                    double tau = 1.0,
                    double timestep = 1e-3);

        GaussianVMP(int numOfComps);

        GaussianVMP() = default;

        virtual std::unique_ptr<AbstractMovementPrimitive> clone(bool deep = false) const override;

        virtual Space getSpace() const override;

        virtual VMPType getSpecificType() const override;

        void setIsSample(bool isSample);

        void setCurrCompId(int compId);

    protected:
        arma::vec getForce(double u, arma::vec& derivative) override;
        void learnModelFromData(const core::DVec& uValues,
                                std::vector<arma::mat> fMatList) override;

        void appendTo(GaussianVMP* mp, bool deep) const;

        std::vector<arma::mat> meanVecs;
        std::vector<arma::cube> covMats;
        std::vector<arma::rowvec> mixingCoeffs;
        size_t numOfComps;
        bool isSample{false};
        int currCompId{0};

    private:
        friend class boost::serialization::access;
        template <class Archive>
        void
        serialize(Archive& ar, const unsigned int /*file_version*/)
        {
            ar& boost::serialization::make_nvp("numOfComps", numOfComps);
            ar& boost::serialization::make_nvp("isSample", isSample);
            ar& boost::serialization::make_nvp("currCompId", currCompId);

            if (Archive::is_saving::value)
            {
                std::vector<std::string> vector;
                vector.reserve(meanVecs.size());
                for (auto& t : meanVecs)
                {
                    vector.push_back(saveArma(t));
                }
                ar& boost::serialization::make_nvp("meanVecs", vector);
            }
            else
            {
                std::vector<std::string> vector;
                ar& boost::serialization::make_nvp("meanVecs", vector);
                meanVecs.resize(vector.size());
                for (std::size_t idx = 0; idx < vector.size(); idx++)
                {
                    loadArma(meanVecs.at(idx), vector.at(idx));
                }
            }

            if (Archive::is_saving::value)
            {
                std::vector<std::string> vector;
                vector.reserve(covMats.size());
                for (auto& t : covMats)
                {
                    vector.push_back(saveArma(t));
                }
                ar& boost::serialization::make_nvp("covMats", vector);
            }
            else
            {
                std::vector<std::string> vector;
                ar& boost::serialization::make_nvp("covMats", vector);
                covMats.resize(vector.size());
                for (std::size_t idx = 0; idx < vector.size(); idx++)
                {
                    loadArma(covMats.at(idx), vector.at(idx));
                }
            }

            if (Archive::is_saving::value)
            {
                std::vector<std::string> vector;
                vector.reserve(mixingCoeffs.size());
                for (auto& t : mixingCoeffs)
                {
                    vector.push_back(saveArma(t));
                }
                ar& boost::serialization::make_nvp("mixingCoeffs", vector);
            }
            else
            {
                std::vector<std::string> vector;
                ar& boost::serialization::make_nvp("mixingCoeffs", vector);
                mixingCoeffs.resize(vector.size());
                for (std::size_t idx = 0; idx < vector.size(); idx++)
                {
                    loadArma(mixingCoeffs.at(idx), vector.at(idx));
                }
            }

            auto& base = boost::serialization::base_object<VMP>(*this);
            ar& boost::serialization::make_nvp("vmpbase", base);
        }
    };

} // namespace mplib::representation::vmp
