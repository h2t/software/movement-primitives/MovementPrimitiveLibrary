/**
* This file is part of MPLIB.
*
* MPLIB is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* MPLIB is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MPLIB::
* @author     H2T @ KIT
* @date       2021
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#pragma once

#include <memory>

#include <boost/serialization/nvp.hpp>

#include "Exception.h"
#include "types.h"

namespace mplib::core
{
    class SystemState
    {
    public:
        virtual ~SystemState() = default;

        DVec getValues() const;
        double& operator[](unsigned int i);
        const double& operator[](unsigned int i) const;
        virtual double& getValue(unsigned int i) = 0;
        virtual const double& getValue(unsigned int i) const = 0;
        virtual unsigned int size() const = 0;

        // Conversion helper functions
        template <typename SystemStatusType>
        static DVec convertStatesToArray(const std::vector<SystemStatusType>& states);
        template <typename SystemStatusType>
        static DVec convertStatesToArray(const std::vector<SystemStatusType>& states, unsigned int i);
        template <typename SystemStatusType>
        static std::vector<SystemStatusType> convertArrayToStates(const DVec& array);
        template <typename SystemStatusType>
        static std::vector<SystemStatusType> convertPosArrayToStates(const DVec& array);
        template <typename SystemStatusType>
        static SystemStatusType convertArrayPtrToState(const std::shared_ptr<DVec>& array);
        template <typename SystemStatusType>
        static DVec2d convertStatesTo2DArray(const std::vector<SystemStatusType>& states);
        template <typename SystemStatusType>
        static std::vector<SystemStatusType> convert2DArrayToStates(const DVec2d& arrays);

    protected:
        void checkSize(const DVec& status) const;
    };

    template <typename SystemStatusType>
    std::vector<SystemStatusType>
    SystemState::convertPosArrayToStates(const DVec& array)
    {
        std::vector<SystemStatusType> res;
        for (auto& item : array)
        {
            SystemStatusType state;
            state.pos = item;
            res.push_back(state);
        }

        return res;
    }

    template <typename SystemStatusType>
    std::vector<SystemStatusType>
    SystemState::convert2DArrayToStates(const DVec2d& arrays)
    {
        std::vector<SystemStatusType> res;
        for (auto& array : arrays)
        {
            res.push_back(SystemStatusType(array));
        }
        return res;
    }

    template <typename SystemStatusType>
    DVec2d
    SystemState::convertStatesTo2DArray(const std::vector<SystemStatusType>& states)
    {
        DVec2d res;
        for (auto& state : states)
        {
            DVec cres = state.getValues();
            res.push_back(cres);
        }
        return res;
    }

    template <typename SystemStatusType>
    DVec
    SystemState::convertStatesToArray(const std::vector<SystemStatusType>& states, unsigned int i) {
        DVec result;

        for (unsigned int j = 0; j < states.size(); j++)
        {
            result.push_back(states[j].getValue(i)); // forwards exception if i >= size()
        }

        return result;
    }

    template <typename SystemStatusType>
    DVec
    SystemState::convertStatesToArray(const std::vector<SystemStatusType>& states)
    {
        DVec result;

        for (unsigned int i = 0; i < states.size(); i++)
        {
            DVec state = states[i].getValues();
            result.insert(result.end(), state.begin(), state.end());
        }

        return result;
    }

    template <typename SystemStatusType>
    std::vector<SystemStatusType>
    SystemState::convertArrayToStates(const DVec& array)
    {
        std::vector<SystemStatusType> result;

        int typeSize = SystemStatusType().size();

        if (array.size() % typeSize != 0)
        {
            throw Exception() << "Cannot convert array to SystemStatusType - size is wrong: "
                              << array.size() << " must be a multiple of " << typeSize;
        }

        for (auto it = array.begin(); it != array.end();)
        {
            DVec singleDimEntry(it, it + typeSize);
            it += typeSize;
            result.push_back(singleDimEntry);
        }

        return result;
    }

    template <typename SystemStatusType>
    SystemStatusType
    SystemState::convertArrayPtrToState(const std::shared_ptr<DVec>& array)
    {
        int typeSize = SystemStatusType().size();

        if (array->size() < typeSize)
        {
            throw Exception() << "Cannot convert array to SystemStatusType - size is wrong: "
                              << array->size() << " must be of atleast of size " << typeSize;
        }

        SystemStatusType result(*array);

        return result;
    }
} // namespace mplib::core
