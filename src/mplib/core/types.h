#pragma once

#include <map>
#include <vector>

namespace mplib::core
{
    using DVec = std::vector<double>;
    using DVec2d = std::vector<DVec>;

    // forward declaration
    class SampledTrajectory;
} // namespace mplib::core
