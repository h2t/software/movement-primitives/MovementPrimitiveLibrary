/**
* This file is part of MPLib.
*
* MPLib is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* MPLib is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MPLIB::
* @author     H2T @ KIT
* @date       2021
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#pragma once

#include <exception>
#include <sstream>
#include <string>

namespace mplib::core
{
    /**
     * \class Exception
     * \brief Standard exception type.
     *
     * \ingroup Exceptions
     *
     * Offers a backtrace to where the exception was thrown.<br/>
     * All local exceptions must inherit from this class.
     */
    class Exception : public std::exception
    {
        mutable std::string resultingMessage;

    public:
        Exception();
        Exception(const Exception& e);
        Exception(const std::string& reason);

        ~Exception() noexcept override;
        const char* what() const noexcept override;
        virtual std::string name() const;
        void setReason(std::string reason);

        static std::string generateBacktrace();

        template <typename T>
        Exception&
        operator<<(const T& message)
        {
            reason << message;
            return *this;
        }

    private:
        void generateOutputString() const;

        std::stringstream reason;
        std::string backtrace;
    };

} // namespace mplib::core
