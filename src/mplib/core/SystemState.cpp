#include "SystemState.h"

namespace mplib::core
{

    void
    SystemState::checkSize(const DVec& status) const
    {
        if (status.size() < size())
        {
            std::stringstream str;
            str << "Size of input Vec does not match: " << status.size() << ", but it should be "
                << size() << std::endl;
            throw Exception(str.str());
        }
    }

    DVec
    SystemState::getValues() const
    {

        DVec result;

        for (unsigned int i = 0; i < size(); i++)
        {
            result.push_back(getValue(i));
        }

        return result;
    }

    double&
    SystemState::operator[](unsigned int i)
    {
        return getValue(i);
    }

} // namespace mplib::core
