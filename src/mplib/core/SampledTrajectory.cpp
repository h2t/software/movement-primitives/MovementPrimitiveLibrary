#include "SampledTrajectory.h"

#include <fstream>
#include <iostream>
#include <memory>

#include <boost/tokenizer.hpp>

#include "mplib/math/util/check_value.h"
#include "mplib/math/util/eigen_arithmetic.h"
#include "mplib/math/util/vector_arithmetic.h"

namespace mplib::core
{

    struct InterpolationPosition
    {
        unsigned int indexOfT;
        double t;
        unsigned int otherFunctionIndexPre; //, other_function_index_post;
        char functionIndex;
        bool interpolationNecessary;
    };

    SampledTrajectory::SampledTrajectory() = default;

    SampledTrajectory::SampledTrajectory(const SampledTrajectory& source)
    {
        copyData(source, *this);
    }

    SampledTrajectory::SampledTrajectory(const DVec& x, const DVec& y)
    {
        addDimension(x, y);
    }

    SampledTrajectory::SampledTrajectory(const std::map<double, DVec>& data)
    {
        if (data.size() == 0)
        {
            return;
        }

        auto it = data.begin();

        for (; it != data.end(); it++)
        {
            TrajData dataEntry(*this);
            dataEntry.timestamp = it->first;
            const DVec& dataVec = it->second;

            for (double i : dataVec)
            {
                math::util::checkValue(i);
                dataEntry.data.push_back(std::make_shared<DVec>(1, i));
            }

            dataMap.insert(dataEntry);
        }
    }

    SampledTrajectory::SampledTrajectory(const std::map<double, Eigen::Matrix4d> &data, bool useReferenceQuaternion) :
        SampledTrajectory(math::util::toDVec(data, useReferenceQuaternion))
    {
    }

    SampledTrajectory::SampledTrajectory(const core::DVec& data, double timestep)
    {
        if (data.size() == 0)
        {
            return;
        }

        double timestamp = 0;
        for (double val : data)
        {
            TrajData dataEntry(*this);
            dataEntry.timestamp = timestamp;
            dataEntry.data.push_back(std::make_shared<DVec>(1, val));
            dataMap.insert(dataEntry);

            timestamp += timestep;
        }
    }

    SampledTrajectory::~SampledTrajectory() = default;

    //
    //    SampledTrajectory::SampledTrajectory(const TrajMap &map)
    //    {
    //        copyMap(map, dataMap);
    //    }

    SampledTrajectory&
    SampledTrajectory::operator=(const SampledTrajectory& source)
    {
        copyData(source, *this);
        return *this;
    }

    double
    SampledTrajectory::getState(double t, unsigned int dim, unsigned int deriv)
    {
        if (dim >= this->dim())
        {
            throw Exception() << "dimension is to big: " << dim << " max: " << this->dim();
        }

        typename timestamp_view::iterator it = dataMap.find(t);

        if (it == dataMap.end() || !it->data.at(dim))
        {
            fillBaseDataAtTimestamp(t); // interpolates and retrieves
            it = dataMap.find(t);
        }

        if (!it->data.at(dim))
        //        it->data.at(dim).reset(new DVec());
        {
            throw Exception() << "Vec ptr is NULL!?";
        }

        std::vector<DVecPtr>& vec = it->data;

        if (deriv != 0 && vec.at(dim)->size() <= deriv)
        {
            //resize and calculate derivations
            unsigned int curDeriv = it->data.at(dim)->size();
            it->data.at(dim)->resize(deriv + 1);

            while (curDeriv <= deriv)
            {
                double derivValue = getDiscretDifferentiationForDimAtT(t, dim, curDeriv);
                math::util::checkValue(curDeriv);
                it->data.at(dim)->at(curDeriv) = derivValue;
                curDeriv++;
            }
        }

        //    std::cout << "dimensions: " <<it->data.size() << " derivations: " <<  it->data.at(dim)->size() << std::endl;
        double result = it->data.at(dim)->at(deriv);
        //    checkValue(result);
        return result;
    }

    double
    SampledTrajectory::getState(double t, unsigned int dim, unsigned int deriv) const
    {

        if (dim >= this->dim())
        {
            throw Exception() << "dimension is to big: " << dim << " max: " << this->dim();
        }

        typename timestamp_view::iterator it = dataMap.find(t);

        if (!dataExists(t, dim, deriv))
        {
            if (deriv == 0)
            {
                return calcBaseDataAtTimestamp(t).at(dim)->at(deriv); // interpolates and retrieves
            }

            return getDiscretDifferentiationForDimAtT(t, dim, deriv);
        }

        //                std::cout << "dimensions: " <<it->data.size() << " derivations: " <<  it->data.at(dim)->size() << std::endl;
        double result = it->data.at(dim)->at(deriv);

        math::util::checkValue(result);

        return result;
    }

    DVec
    SampledTrajectory::getDerivations(double t,
                                      unsigned int dimension,
                                      unsigned int derivations) const
    {
        if (dimension >= dim())
        {
            throw Exception() << "Dimension out of bounds: requested: " << dimension
                              << " available: " << dim();
        }

        DVec result;

        for (unsigned int i = 0; i <= derivations; i++)
        {
            result.push_back(getState(t, dimension, i));
        }

        return result;
    }

    SampledTrajectory::TrajDataContainer&
    SampledTrajectory::data()
    {
        return dataMap;
    }

    DVec
    SampledTrajectory::getDimensionData(unsigned int dimension, unsigned int deriv) const
    {
        if (dimension >= dim())
        {
            throw Exception("dimension is out of range: ")
                << dimension << " actual dimensions: " << dim();
        }

        DVec result;

        const ordered_view& ordv = dataMap.get<TagOrdered>();
        typename ordered_view::const_iterator itMap = ordv.begin();

        for (; itMap != ordv.end(); itMap++)
        {
            DVecPtr dataPtr = itMap->data[dimension];
            result.push_back(itMap->getDeriv(dimension, deriv));
            //        if(dataPtr && dataPtr->size() > deriv)
            //            result.push_back(itMap->getDeriv(dimension, deriv));
            //        else result.push_back(getState(itMap->timestamp, dimension, deriv));
        }

        return result;
    }

    Eigen::VectorXd
    SampledTrajectory::getDimensionDataAsEigen(unsigned int dimension,
                                               unsigned int derivation) const
    {
        return getDimensionDataAsEigen(
            dimension, derivation, begin()->getTimestamp(), rbegin()->getTimestamp());
    }

    Eigen::VectorXd
    SampledTrajectory::getDimensionDataAsEigen(unsigned int dimension,
                                               unsigned int derivation,
                                               double startTime,
                                               double endTime) const
    {
        if (dimension >= dim())
        {
            throw Exception("dimension is out of range: ")
                << dimension << " actual dimensions: " << dim();
        }

        DVec result;

        const ordered_view& ordv = dataMap.get<TagOrdered>();
        ordered_view::iterator itO = ordv.lower_bound(startTime);
        ordered_view::iterator itOEnd = ordv.upper_bound(endTime);
        //    typename ordered_view::const_iterator itMap = ordv.begin();

        size_t i = 0;
        for (; itO != itOEnd && i < size(); itO++, i++)
        {
            DVecPtr dataPtr = itO->data[dimension];
            result.push_back(itO->getDeriv(dimension, derivation));
            //        if(dataPtr && dataPtr->size() > deriv)
            //            result.push_back(itMap->getDeriv(dimension, deriv));
            //        else result.push_back(getState(itMap->timestamp, dimension, deriv));
        }
        Eigen::VectorXd resultVec(result.size());
        for (size_t i = 0; i < result.size(); i++)
        {
            resultVec(i) = result[i];
        }
        return resultVec;
    }

    Eigen::MatrixXd
    SampledTrajectory::toEigen(unsigned int derivation, double startTime, double endTime) const
    {

        Eigen::MatrixXd result(size(), dim());

        const ordered_view& ordv = dataMap.get<TagOrdered>();
        ordered_view::iterator itO = ordv.lower_bound(startTime);
        ordered_view::iterator itOEnd = ordv.upper_bound(endTime);
        //    typename ordered_view::const_iterator itMap = ordv.begin();

        size_t i = 0;
        for (; itO != itOEnd; itO++, i++)
        {
            //        DVecPtr dataPtr = itO->data[dimension];
            for (size_t d = 0; d < itO->data.size(); d++)
            {
                result(i, d) = (itO->getDeriv(d, derivation));
            }
        }

        return result;
    }

    Eigen::MatrixXd
    SampledTrajectory::toEigen(unsigned int derivation) const
    {
        return toEigen(derivation, begin()->getTimestamp(), rbegin()->getTimestamp());
    }

    SampledTrajectory
    SampledTrajectory::getPart(double startTime,
                               double endTime,
                               unsigned int numberOfDerivations) const
    {
        SampledTrajectory result;

        const ordered_view& ordv = dataMap.get<TagOrdered>();
        ordered_view::iterator itO = ordv.lower_bound(startTime);
        ordered_view::iterator itOEnd = ordv.upper_bound(endTime);
        //    typename ordered_view::const_iterator itMap = ordv.begin();

        size_t i = 0;
        for (; itO != itOEnd; itO++, i++)
        {
            //        DVecPtr dataPtr = itO->data[dimension];
            for (size_t d = 0; d < itO->data.size(); d++)
            {
                DVec derivs;
                for (size_t i = 0; i < numberOfDerivations + 1; i++)
                {
                    derivs.push_back(itO->getDeriv(d, i));
                }

                result.addDerivationsToDimension(d, itO->getTimestamp(), derivs);
            }
            //            result.addPositionsToDimension(d, itO->getTimestamp(), itO->getDeriv(d, 0));
        }

        return result;
    }

    unsigned int
    SampledTrajectory::dim() const
    {
        if (dataMap.size() == 0)
        {
            return 0;
        }

        return dataMap.begin()->data.size();
    }

    unsigned int
    SampledTrajectory::size() const
    {
        return dataMap.size();
    }

    //    DVec
    //    SampledTrajectory::getDimensionData(unsigned int dimension, unsigned int deriv) const
    //    {
    //        if(dimension >= dim()){
    //            throw Exception("dimension is out of range: ") << dimension << " actual dimensions: " << dim();
    //        }

    //        DVec result;
    //        DVec tempResult = getDimensionData(dimension);
    //        typename DVec::iterator it = tempResult.begin();
    //        for(; it != tempResult.end(); it++)
    //            result.push_back(it->at(deriv));

    //        return result;
    //    }

    double
    SampledTrajectory::getTimeLength() const
    {
        const ordered_view& ordView = dataMap.get<TagOrdered>();

        if (ordView.begin() != ordView.end())
        {
            return ordView.rbegin()->timestamp - ordView.begin()->timestamp;
        }

        return 0;
    }

    double
    SampledTrajectory::getSpaceLength(unsigned int dimension, unsigned int stateDim) const
    {
        return getSpaceLength(
            dimension, stateDim, begin()->getTimestamp(), rbegin()->getTimestamp());
    }

    double
    SampledTrajectory::getSpaceLength(unsigned int dimension,
                                      unsigned int stateDim,
                                      double startTime,
                                      double endTime) const
    {
        double length = 0.0;
        const ordered_view& ordv = dataMap.get<TagOrdered>();
        //    timestamp_view::iterator it = dataMap.find(startTime);
        //    DVec data = getDimensionData(0);
        //    timestamp_view::iterator itEnd= dataMap.find(endTime);
        ordered_view::iterator itO = ordv.lower_bound(startTime);
        ordered_view::iterator itOEnd = ordv.upper_bound(endTime);
        if (itO == ordv.end())
        {
            return 0.0;
        }
        double prevValue = itO->getDeriv(dimension, stateDim);

        for (; itO != itOEnd; itO++)
        {
            length += fabs(prevValue - itO->getDeriv(dimension, stateDim));
            prevValue = itO->getDeriv(dimension, stateDim);
        }

        return length;
    }

    double
    SampledTrajectory::getSquaredSpaceLength(unsigned int dimension, unsigned int stateDim) const
    {
        return getSquaredSpaceLength(
            dimension, stateDim, begin()->getTimestamp(), rbegin()->getTimestamp());
    }

    double
    SampledTrajectory::getSquaredSpaceLength(unsigned int dimension,
                                             unsigned int stateDim,
                                             double startTime,
                                             double endTime) const
    {
        double length = 0.0;
        const ordered_view& ordv = dataMap.get<TagOrdered>();
        //    timestamp_view::iterator it = dataMap.find(startTime);
        //    DVec data = getDimensionData(0);
        //    timestamp_view::iterator itEnd= dataMap.find(endTime);
        ordered_view::iterator itO = ordv.lower_bound(startTime);
        ordered_view::iterator itOEnd = ordv.upper_bound(endTime);
        if (itO == ordv.end())
        {
            return 0.0;
        }
        //    double prevValue = itO->data[dimension]->at(stateDim);
        double prevValue = itO->getDeriv(dimension, stateDim);

        for (; itO != itOEnd; itO++)
        {
            length += fabs(pow(prevValue, 2.0) - pow(itO->getDeriv(dimension, stateDim), 2.0));
            prevValue = itO->getDeriv(dimension, stateDim);
        }

        return length;
    }

    double
    SampledTrajectory::getMax(unsigned int dimension,
                              unsigned int stateDim,
                              double startTime,
                              double endTime) const
    {
        return getWithFunc(&std::max<double>,
                           -std::numeric_limits<double>::max(),
                           dimension,
                           stateDim,
                           startTime,
                           endTime);
    }

    double
    SampledTrajectory::getMin(unsigned int dimension,
                              unsigned int stateDim,
                              double startTime,
                              double endTime) const
    {
        return getWithFunc(&std::min<double>,
                           std::numeric_limits<double>::max(),
                           dimension,
                           stateDim,
                           startTime,
                           endTime);
    }

    double
    SampledTrajectory::getWithFunc(const double& (*foo)(const double&, const double&),
                                   double initValue,
                                   unsigned int dimension,
                                   unsigned int stateDim,
                                   double startTime,
                                   double endTime) const
    {
        double bestValue = initValue;
        const ordered_view& ordv = dataMap.get<TagOrdered>();
        ordered_view::iterator itO = ordv.lower_bound(startTime);
        ordered_view::iterator itOEnd = ordv.upper_bound(endTime);

        if (itO == ordv.end())
        {
            return bestValue;
        }
        for (; itO != itOEnd; itO++)
        {
            bestValue = foo(bestValue, itO->getDeriv(dimension, stateDim));
        }

        return bestValue;
    }

    double
    SampledTrajectory::getTrajectorySpace(unsigned int dimension,
                                          unsigned int stateDim,
                                          double startTime,
                                          double endTime) const
    {
        return getMax(dimension, stateDim, startTime, endTime) -
               getMin(dimension, stateDim, startTime, endTime);
    }

    DVec
    SampledTrajectory::getMinima(unsigned int dimension,
                                 unsigned int stateDim,
                                 double startTime,
                                 double endTime) const
    {
        DVec result;
        const ordered_view& ordv = dataMap.get<TagOrdered>();
        ordered_view::iterator itO = ordv.lower_bound(startTime);
        ordered_view::iterator itOEnd = ordv.upper_bound(endTime);

        if (itO == ordv.end())
        {
            return result;
        }
        double preValue = itO->getDeriv(dimension, stateDim);
        for (; itO != itOEnd;

        )
        {
            double cur = itO->getDeriv(dimension, stateDim);
            itO++;
            if (itO == ordv.end())
            {
                break;
            }
            double next = itO->getDeriv(dimension, stateDim);
            if (cur <= preValue && cur <= next)
            {
                result.push_back(cur);
            }
            preValue = cur;
        }

        return result;
    }

    DVec
    SampledTrajectory::getMinimaPositions(unsigned int dimension,
                                          unsigned int stateDim,
                                          double startTime,
                                          double endTime) const
    {
        DVec result;
        const ordered_view& ordv = dataMap.get<TagOrdered>();
        ordered_view::iterator itO = ordv.lower_bound(startTime);
        ordered_view::iterator itOEnd = ordv.upper_bound(endTime);

        if (itO == ordv.end())
        {
            return result;
        }
        double preValue = itO->getDeriv(dimension, stateDim);
        for (; itO != itOEnd;

        )
        {
            double cur = itO->getDeriv(dimension, stateDim);
            itO++;
            if (itO == ordv.end())
            {
                break;
            }
            double next = itO->getDeriv(dimension, stateDim);
            if (cur <= preValue && cur <= next)
            {
                result.push_back(itO->getTimestamp());
            }
            preValue = cur;
        }

        return result;
    }

    DVec
    SampledTrajectory::getMaxima(unsigned int dimension,
                                 unsigned int stateDim,
                                 double startTime,
                                 double endTime) const
    {
        DVec result;
        const ordered_view& ordv = dataMap.get<TagOrdered>();
        ordered_view::iterator itO = ordv.lower_bound(startTime);
        ordered_view::iterator itOEnd = ordv.upper_bound(endTime);

        if (itO == ordv.end())
        {
            return result;
        }
        double preValue = itO->getDeriv(dimension, stateDim);
        for (; itO != itOEnd;

        )
        {
            double cur = itO->getDeriv(dimension, stateDim);
            itO++;
            if (itO == ordv.end())
            {
                break;
            }
            double next = itO->getDeriv(dimension, stateDim);
            if (cur >= preValue && cur >= next)
            {
                result.push_back(cur);
            }
            preValue = cur;
        }

        return result;
    }

    DVec
    SampledTrajectory::getMaximaPositions(unsigned int dimension,
                                          unsigned int stateDim,
                                          double startTime,
                                          double endTime) const
    {
        DVec result;
        const ordered_view& ordv = dataMap.get<TagOrdered>();
        ordered_view::iterator itO = ordv.lower_bound(startTime);
        ordered_view::iterator itOEnd = ordv.upper_bound(endTime);

        if (itO == ordv.end())
        {
            return result;
        }
        double preValue = itO->getDeriv(dimension, stateDim);
        for (; itO != itOEnd;

        )
        {
            double cur = itO->getDeriv(dimension, stateDim);
            itO++;
            if (itO == ordv.end())
            {
                break;
            }
            double next = itO->getDeriv(dimension, stateDim);

            if (cur >= preValue && cur >= next)
            {
                result.push_back(itO->getTimestamp());
            }
            preValue = cur;
        }

        return result;
    }

    std::vector<DVecPtr>
    SampledTrajectory::calcBaseDataAtTimestamp(const double& t) const
    {
        //    typename timestamp_view::const_iterator it = dataMap.find(t);
        //    if(it != dataMap.end())
        //        return it->data;
        std::vector<DVecPtr> result;

        for (unsigned int dimension = 0; dimension < dim(); dimension++)
        {
            double newValue = interpolate(t, dimension, 0);
            math::util::checkValue(newValue);
            result.push_back(std::make_shared<DVec>(1, newValue));
        }

        return result;
    }

    bool
    SampledTrajectory::dataExists(double t, unsigned int dimension, unsigned int deriv) const
    {
        typename timestamp_view::iterator it = dataMap.find(t);

        if (it == dataMap.end() || !it->data.at(dimension) ||
            it->data.at(dimension)->size() <= deriv)
        {
            return false;
        }

        return true;
    }

    double
    SampledTrajectory::getPhi1() const
    {
        return mPhi1;
    }

    void
    SampledTrajectory::setPhi1(double phiNew)
    {
        mPhi1 = phiNew;
    }

    void
    SampledTrajectory::setPeriodLength(double newPeriodLength)
    {
        mPeriodLength = newPeriodLength;
    }

    void
    SampledTrajectory::setTransientLength(double newTransientLength)
    {
        mTransientLength = newTransientLength;
    }

    SampledTrajectory::timestamp_view::iterator
    SampledTrajectory::fillBaseDataAtTimestamp(const double& t)
    {
        typename timestamp_view::const_iterator it = dataMap.find(t);

        if (it != dataMap.end())
        {
            bool foundEmpty = false;

            for (auto& i : it->data)
            {
                if (!i)
                {
                    foundEmpty = true;
                }
            }

            if (!foundEmpty)
            {
                return it;
            }
        }

        TrajData entry(*this);
        entry.timestamp = t;
        entry.data.resize(dim());
        dataMap.insert(entry);
        it = dataMap.find(t);

        assert(it != dataMap.end());
        it->data = calcBaseDataAtTimestamp(t);

        return it;
    }

    const std::vector<DVecPtr>&
    SampledTrajectory::getStates(double t)
    {
        typename timestamp_view::const_iterator it = dataMap.find(t);

        if (it == dataMap.end())
        {
            // interpolate and insert entry
            it = fillBaseDataAtTimestamp(t);
        }

        return it->data;
    }

    std::vector<DVecPtr>
    SampledTrajectory::getStates(double t) const
    {
        typename timestamp_view::const_iterator it = dataMap.find(t);

        if (it == dataMap.end())
        {
            // interpolate and return data
            return calcBaseDataAtTimestamp(t);
        }

        return it->data;
    }

    DVec
    SampledTrajectory::getStates(double t, unsigned int derivation) const
    {
        DVec result;
        unsigned int dimensions = dim();

        for (unsigned int i = 0; i < dimensions; i++)
        {
            result.push_back(getState(t, i, derivation));
        }

        return result;
    }

    std::map<double, DVec>
    SampledTrajectory::getStatesAround(double t, unsigned int derivation, unsigned int extend) const
    {

        std::map<double, DVec> res;
        typename timestamp_view::iterator itMap = dataMap.find(t);

        if (itMap != dataMap.end())
        {
            for (size_t i = 0; i < dim(); i++)
            {
                res.insert(std::pair<double, DVec>(t, itMap->data[i]->at(derivation)));
            }

            return res;
        }

        const ordered_view& ordv = dataMap.get<TagOrdered>();

        typename ordered_view::iterator itNext = ordv.upper_bound(t);

        typename ordered_view::iterator itPrev = itNext;

        itPrev--;

        if (itPrev == ordv.end())
        {

            throw Exception("Cannot find value at timestamp ") << t;
        }

        for (size_t i = 0; i < extend; i++)
        {
            DVec preData = getStates(itPrev->timestamp, derivation);
            DVec nexData = getStates(itNext->timestamp, derivation);

            res.insert(std::pair<double, DVec>(itPrev->timestamp, preData));
            res.insert(std::pair<double, DVec>(itNext->timestamp, nexData));

            if (itPrev == ordv.begin() || itNext == ordv.end())
            {
                std::cout << "Warning: the timestamp is out of the range. "
                          << "The current result will be returned" << std::endl;
                break;
            }
            itPrev--;
            itNext++;
        }

        return res;
    }

    std::vector<std::string>
    SampledTrajectory::getDimensionNames()
    {
        return dimNames;
    }

    SampledTrajectory&
    SampledTrajectory::operator+=(const SampledTrajectory traj)
    {
        int dims = traj.dim();
        DVec timestamps = traj.getTimestamps();

        for (int d = 0; d < dims; ++d)
        {
            addPositionsToDimension(d, timestamps, traj.getDimensionData(d));
        }

        return *this;
    }

    void
    SampledTrajectory::addDimension()
    {
        unsigned int newDim = dim() + 1;

        typename timestamp_view::iterator itMap = dataMap.begin();

        for (; itMap != dataMap.end(); itMap++)
        {
            itMap->data.resize(newDim);
        }
    }

    void
    SampledTrajectory::setEntries(const double t, const unsigned int dimIndex, const DVec& y)
    {
        typename timestamp_view::iterator itMap = dataMap.find(t);

        if (itMap == dataMap.end() || dim() == 0)
        {
            TrajData newData(*this);
            newData.timestamp = t;
            newData.data = std::vector<DVecPtr>(std::max((unsigned int)1, dim()), DVecPtr());
            newData.data[dimIndex] = std::make_shared<DVec>(y);
            dataMap.insert(newData);
        }
        else
        {
            assert(dim() > 0);

            while (dim() <= dimIndex)
            {
                addDimension();
            }

            itMap->data.resize(dim());
            itMap->data.at(dimIndex) = std::make_shared<DVec>(y);
        }
    }

    void
    SampledTrajectory::setPositionEntry(const double t,
                                        const unsigned int dimIndex,
                                        const double& y)
    {
        typename timestamp_view::iterator itMap = dataMap.find(t);

        if (itMap == dataMap.end() || dim() == 0)
        {
            TrajData newData(*this);
            newData.timestamp = t;
            newData.data = std::vector<DVecPtr>(std::max((unsigned int)1, dim()), DVecPtr());
            newData.data[dimIndex] = std::make_shared<DVec>(1, y);
            dataMap.insert(newData);
        }
        else
        {
            assert(dim() > 0);
            itMap->data.resize(dim());
            itMap->data.at(dimIndex) = std::make_shared<DVec>(1, y);
        }
    }

    void
    SampledTrajectory::fillAllEmptyFields()
    {
        const ordered_view& ordv = dataMap.get<TagOrdered>();
        typename ordered_view::const_iterator itMap = ordv.begin();

        for (; itMap != ordv.end(); itMap++)
        {
            for (unsigned int dimension = 0; dimension < dim(); dimension++)
            {
                if (!itMap->data[dimension])
                {
                    itMap->data[dimension] =
                        std::make_shared<DVec>(interpolate(itMap, dimension, 0));
                }
            }
        }
    }

    DVec
    SampledTrajectory::getTimestamps() const
    {
        DVec result;

        const ordered_view& ordv = dataMap.get<TagOrdered>();
        typename ordered_view::const_iterator itMap = ordv.begin();

        for (; itMap != ordv.end(); itMap++)
        {
            result.push_back(itMap->timestamp);
        }

        return result;
    }

    DVec
    SampledTrajectory::getDiscretDifferentiationForDim(unsigned int trajDimension,
                                                       unsigned int derivation) const
    {
        if (trajDimension >= dim())
        {
            throw Exception("dimension is out of range: ")
                << trajDimension << " actual dimensions: " << dim();
        }

        //        if(sourceDimOfSystemState >= double().size()){
        //            throw Exception("sourceDimOfSystemState is out of range: ") << sourceDimOfSystemState << " actual dimensions: " << double().size();
        //        }
        //        if(targetDimOfSystemState >= double().size()){
        //            throw Exception("sourceDimOfSystemState is out of range: ") << targetDimOfSystemState << " actual dimensions: " << double().size();
        //        }

        const ordered_view& ordv = dataMap.get<TagOrdered>();
        typename ordered_view::const_iterator itPrev = ordv.begin();

        typename ordered_view::const_iterator itCurrent = itPrev;
        //        itCurrent++;
        typename ordered_view::const_iterator itNext = itCurrent;
        itNext++;

        DVec result;

        for (; itCurrent != ordv.end();)
        {
            if (itNext == ordv.end()) // last step-> differentiate between current and prev
            {
                itNext = itCurrent;
            }

            // get current data
            DVecPtr prevStatePtr = itPrev->data[trajDimension];
            DVecPtr nextStatePtr = itNext->data[trajDimension];
            //            double nextValue = nextStatePtr->getValues()[sourceDimOfSystemState];
            //            double prevValue = prevStatePtr->getValues()[sourceDimOfSystemState];
            //            double nextTime = itNext->timestamp;
            //            double prevTime = itPrev->timestamp;
            // differentiateDiscretly now
            result.push_back((nextStatePtr->at(derivation) - prevStatePtr->at(derivation)) /
                             (itNext->timestamp - itPrev->timestamp));
            math::util::checkValue(*result.rbegin());

            itCurrent++, itPrev++, itNext++;

            if (itPrev == itCurrent)
            {
                // first step-> reestablish that current is > prev
                itPrev--;
            }
        }

        return result;
    }

    DVec
    SampledTrajectory::differentiateDiscretly(const DVec& timestamps,
                                              const DVec& values,
                                              int derivationCount)
    {
        if (derivationCount < 0)
        {
            throw Exception("Negative derivation value is not allowed!");
        }

        if (derivationCount == 0)
        {
            return values;
        }

        DVec result;
        int size = std::min(timestamps.size(), values.size());

        if (size < 2)
        {
            return values;
        }

        result.resize(size);

        // boundaries
        result[0] = (values.at(1) - values.at(0)) / (timestamps.at(1) - timestamps.at(0));
        result[size - 1] = (values.at(size - 1) - values.at(size - 2)) /
                           (timestamps.at(size - 1) - timestamps.at(size - 2));

        //#pragma omp parallel for
        for (int i = 1; i < size - 1; ++i)
        {
            result[i] = (values.at(i + 1) - values.at(i - 1)) /
                        (timestamps.at(i + 1) - timestamps.at(i - 1));
            math::util::checkValue(result[i]);
        }

        if (derivationCount > 1) // recursivly differentiate
        {
            result = differentiateDiscretly(timestamps, result, derivationCount - 1);
        }

        return result;
    }

    double
    SampledTrajectory::getDiscretDifferentiationForDimAtT(double t,
                                                          unsigned int trajDimension,
                                                          unsigned int deriv) const
    {
        if (deriv == 0)
        {
            return getState(t, trajDimension, deriv);
        }

        typename timestamp_view::iterator it = dataMap.find(t);
        //        if(it == dataMap.end())
        //        {
        //            getStates(t); // interpolate and insert entry
        //            it = dataMap.find(t);
        //        }

        const ordered_view& ordV = dataMap.get<TagOrdered>();
        typename ordered_view::iterator itCurrent = ordV.end();

        if (it != dataMap.end())
        {
            itCurrent = ordV.iterator_to(*it);
        }

        typename ordered_view::iterator itNext = ordV.upper_bound(t); // first element after t

        if (it != dataMap.end() && itNext == ordV.end())
        {
            itNext = itCurrent; // current item is last, set next to current
        }
        else if (itNext == ordV.end() && it == dataMap.end())
        {
            throw Exception() << "Cannot interpolate for t " << t << " no data in trajectory";
        }

        typename ordered_view::iterator itPrev = itNext;
        itPrev--; // now equal to current (if current exists) or before current

        if (itCurrent != ordV.end()) // check if current item exists
        {
            itPrev--; //element in front of t
        }

        if (itPrev == ordV.end())
        {
            //element in front of t does not exist
            if (itCurrent != ordV.end())
            {
                itPrev = itCurrent; // set prev element to current
            }
            else
            {
                throw Exception() << "Cannot interpolate for t " << t;
            }
        }

        if (itNext == ordV.end())
        {
            //element after t does not exist
            if (itCurrent != ordV.end())
            {
                itNext = itCurrent; // set next element to current
            }
            else
            {
                throw Exception() << "Cannot interpolate for t " << t;
            }
        }

        if (itNext == itPrev)
        {
            throw Exception() << "Interpolation failed: the next data and the previous are missing";
        }

        //        double diff = itNext->data[trajDimension]->at(deriv-1) - itPrev->data[trajDimension]->at(deriv-1);
        double tNext;

        if (dataExists(itNext->timestamp, trajDimension, deriv - 1) || deriv > 1)
        {
            tNext = itNext->timestamp;
        }
        else
        {
            tNext = t;
        }

        double next = getState(tNext, trajDimension, deriv - 1);

        double tBefore;

        if (dataExists(itPrev->timestamp, trajDimension, deriv - 1) || deriv > 1)
        {
            tBefore = itPrev->timestamp;
        }
        else
        {
            tBefore = t;
        }

        double before = getState(tBefore, trajDimension, deriv - 1);

        if (fabs(tNext - tBefore) < 1e-10)
        {
            throw Exception() << "Interpolation failed: the next data and the previous are missing";
        }

        double duration = tNext - tBefore;
        double delta = next - before;
        delta = delta / duration;
        math::util::checkValue(delta);
        return delta;
    }

    void
    SampledTrajectory::differentiateDiscretly(unsigned int derivation)
    {

        for (unsigned int d = 0; d < dim(); d++)
        {
            differentiateDiscretlyForDim(d, derivation);
        }
    }

    void
    SampledTrajectory::differentiateDiscretlyForDim(unsigned int trajDimension,
                                                    unsigned int derivation)
    {
        removeDerivation(trajDimension, derivation);
        typename ordered_view::iterator itOrd = begin();

        for (; itOrd != end(); itOrd++)
        {
            getState(itOrd->timestamp, trajDimension, derivation);
        }
    }

    //    void SampledTrajectory::differentiateDiscretlyForDim(unsigned int trajDimension, unsigned int sourceDimOfSystemState, unsigned int targetDimOfSystemState)
    //    {
    //        if(trajDimension >= dim()){
    //            throw Exception("dimension is out of range: ") << trajDimension << " actual dimensions: " << dim();
    //        }
    //        if(sourceDimOfSystemState >= double().size()){
    //            throw Exception("sourceDimOfSystemState is out of range: ") << sourceDimOfSystemState << " actual dimensions: " << double().size();
    //        }
    //        if(targetDimOfSystemState >= double().size()){
    //            throw Exception("targetDimOfSystemState is out of range: ") << targetDimOfSystemState << " actual dimensions: " << double().size();
    //        }

    //        const ordered_view & ordv = dataMap.template get<TagOrdered>();
    //        typename ordered_view::const_iterator itPrev = ordv.begin();

    //        typename ordered_view::const_iterator itCurrent = itPrev;
    ////        itCurrent++;
    //        typename ordered_view::const_iterator itNext = itCurrent;
    //        itNext++;

    //        for(; itCurrent != ordv.end(); )
    //        {
    //            if(itNext == ordv.end()) // last step-> differentiate between current and prev
    //                itNext = itCurrent;

    //            // get current data
    //            DVecPtr currentStatePtr = itCurrent->data[trajDimension];
    //            DVecPtr prevStatePtr = itPrev->data[trajDimension];
    //            DVecPtr nextStatePtr = itNext->data[trajDimension];
    //            DVec newStateData = currentStatePtr->getValues();

    //            // differentiateDiscretly now
    //            newStateData[targetDimOfSystemState] = (nextStatePtr->at(sourceDimOfSystemState) - prevStatePtr->at(sourceDimOfSystemState))/
    //                    (itNext->timestamp - itPrev->timestamp);
    //            itCurrent->data[trajDimension] = DVecPtr(new DVec(1,newStateData));

    //            itCurrent++, itPrev++, itNext++;
    //            if(itPrev == itCurrent)
    //            {
    //                // first step-> reestablish that current is > prev
    //                itPrev--;
    //            }

    //        }
    //    }

    //    void SampledTrajectory::differentiateDiscretly(unsigned int sourceDimOfSystemState, unsigned int targetDimOfSystemState)
    //    {
    //        unsigned int size = dim();
    //        for(unsigned int i=0; i < size; i++)
    //        {
    //            differentiateDiscretlyForDim(i, sourceDimOfSystemState, targetDimOfSystemState);
    //        }
    //    }

    void
    SampledTrajectory::reconstructFromDerivativeForDim(double valueAtFirstTimestamp,
                                                       unsigned int trajDimension,
                                                       unsigned int sourceDimOfSystemState,
                                                       unsigned int targetDimOfSystemState)
    {
        const ordered_view& ordv = dataMap.get<TagOrdered>();
        typename ordered_view::iterator it = ordv.begin();

        if (it == ordv.end())
        {
            return;
        }

        it->data[trajDimension]->at(targetDimOfSystemState) = valueAtFirstTimestamp;
        double previousValue = valueAtFirstTimestamp;
        double previousTimestamp = it->timestamp;

        //        unsigned int stateDim = double().size();
        for (it++; it != ordv.end(); it++)
        {
            double slope;
            if (it->data[trajDimension]->size() > sourceDimOfSystemState)
            {
                slope = it->data[trajDimension]->at(sourceDimOfSystemState);
            }
            else
            {
                slope = 0;
            }

            double diff = it->timestamp - previousTimestamp;
            it->data[trajDimension]->at(targetDimOfSystemState) = previousValue + diff * slope;
            previousTimestamp = it->timestamp;
            previousValue = it->data[trajDimension]->at(targetDimOfSystemState);
        }
    }

    void
    SampledTrajectory::negateDim(unsigned int trajDimension)
    {
        if (trajDimension >= dim())
        {
            throw Exception("dimension is out of range: ")
                << trajDimension << " actual dimensions: " << dim();
        }

        const ordered_view& ordv = dataMap.get<TagOrdered>();

        using namespace math::util;

        SampledTrajectory filteredTraj;

        for (const auto& it : ordv)
        {
            (*it.data.at(trajDimension)) *= -1;
        }
    }

    double
    SampledTrajectory::interpolate(double t, unsigned int dimension, unsigned int deriv) const
    {
        typename timestamp_view::const_iterator it = dataMap.find(t);

        if (it != dataMap.end() && it->data.size() > dimension && it->data.at(dimension) &&
            it->data.at(dimension)->size() > deriv)
        {
            return it->data.at(dimension)->at(deriv);
        }

        const ordered_view& ordv = dataMap.get<TagOrdered>();

        typename ordered_view::iterator itNext = ordv.upper_bound(t);

        typename ordered_view::iterator itPrev = itNext;

        itPrev--;

        double result = interpolate(t, itPrev, itNext, dimension, deriv);

        math::util::checkValue(result);

        return result;
    }

    double
    SampledTrajectory::interpolate(typename ordered_view::const_iterator itMap,
                                   unsigned int dimension,
                                   unsigned int deriv) const
    {
        typename ordered_view::iterator itPrev = itMap;
        itPrev--;
        typename ordered_view::iterator itNext = itMap;
        itNext++;
        return interpolate(itMap->timestamp, itPrev, itNext, dimension, deriv);
    }

    double
    SampledTrajectory::interpolate(double t,
                                   typename ordered_view::const_iterator itPrev,
                                   typename ordered_view::const_iterator itNext,
                                   unsigned int dimension,
                                   unsigned int deriv) const
    {
        //    typename ordered_view::const_iterator itCur = itPrev;
        //    itCur++;

        const ordered_view& ordView = dataMap.get<TagOrdered>();
        double previous = 0;
        double next;

        // find previous SystemState that exists for that dimension
        while (itPrev != ordView.end() && (itPrev->data.at(dimension) ==
                                           nullptr /*|| itPrev->data[dimension]->size() <= deriv*/))
        {
            itPrev--;
        }

        if (itPrev != ordView.end())
        {
            previous = getState(itPrev->timestamp, dimension, deriv);
        }

        // find next SystemState that exists for that dimension
        while (itNext != ordView.end() && (itNext->data.at(dimension) ==
                                           nullptr /*|| itNext->data[dimension]->size() <= deriv*/))
        {
            itNext++;
        }

        if (itNext != ordView.end())
        {
            next = getState(itNext->timestamp, dimension, deriv);
        }

        if (itNext == ordView.end() && itPrev == ordView.end())
        {
            throw Exception() << "Cannot find next or prev values in dim " << dimension
                              << " at timestamp " << t;
        }

        if (itNext == ordView.end())
        {
            return getState(itPrev->timestamp, dimension, deriv) +
                   getState(itPrev->timestamp, dimension, deriv + 1) * (t - itPrev->timestamp);
        }
        if (itPrev == ordView.end())
        {
            return getState(itNext->timestamp, dimension, deriv) -
                   getState(itNext->timestamp, dimension, deriv + 1) * (itNext->timestamp - t);
        }

        // linear interpolation

        double distance = fabs(itNext->timestamp - itPrev->timestamp);
        double weightPrev = fabs(t - itNext->timestamp) / distance;
        double weightNext = fabs(itPrev->timestamp - t) / distance;

        return weightNext * next + weightPrev * previous;
    }
    void
    SampledTrajectory::gaussianFilter(double filterRadius)
    {
        const ordered_view& ordv = dataMap.get<TagOrdered>();
        SampledTrajectory filteredTraj;
        DVec timestamps = getTimestamps();

        for (unsigned int d = 0; d < dim(); d++)
        {
            DVec entries;

            for (typename ordered_view::iterator it = ordv.begin(); it != ordv.end(); it++)
            {
                entries.push_back(gaussianFilter(filterRadius, it, d, 0));
            }

            filteredTraj.addDimension(timestamps, entries);
        }

        copyData(filteredTraj, *this);
    }

// gauss macro
#define g(x) exp(-double((x) * (x)) / (2 * sigma * sigma)) / (sigma * sqrt(2 * 3.14159265))
#define round(x) int(0.5 + x)

    double
    SampledTrajectory::gaussianFilter(double filterRadiusInTime,
                                      typename ordered_view::iterator centerIt,
                                      unsigned int trajNum,
                                      unsigned int dim)
    {
        const ordered_view& ordView = dataMap.get<TagOrdered>();
        //        typename ordered_view::iterator start = centerIt;
        //        while(start != ordView.end() && start->timestamp > centerIt->timestamp - filterRadiusInTime)
        //            start--;
        //        if(start == ordView.end())
        //            start = ordView.begin();

        //        typename ordered_view::iterator end;
        //        while(end != ordView.end() && end->timestamp < centerIt->timestamp + filterRadiusInTime)
        //            end++;

        const double sigma = filterRadiusInTime / 2.5;
        //-log(0.05) / pow(itemsToCheck+1+centerIndex,2) * ( sqrt(1.0/h) * sqrt(2*3.14159265));

        double weightedSum = 0;
        double sumOfWeight = 0;
        ordered_view::iterator start = centerIt;

        const double sqrt2PI = sqrt(2 * M_PI);

        for (int sign = -1; sign < 2; sign += 2)
        {
            for (ordered_view::iterator it = start;
                 it != ordView.end() &&
                 fabs(it->timestamp - centerIt->timestamp) <= fabs(filterRadiusInTime * 2);)
            {
                //            if(centerIt->timestamp >= 15.01)
                //            {
                //                double t = it->timestamp;
                //            }
                double value;
                value = it->getDeriv(trajNum, dim); //data[trajNum]->at(dim);
                double diff = (it->timestamp - centerIt->timestamp);
                double squared = diff * diff;
                const double gaussValue = exp(-squared / (2 * sigma * sigma)) / (sigma * sqrt2PI);
                sumOfWeight += gaussValue;
                weightedSum += gaussValue * value;

                if (sign > 0)
                {
                    it++;
                }
                else
                {
                    it--;
                }

                math::util::checkValue(weightedSum);
            }

            start++; // skip center value in second loop iteration
        }

        double result;
        result = weightedSum / sumOfWeight;
        math::util::checkValue(result);
        //        centerIt->data[trajNum]->at(dim) = result;
        return result;
    }

    void
    SampledTrajectory::copyData(const SampledTrajectory& source, SampledTrajectory& destination)
    {
        if (&source == &destination)
        {
            return;
        }

        destination.clear();
        destination.headerString = source.headerString;
        DVec timestamps = source.getTimestamps();

        for (unsigned int dim = 0; dim < source.dim(); dim++)
        {
            destination.addDimension(timestamps, source.getDimensionData(dim));
        }

        //        typename TrajMap::iterator it = source.begin();
        //        for(; it != source.end(); it++)
        //        {
        //            destination[it->first] = it->second;
        //            Vec< DVecPtr >& vecS = it->second;
        //            Vec< DVecPtr >& vecD = destination[it->first];
        //            for(unsigned int i = 0; i < vecS.size(); i++)
        //            {
        //                vecD[i] = new DVecPtr(*vecS[i]);
        //            }
        //        }
    }

    void
    SampledTrajectory::clear()
    {
        dataMap.erase(dataMap.begin(), dataMap.end());
    }

    void
    SampledTrajectory::readFromCSVFileWithQuery(const std::string& filepath,
                                                DVec& query,
                                                bool withHead,
                                                int derivations)
    {
        clear();
        std::ifstream f(filepath.c_str());

        if (!f.is_open())
        {
            throw Exception("Could not open file: " + filepath);
        }

        using Tokenizer = boost::tokenizer<boost::escaped_list_separator<char>>;

        std::vector<std::string> stringVec;
        std::string line;

        //        unsigned int stateDim = double().size();
        unsigned int i = 1;

        while (getline(f, line))
        {
            if (withHead && i == 2)
            {
                headerString = line;
                i++;
                continue;
            }

            Tokenizer tok(line);
            stringVec.assign(tok.begin(), tok.end());

            if (/*-1+stringVec.size() % stateDim !=0 || */ stringVec.size() == 0)
            {
                std::cout << "Line " << i << " is invalid  skipping: " << line << std::endl;
                continue;
            }

            if (i == 1)
            {
                query.clear();
                for (const auto& j : stringVec)
                {
                    query.push_back(math::util::fromString<double>(j));
                }
                i++;
            }
            else
            {
                TrajData entry(*this);
                entry.timestamp = math::util::fromString<double>(stringVec[0]);
                unsigned int j = 1;

                int size = stringVec.size() - 1;
                //            DVec values(int(ceil(size/stateDim)*stateDim), 0.0);
                DVec values(size, 0.0);

                for (unsigned int sd = 0; j < stringVec.size(); sd++, ++j)
                {
                    values.at(sd) = (math::util::fromString<double>(stringVec[j]));
                    math::util::checkValue(values.at(sd));
                }

                //            for (DVec::iterator it = values.begin(); it != values.end(); it+=stateDim+columnsToSkip) {
                for (unsigned int d = 0; d < values.size(); d += 1 + derivations)
                {
                    DVecPtr statePtr(
                        new DVec(DVec(values.begin() + d, values.begin() + d + 1 + derivations)));
                    entry.data.push_back(statePtr);
                }

                dataMap.insert(entry);

                i++;
            }
        }

        std::cout << "file loaded: read " << i << " lines from " << filepath << " with " << dim()
                  << " dimensions" << std::endl;
    }

    void
    SampledTrajectory::readFromCSVFile(const std::string& filepath,
                                       bool noTimeStamp,
                                       int derivations,
                                       int columnsToSkip,
                                       bool containsHeader)
    {
        clear();
        std::ifstream f(filepath.c_str());

        if (!f.is_open())
        {
            throw Exception("Could not open file: " + filepath);
        }

        using Tokenizer = boost::tokenizer<boost::escaped_list_separator<char>>;

        std::vector<std::string> stringVec;
        std::string line;

        //        unsigned int stateDim = double().size();
        unsigned int i = 1;

        double current_time = 0;
        while (getline(f, line))
        {
            if (containsHeader && i == 1)
            {
                headerString = line;
                Tokenizer tok(line);
                stringVec.assign(tok.begin(), tok.end());

                dimNames.clear();

                for (size_t j = 1; j < stringVec.size(); ++j)
                {
                    dimNames.push_back(stringVec.at(j));
                }

                i++;
                continue;
            }

            Tokenizer tok(line);
            stringVec.assign(tok.begin(), tok.end());

            if (/*-1+stringVec.size() % stateDim !=0 || */ stringVec.size() == 0)
            {
                std::cout << "Line " << i << " is invalid  skipping: " << line << std::endl;
                continue;
            }

            unsigned int j = 1;
            TrajData entry(*this);
            int size = stringVec.size() - 1;
            if (noTimeStamp)
            {
                j = 0;
                entry.timestamp = current_time;
                current_time += 0.01;
                size = stringVec.size();
            }
            else
            {
                entry.timestamp = math::util::fromString<double>(stringVec[0]);
            }

            //            DVec values(int(ceil(size/stateDim)*stateDim), 0.0);
            DVec values(size, 0.0);

            for (unsigned int sd = 0; j < stringVec.size(); sd++, ++j)
            {
                values.at(sd) = (math::util::fromString<double>(stringVec[j]));
                math::util::checkValue(values.at(sd));
            }

            //            for (DVec::iterator it = values.begin(); it != values.end(); it+=stateDim+columnsToSkip) {
            for (unsigned int d = 0; d < values.size(); d += 1 + derivations + columnsToSkip)
            {
                DVecPtr statePtr(
                    new DVec(DVec(values.begin() + d, values.begin() + d + 1 + derivations)));
                entry.data.push_back(statePtr);
            }

            dataMap.insert(entry);

            i++;
        }

        std::cout << "file loaded: read " << i << " lines from " << filepath << " with " << dim()
                  << " dimensions" << std::endl;
    }

    void
    SampledTrajectory::writeToCSVFile(const std::string& filepath, bool noTimeStamp, char delimiter)
    {
        //    clear();
        std::ofstream f(filepath.c_str());

        if (!f.is_open())
        {
            throw Exception("Could not open file: " + filepath);
        }
        const ordered_view& ordv = dataMap.get<TagOrdered>();
        typename ordered_view::const_iterator itMap = ordv.begin();

        for (; itMap != ordv.end(); itMap++)
        {
            std::vector<DVecPtr> data = itMap->getData();

            if (!noTimeStamp)
            {
                f << itMap->getTimestamp() << delimiter;
            }
            for (size_t i = 0; i < data.size(); ++i)
            {
                DVecPtr cdata = data[i];

                f << cdata->at(0);

                if (i != data.size() - 1)
                {
                    f << delimiter;
                }
            }
            f << '\n';
        }

        f.close();
    }

    void
    SampledTrajectory::writeTrajToCSVFile(const std::string& filepath,
                                          float timestep,
                                          float startTime,
                                          float endTime)
    {
        std::ofstream f(filepath.c_str());

        if (!f.is_open())
        {
            throw Exception("Could not open file: " + filepath);
        }

        for (float t = startTime; t < endTime; t = t + timestep)
        {
            std::vector<DVecPtr> data = getStates(t);

            f << t;
            f << ',';
            for (size_t i = 0; i < data.size(); ++i)
            {
                DVecPtr cdata = data[i];

                f << cdata->at(0);

                if (i != data.size() - 1)
                {
                    f << ',';
                }
            }
            f << '\n';
        }

        f.close();
    }

    std::string
    SampledTrajectory::writeTrajToCSVString(int numOfSamples)
    {
        float timestep = 1.0 / (float)numOfSamples;

        float t = 0;
        std::stringstream f;
        for (int counter = 0; counter < numOfSamples; counter++)
        {
            std::vector<DVecPtr> data = getStates(t);

            for (size_t i = 0; i < data.size(); ++i)
            {
                DVecPtr cdata = data[i];

                f << cdata->at(0);

                if (i != data.size() - 1)
                {
                    f << ',';
                }
            }

            if (counter != numOfSamples - 1)
            {
                f << ',';
            }

            t = t + timestep;
        }

        return f.str();
    }

    DVec
    SampledTrajectory::generateTimestamps(double startTime, double endTime, double stepSize)
    {
        if (startTime >= endTime)
        {
            throw Exception("startTime must be smaller than endTime.");
        }

        DVec result;
        int size = int((endTime - startTime) / stepSize) + 1;
        stepSize = (endTime - startTime) / (size - 1);
        result.reserve(size);

        double currentTimestamp = startTime;
        int i = 0;

        while (i < size)
        {
            result.push_back(currentTimestamp);
            currentTimestamp += stepSize;
            i++;
        }

        //        if(*result.rbegin() != endTime)
        //            result.push_back(endTime);
        return result;
    }

    DVec
    SampledTrajectory::normalizeTimestamps(const DVec& timestamps,
                                           const double startTime,
                                           const double endTime)
    {
        DVec normTimestamps;
        normTimestamps.resize(timestamps.size());
        const double minValue = *timestamps.begin();
        const double maxValue = *timestamps.rbegin();
        const double duration = maxValue - minValue;

        for (unsigned int i = 0; i < timestamps.size(); i++)
        {
            normTimestamps[i] =
                startTime + (timestamps.at(i) - minValue) / duration * (endTime - startTime);
        }

        return normTimestamps;
    }

    SampledTrajectory
    SampledTrajectory::normalizeTimestamps(const SampledTrajectory& traj,
                                           const double startTime,
                                           const double endTime)
    {

        if (traj.size() <= 1 ||
            (traj.begin()->timestamp == startTime && traj.rbegin()->timestamp == endTime))
        {
            return traj; // already normalized
        }

        DVec timestamps = traj.getTimestamps();

        DVec normTimestamps = normalizeTimestamps(timestamps, startTime, endTime);
        SampledTrajectory normExampleTraj;

        for (unsigned int dim = 0; dim < traj.dim(); dim++)
        {
            DVec dimensionData = traj.getDimensionData(dim);
            normExampleTraj.addDimension(normTimestamps, dimensionData);
        }

        return normExampleTraj;
    }

    unsigned int
    SampledTrajectory::addDimension(const DVec& x, const DVec& y)
    {

        unsigned int newDim = dim() + 1;

        addDimension();

        addPositionsToDimension(newDim - 1, x, y);
        return newDim - 1;
    }

    void
    SampledTrajectory::removeDimension(unsigned int dimension)
    {
        typename timestamp_view::iterator itMap = dataMap.begin();

        for (; itMap != dataMap.end(); itMap++)
        {
            std::vector<DVecPtr>& data = itMap->data;

            if (dimension < data.size())
            {
                data.erase(data.begin() + dimension);
            }
        }
    }

    void
    SampledTrajectory::removeDerivation(unsigned int deriv)
    {
        typename timestamp_view::iterator itMap = dataMap.begin();

        for (; itMap != dataMap.end(); itMap++)
        {
            std::vector<DVecPtr>& data = itMap->data;

            for (auto& vec : data)
            {
                if (deriv + 1 < vec->size())
                {
                    vec->resize(deriv);
                }
            }
        }
    }

    void
    SampledTrajectory::removeDerivation(unsigned int dimension, unsigned int deriv)
    {
        typename timestamp_view::iterator itMap = dataMap.begin();

        for (; itMap != dataMap.end(); itMap++)
        {
            std::vector<DVecPtr>& data = itMap->data;

            if (data.size() > dimension && deriv + 1 < data.at(dimension)->size())
            {
                data.at(dimension)->resize(deriv);
            }
        }
    }

    SampledTrajectory
    SampledTrajectory::traj2qtraj() const
    {
        typename ordered_view::const_iterator itMap = begin();
        std::map<double, DVec> res;

        for (; itMap != end(); itMap++)
        {
            double curTime = itMap->getTimestamp();
            DVec curData;
            std::vector<DVecPtr> data = itMap->getData();

            for (size_t i = 0; i <= data.size() - 3; i += 3)
            {
                double yaw = data[i]->at(0);
                double pitch = data[i + 1]->at(0);
                double roll = data[i + 2]->at(0);

                Eigen::Quaterniond q = math::util::angle2quat(yaw, pitch, roll);
                curData.push_back(q.w());
                curData.push_back(q.x());
                curData.push_back(q.y());
                curData.push_back(q.z());
            }

            res[curTime] = curData;
        }

        return SampledTrajectory(res);
    }

    SampledTrajectory
    SampledTrajectory::qtraj2traj() const
    {
        typename ordered_view::const_iterator itMap = begin();
        std::map<double, DVec> res;

        for (; itMap != end(); itMap++)
        {
            double curTime = itMap->getTimestamp();
            DVec curData;
            std::vector<DVecPtr> data = itMap->getData();

            for (size_t i = 0; i <= data.size() - 4; i += 4)
            {
                double w = data[i]->at(0);
                double x = data[i + 1]->at(0);
                double y = data[i + 2]->at(0);
                double z = data[i + 3]->at(0);

                Eigen::Quaterniond q(w, x, y, z);
                Eigen::Vector3d ypr = math::util::quat2angle(q);

                curData.push_back(ypr(0));
                curData.push_back(ypr(1));
                curData.push_back(ypr(2));
            }

            res[curTime] = curData;
        }
        return SampledTrajectory(res);
    }

    SampledTrajectory
    SampledTrajectory::traj2dqtraj() const
    {
        typename ordered_view::const_iterator itMap = begin();
        std::map<double, DVec> res;

        for (; itMap != end(); itMap++)
        {
            double curTime = itMap->getTimestamp();
            DVec curData;
            std::vector<DVecPtr> data = itMap->getData();

            for (size_t i = 0; i <= data.size() - 6; i += 6)
            {
                double yaw = data[i]->at(0);
                double pitch = data[i + 1]->at(0);
                double roll = data[i + 2]->at(0);

                double tx = data[i + 3]->at(0);
                double ty = data[i + 4]->at(0);
                double tz = data[i + 5]->at(0);

                Eigen::Quaterniond r = math::util::angle2quat(yaw, pitch, roll);
                curData.push_back(r.w());
                curData.push_back(r.x());
                curData.push_back(r.y());
                curData.push_back(r.z());

                Eigen::Quaterniond t = Eigen::Quaterniond(0, tx, ty, tz);
                Eigen::Quaterniond d((0.5 * t.coeffs()).cwiseProduct(r.coeffs()));
                curData.push_back(d.w());
                curData.push_back(d.x());
                curData.push_back(d.y());
                curData.push_back(d.z());
            }

            res[curTime] = curData;
        }

        return SampledTrajectory(res);
    }

    SampledTrajectory
    SampledTrajectory::dqtraj2traj() const
    {
        using namespace math::util;

        typename ordered_view::const_iterator itMap = begin();
        std::map<double, DVec> res;

        for (; itMap != end(); itMap++)
        {
            double curTime = itMap->getTimestamp();
            DVec curData;
            std::vector<DVecPtr> data = itMap->getData();

            for (size_t i = 0; i <= data.size() - 8; i += 8)
            {
                double rw = data[i]->at(0);
                double rx = data[i + 1]->at(0);
                double ry = data[i + 2]->at(0);
                double rz = data[i + 3]->at(0);
                double dw = data[i + 4]->at(0);
                double dx = data[i + 5]->at(0);
                double dy = data[i + 6]->at(0);
                double dz = data[i + 7]->at(0);

                Eigen::Quaterniond r(rw, rx, ry, rz);
                Eigen::Quaterniond d(dw, dx, dy, dz);
                Eigen::Vector3d ypr = math::util::quat2angle(r);

                Eigen::Quaterniond t((2 * d.coeffs()).cwiseProduct(r.conjugate().coeffs()));

                curData.push_back(ypr(0));
                curData.push_back(ypr(1));
                curData.push_back(ypr(2));
                curData.push_back(t.x());
                curData.push_back(t.y());
                curData.push_back(t.z());
            }

            res[curTime] = curData;
        }
        return SampledTrajectory(res);
    }

    SampledTrajectory
    SampledTrajectory::downSample(const SampledTrajectory& traj, int newlen)
    {
        DVec origTimeStamps = traj.getTimestamps();
        double t0 = *(origTimeStamps.begin());
        double tT = *(origTimeStamps.rbegin());

        if (newlen <= 1)
        {
            throw Exception() << "The length of trajectory should bigger than 1";
        }

        double dt = (tT - t0) / (newlen - 1);

        SampledTrajectory res;
        double t = t0;
        while (t <= tT)
        {
            for (unsigned int dimIdx = 0; dimIdx < traj.dim(); ++dimIdx)
            {
                DVec y{traj.getState(t, dimIdx)};
                res.setEntries(t, dimIdx, y);
            }
            t += dt;
        }
        return res;
    }

    void
    SampledTrajectory::scale(double ratio)
    {
        const ordered_view& ordv = dataMap.get<TagOrdered>();
        typename ordered_view::const_iterator itMap = ordv.begin();
        for (; itMap != ordv.end(); ++itMap)
        {
            for (unsigned int dimIdx = 0; dimIdx < dim(); ++dimIdx)
            {
                itMap->data[dimIdx]->at(0) *= ratio;
            }
        }
    }

    SampledTrajectory::ordered_view::const_iterator
    SampledTrajectory::begin() const
    {
        return dataMap.get<TagOrdered>().begin();
    }

    SampledTrajectory::ordered_view::const_iterator
    SampledTrajectory::end() const
    {
        return dataMap.get<TagOrdered>().end();
    }

    SampledTrajectory::ordered_view::const_reverse_iterator
    SampledTrajectory::rbegin() const
    {
        return dataMap.get<TagOrdered>().rbegin();
    }

    SampledTrajectory::ordered_view::const_reverse_iterator
    SampledTrajectory::rend() const
    {
        return dataMap.get<TagOrdered>().rend();
    }

    void
    SampledTrajectory::addPositionsToDimension(unsigned int dimension, const DVec& x, const DVec& y)
    {
        if (dimension >= dim() && dim() > 0)
        {
            addDimension(x, y);
        }
        else
        {
            assert(x.size() == y.size());

            for (unsigned int i = 0; i < x.size() && i < y.size(); ++i)
            {
                math::util::checkValue(x[i]);
                math::util::checkValue(y[i]);
                setPositionEntry(x[i], dimension, y[i]);
            }
        }
    }

    void
    SampledTrajectory::addDerivationsToDimension(unsigned int dimension,
                                                 const double t,
                                                 const DVec& derivs)
    {
        setEntries(t, dimension, derivs);
    }

    SampledTrajectory::TrajData::TrajData(SampledTrajectory& traj) : trajectory(traj)
    {
    }

    void
    SampledTrajectory::shiftTime(double /*shift*/)
    {
        const ordered_view& ordv = dataMap.get<TagOrdered>();
        typename ordered_view::const_iterator itMap = ordv.begin();

        for (; itMap != ordv.end(); itMap++)
        {
            //itMap->timestamp -= shift;
        }
    }

    void
    SampledTrajectory::shiftValue(const DVec& shift)
    {
        if (shift.size() > dim())
        {
            throw Exception("dimension is out of range: ")
                << shift.size() << " actual dimensions: " << dim();
        }

        for (unsigned int dimension = 0; dimension < dim(); dimension++)
        {
            const ordered_view& ordv = dataMap.get<TagOrdered>();
            typename ordered_view::const_iterator itMap = ordv.begin();

            for (; itMap != ordv.end(); itMap++)
            {
                itMap->data[dimension]->at(0) += shift[dimension];
            }
        }
    }

    void
    SampledTrajectory::saveToFile(std::string filename)
    {
        SampledTrajectory::ordered_view::const_iterator itTraj = begin();

        std::ofstream ofs(filename);
        std::cout << "dim: " << dim() << std::endl;
        for (; itTraj != end(); itTraj++)
        {
            ofs << itTraj->getTimestamp();
            ofs << ",";
            for (size_t i = 0; i < dim(); ++i)
            {

                ofs << itTraj->getPosition(i);
                if (i != dim() - 1)
                {
                    ofs << ",";
                }
                else
                {
                    ofs << "\n";
                }
            }
        }

        ofs.close();
    }

    DVec
    SampledTrajectory::calcAnchorPoint()
    {
        DVec anchor(dim());

        for (unsigned int dimension = 0; dimension < dim(); dimension++)
        {
            int startInd = 0;
            double startTime = getTimestamps()[startInd];
            double maxi = getMax(dimension, 0, startTime, startTime + getTimeLength());
            double mini = getMin(dimension, 0, startTime, startTime + getTimeLength());

            anchor[dimension] = (maxi + mini) / 2;
        }

        return anchor;
    }

    std::map<double, DVec>
    SampledTrajectory::getPositionData() const
    {
        const ordered_view& ordv = dataMap.get<TagOrdered>();
        typename ordered_view::const_iterator itMap = ordv.begin();

        std::map<double, DVec> res;
        for (; itMap != ordv.end(); itMap++)
        {
            DVec dimData;
            dimData.resize(dim());
            for (unsigned int di = 0; di < dim(); di++)
            {
                double cdata = itMap->getPosition(di);
                dimData[di] = cdata;
            }

            res.insert(std::pair<double, DVec>(itMap->getTimestamp(), dimData));
        }

        return res;
    }

    SampledTrajectory
    SampledTrajectory::getSegment(double startTime, double endTime)
    {
        double dt = getTimestamps()[1] - getTimestamps()[0];

        std::map<double, DVec> resdata;
        for (double t = startTime; t < endTime; t += dt)
        {
            std::vector<DVecPtr> data = getStates(t);
            DVec curData;

            for (auto& i : data)
            {
                curData.push_back(i->at(0));
            }

            resdata.insert(std::pair<double, DVec>(t, curData));
        }

        return SampledTrajectory(resdata);
    }

    std::vector<SampledTrajectory>
    SampledTrajectory::getSegments(int num)
    {
        std::vector<SampledTrajectory> res;

        DVec timestamps = getTimestamps();
        int nt = timestamps.size();
        double timeLength = getTimeLength() / (double)num;

        for (double t = timestamps[0]; t < timestamps[nt - 1] - timeLength; t = t + timeLength)
        {
            SampledTrajectory cres = getSegment(t, t + timeLength);
            res.push_back(cres);
        }

        return res;

        return res;
    }

    double
    SampledTrajectory::getPeriodLength() const
    {
        return mPeriodLength;
    }

    double
    SampledTrajectory::getTransientLength() const
    {
        return mTransientLength;
    }

    SampledTrajectory
    SampledTrajectory::normalizeTraj(double min, double max)
    {
        std::map<double, DVec> res;

        const ordered_view& ordv = dataMap.get<TagOrdered>();
        typename ordered_view::const_iterator itMap = ordv.begin();

        DVec maxData;
        DVec minData;
        for (size_t di = 0; di < dim(); di++)
        {
            double cmax = getMax(di, 0, *(getTimestamps().begin()), *(getTimestamps().rbegin()));
            double cmin = getMin(di, 0, *(getTimestamps().begin()), *(getTimestamps().rbegin()));

            maxData.push_back(cmax);
            minData.push_back(cmin);
        }

        for (; itMap != ordv.end(); itMap++)
        {
            DVec dimData;
            dimData.resize(dim());
            for (size_t di = 0; di < dim(); di++)
            {
                double cdata = itMap->getPosition(di);

                if (maxData[di] != minData[di])
                {
                    cdata = min + (max - min) * (cdata - minData[di]) / (maxData[di] - minData[di]);
                }
                else
                {
                    cdata = minData[di];
                }
                dimData[di] = cdata;
            }

            res.insert(std::pair<double, DVec>(itMap->getTimestamp(), dimData));
        }

        return SampledTrajectory(res);
    }

} // namespace mplib::core
