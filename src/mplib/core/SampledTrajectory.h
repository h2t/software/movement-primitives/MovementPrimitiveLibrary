#pragma once

#include "SystemState.h"
#include "types.h"
#include <mplib/math/util/eigen_arithmetic.h>

// TODO remove from header (forward declaration not working because of iterator)
#include <memory>

#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index_container.hpp>

#include <Eigen/Core>

namespace mplib::core
{
    enum InterpolationMethod
    {
        ImLinear
    };

    using DVecPtr = std::shared_ptr<DVec>;

    class SampledTrajectory
    {
    public:
        struct TrajData
        {
            TrajData(SampledTrajectory& traj);
            DVecPtr
            operator[](unsigned int dim)
            {
                return data.at(dim);
            }
            inline double
            getTimestamp() const
            {
                return timestamp;
            }
            inline double
            getPosition(int dim) const
            {
                return getDeriv(dim, 0);
            }
            inline double
            getDeriv(int dim, int derivation) const
            {
                return trajectory.getState(timestamp, dim, derivation);
            }
            inline const std::vector<DVecPtr>&
            getData() const
            {
                return data;
            }

            double timestamp;
            mutable std::vector<DVecPtr> data; // mutable so that it can be changed
            SampledTrajectory& trajectory;

            friend class SampledTrajectory;
        };

        // structs for indices
        struct TagTimestamp
        {
        };
        struct TagOrdered
        {
        };

        // Trajectory data container
        using TrajDataContainer = boost::multi_index::multi_index_container<
            TrajData,
            boost::multi_index::indexed_by<
                boost::multi_index::hashed_unique<
                    boost::multi_index::tag<TagTimestamp>,
                    boost::multi_index::member<TrajData, double, &TrajData::timestamp>>,
                boost::multi_index::ordered_unique<
                    boost::multi_index::tag<TagOrdered>,
                    boost::multi_index::member<TrajData, double, &TrajData::timestamp>>>>;
        using timestamp_view =
            typename boost::multi_index::index<TrajDataContainer, TagTimestamp>::type;
        using ordered_view =
            typename boost::multi_index::index<TrajDataContainer, TagOrdered>::type;

        SampledTrajectory();
        SampledTrajectory(const SampledTrajectory& source);
        SampledTrajectory(const DVec& x, const DVec& y);
        SampledTrajectory(const std::map<double, DVec>& data);
        SampledTrajectory(const std::map<double, Eigen::Matrix4d>& data, bool useReferenceQuaternion = true);

        template <typename SystemState>
        SampledTrajectory(const std::map<double, std::vector<SystemState>>& data)
        {
            if (data.empty())
            {
                return;
            }

            auto it = data.begin();

            for (; it != data.end(); it++)
            {
                TrajData dataEntry(*this);
                dataEntry.timestamp = it->first;
                const std::vector<SystemState>& dataVec = it->second;

                for (const auto& i : dataVec)
                {

                    dataEntry.data.push_back(std::make_shared<DVec>(i.getValues()));
                }

                dataMap.insert(dataEntry);
            }
        }

        SampledTrajectory(const core::DVec& data, double timestep = 0.001);

        virtual ~SampledTrajectory();

        SampledTrajectory& operator=(const SampledTrajectory& source);

        unsigned int addDimension(const DVec& x, const DVec& y);
        void addPositionsToDimension(unsigned int dimension, const DVec& x, const DVec& y);
        void addDerivationsToDimension(unsigned int dimension, const double t, const DVec& derivs);
        SampledTrajectory& operator+=(const SampledTrajectory traj);
        void removeDimension(unsigned int dimension);
        void removeDerivation(unsigned int deriv);
        void removeDerivation(unsigned int dimension, unsigned int deriv);

        // quaternion related
        SampledTrajectory traj2qtraj() const;
        SampledTrajectory qtraj2traj() const;
        SampledTrajectory traj2dqtraj() const;
        SampledTrajectory dqtraj2traj() const;

        static SampledTrajectory downSample(const SampledTrajectory& traj, int newlen);
        void scale(double ratio);

        // iterators
        typename ordered_view::const_iterator begin() const;
        typename ordered_view::const_iterator end() const;
        typename ordered_view::const_reverse_iterator rbegin() const;
        typename ordered_view::const_reverse_iterator rend() const;

        ///// getters
        DVec getTimestamps() const;
        const std::vector<DVecPtr>& getStates(double t);
        std::vector<DVecPtr> getStates(double t) const;
        DVec getStates(double t, unsigned int derivation) const;

        double getState(double t, unsigned int dim = 0, unsigned int deriv = 0);
        double getState(double t, unsigned int dim = 0, unsigned int deriv = 0) const;
        DVec getDerivations(double t, unsigned int dimension, unsigned int derivations) const;

        TrajDataContainer& data();

        /**
         * @brief getDimensionData gets all entries for one dimensions with order of increasing timestamps
         * @param dimension
         * @return
         */
        //        DVec getDimensionData(unsigned int dimension = 0) const;
        DVec getDimensionData(unsigned int dimension, unsigned int deriv = 0) const;
        Eigen::VectorXd getDimensionDataAsEigen(unsigned int dimension,
                                                unsigned int derivation) const;
        Eigen::VectorXd getDimensionDataAsEigen(unsigned int dimension,
                                                unsigned int derivation,
                                                double startTime,
                                                double endTime) const;
        Eigen::MatrixXd toEigen(unsigned int derivation, double startTime, double endTime) const;
        Eigen::MatrixXd toEigen(unsigned int derivation = 0) const;
        SampledTrajectory
        getPart(double startTime, double endTime, unsigned int numberOfDerivations = 0) const;
        /**
         * @brief dim returns the trajectory dimension count for this trajectory (e.g. taskspace w/o orientation would be 3)
         * @return
         */
        unsigned int dim() const;
        unsigned int size() const;
        double getTimeLength() const;
        double getSpaceLength(unsigned int dimension, unsigned int stateDim) const;
        double getSpaceLength(unsigned int dimension,
                              unsigned int stateDim,
                              double startTime,
                              double endTime) const;
        double getSquaredSpaceLength(unsigned int dimension, unsigned int stateDim) const;
        double getSquaredSpaceLength(unsigned int dimension,
                                     unsigned int stateDim,
                                     double startTime,
                                     double endTime) const;
        double getMax(unsigned int dimension,
                      unsigned int stateDim,
                      double startTime,
                      double endTime) const;
        double getMin(unsigned int dimension,
                      unsigned int stateDim,
                      double startTime,
                      double endTime) const;
        double getWithFunc(const double& (*foo)(const double&, const double&),
                           double initValue,
                           unsigned int dimension,
                           unsigned int stateDim,
                           double startTime,
                           double endTime) const;
        double getTrajectorySpace(unsigned int dimension,
                                  unsigned int stateDim,
                                  double startTime,
                                  double endTime) const;
        DVec getMinima(unsigned int dimension,
                       unsigned int stateDim,
                       double startTime,
                       double endTime) const;
        DVec getMaxima(unsigned int dimension,
                       unsigned int stateDim,
                       double startTime,
                       double endTime) const;
        DVec getMinimaPositions(unsigned int dimension,
                                unsigned int stateDim,
                                double startTime,
                                double endTime) const;
        DVec getMaximaPositions(unsigned int dimension,
                                unsigned int stateDim,
                                double startTime,
                                double endTime) const;
        DVec calcAnchorPoint();
        std::map<double, DVec> getPositionData() const;

        SampledTrajectory getSegment(double startTime, double endTime);
        std::vector<SampledTrajectory> getSegments(int n);

        double getPeriodLength() const;
        double getTransientLength() const;

        SampledTrajectory normalizeTraj(double min = 0.0, double max = 1.0);
        // calculations

        DVec getDiscretDifferentiationForDim(unsigned int trajDimension,
                                             unsigned int derivation) const;
        static DVec
        differentiateDiscretly(const DVec& timestamps, const DVec& values, int derivationCount = 1);
        double getDiscretDifferentiationForDimAtT(double t,
                                                  unsigned int trajDimension,
                                                  unsigned int deriv) const;

        void differentiateDiscretlyForDim(unsigned int trajDimension, unsigned int derivation);
        void differentiateDiscretly(unsigned int derivation);
        //        void differentiateDiscretly( unsigned int sourceDimOfSystemState, unsigned int targetDimOfSystemState);

        void reconstructFromDerivativeForDim(double valueAtFirstTimestamp,
                                             unsigned int trajDimension,
                                             unsigned int sourceDimOfSystemState,
                                             unsigned int targetDimOfSystemState);

        /**
         * @brief negateDim changes the sign of all values of the given dimension.
         */
        void negateDim(unsigned int trajDimension);
        /**
         * @brief gaussianFilter smoothes the trajectory
         * @param filterRadius, in easy words: time range to left and right from center that
         * influences the resulting center value
         */
        void gaussianFilter(double filterRadius);

        static SampledTrajectory normalizeTimestamps(const SampledTrajectory& traj,
                                                     const double startTime = 0.0,
                                                     const double endTime = 1.0);
        static DVec normalizeTimestamps(const DVec& timestamps,
                                        const double startTime = 0.0,
                                        const double endTime = 1.0);
        static DVec
        generateTimestamps(double startTime = 0.0, double endTime = 1.0, double stepSize = 0.001);
        void shiftTime(double shift);
        void shiftValue(const DVec& shift);

        void saveToFile(std::string filename);
        // serialization
        friend class boost::serialization::access;
        template <class Archive>
        void
        save(Archive& ar, const unsigned int /*fileVersion*/) const
        {

            unsigned int dimensions = dim();
            ar& BOOST_SERIALIZATION_NVP(dimensions);
            core::DVec times = getTimestamps();
            ar& BOOST_SERIALIZATION_NVP(times);

            for (double time : times)
            {
                const std::vector<DVecPtr> tData = getStates(time);

                for (size_t d = 0; d < dimensions; d++)
                {
                    core::DVec& derivations = *tData.at(d);
                    ar& BOOST_SERIALIZATION_NVP(derivations);
                }
            }
        }

        template <class Archive>
        void
        load(Archive& ar, const unsigned int /*fileVersion*/)
        {

            unsigned int dimensions;
            ar& BOOST_SERIALIZATION_NVP(dimensions);
            core::DVec times;
            ar& BOOST_SERIALIZATION_NVP(times);

            for (double time : times)
            {
                for (size_t d = 0; d < dimensions; d++)
                {
                    DVec derivs;
                    core::DVec& derivations = derivs;
                    ar& BOOST_SERIALIZATION_NVP(derivations);
                    addDerivationsToDimension(d, time, derivs);
                }
            }
        }
        BOOST_SERIALIZATION_SPLIT_MEMBER()

        // misc
        double mPhi1;
        double mPeriodLength;
        double mTransientLength;

        //        typedef boost::unordered::unordered_map<double, Vec< DVecPtr > > TrajMap;
        void readFromCSVFileWithQuery(const std::string& filepath,
                                      DVec& query,
                                      bool withHead = true,
                                      int derivations = 0);
        void readFromCSVFile(const std::string& filepath,
                             bool noTimeStamp = false,
                             int derivations = 0,
                             int columnsToSkip = 0,
                             bool containsHeader = true);
        void
        writeToCSVFile(const std::string& filepath, bool noTimeStamp = true, char delimiter = ',');
        void writeTrajToCSVFile(const std::string& filepath,
                                float timestep = 0.001,
                                float startTime = 0,
                                float endTime = 1.0);

        static void copyData(const SampledTrajectory& source, SampledTrajectory& destination);
        void clear();
        void setPositionEntry(const double t, const unsigned int dimIndex, const double& y);
        void setEntries(const double t, const unsigned int dimIndex, const DVec& y);
        bool dataExists(double t, unsigned int dimension = 0, unsigned int deriv = 0) const;
        double getPhi1() const;
        void setPhi1(double phiNew);

        void setPeriodLength(double newPeriodLength);
        void setTransientLength(double newTransientLength);

        std::map<double, DVec>
        getStatesAround(double t, unsigned int derivation, unsigned int extend) const;

        std::vector<std::string> getDimensionNames();

        std::string writeTrajToCSVString(int numOfSamples);

    protected:
        void addDimension();
        void fillAllEmptyFields();
        double interpolate(typename ordered_view::const_iterator itMap,
                           unsigned int dimension,
                           unsigned int deriv) const;
        double gaussianFilter(double filterRadius,
                              typename ordered_view::iterator centerIt,
                              unsigned int trajNum,
                              unsigned int dim);
        timestamp_view::iterator fillBaseDataAtTimestamp(const double& t);
        std::vector<DVecPtr> calcBaseDataAtTimestamp(const double& t) const;

        std::string headerString;
        TrajDataContainer dataMap;

        std::vector<std::string> dimNames;

        //        //! Counts the dimensions that were inserted into this trajectory.
        //        unsigned int dimensions;

        virtual double interpolate(double t,
                                   ordered_view::const_iterator itPrev,
                                   ordered_view::const_iterator itNext,
                                   unsigned int dimension,
                                   unsigned int deriv) const;
        double interpolate(double t, unsigned int dimension, unsigned int deriv) const;
    };
} // namespace mplib::core
