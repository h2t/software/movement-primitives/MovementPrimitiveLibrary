#include "Exception.h"

#ifndef _WIN32
#include <cxxabi.h>
#include <dlfcn.h>
#include <execinfo.h>
#endif

#include <cstdio>
#include <cstdlib>
#include <sstream>

namespace mplib::core
{

    Exception::Exception() : backtrace("")
    {
        backtrace = generateBacktrace();
        setReason("");
    }

    Exception::Exception(const Exception& e)

    {
        reason << e.reason.str();
        backtrace = e.backtrace;
        resultingMessage = e.resultingMessage;
    }

    Exception::Exception(const std::string& reason)
    {
        backtrace = generateBacktrace();
        setReason(reason);
    }

    Exception::~Exception() noexcept = default;

    void
    Exception::setReason(std::string reason)
    {
        this->reason.str("");
        this->reason.clear();

        this->reason << reason;
    }

    const char*
    Exception::what() const noexcept
    {
        generateOutputString();
        return resultingMessage.c_str();
    }

    std::string
    Exception::name() const
    {
        return "MPLIB::Exception";
    }

    std::string
    Exception::generateBacktrace()
    {
#ifdef _WIN32
        return "no Backtrace under Windows!";
#else
        int skip = 1;
        void* callstack[128];
        const int nMaxFrames = sizeof(callstack) / sizeof(callstack[0]);
        char buf[1024];

        int nFrames = ::backtrace(callstack, nMaxFrames);
        char** symbols = backtrace_symbols(callstack, nFrames);

        std::ostringstream trace_buf;

        for (int i = skip; i < nFrames; i++)
        {
            Dl_info info;

            if (dladdr(callstack[i], &info) && info.dli_sname)
            {
                char* demangled = nullptr;
                int status = -1;

                if (info.dli_sname[0] == '_')
                {
                    demangled = abi::__cxa_demangle(info.dli_sname, nullptr, nullptr, &status);
                }

                snprintf(buf,
                         sizeof(buf),
                         "%-3d %*p %s + %zd\n",
                         i - skip + 1,
                         int(2 + sizeof(void*) * 2),
                         callstack[i],
                         status == 0                 ? demangled
                         : info.dli_sname == nullptr ? symbols[i]
                                                     : info.dli_sname,
                         (char*)callstack[i] - (char*)info.dli_saddr);
                free(demangled);
            }
            else
            {
                snprintf(buf,
                         sizeof(buf),
                         "%-3d %*p %s\n",
                         i - skip + 1,
                         int(2 + sizeof(void*) * 2),
                         callstack[i],
                         symbols[i]);
            }

            trace_buf << buf;
        }

        free(symbols);

        if (nFrames == nMaxFrames)
        {
            trace_buf << "[truncated]\n";
        }

        return trace_buf.str();
#endif
    }

    void
    Exception::generateOutputString() const
    {
        std::stringstream what_buf;

        what_buf << std::endl;
        what_buf << "Reason: " << ((reason.str().empty()) ? "undefined" : reason.str())
                 << std::endl;
        what_buf << "Backtrace: " << std::endl;
        what_buf << backtrace << std::endl;

        resultingMessage = what_buf.str();
    }

} // namespace mplib::core
