#pragma once

#include <memory>

#include "mplib/core/types.h"

namespace mplib
{

    // forward declaration
    namespace math::function_approximation
    {
        class AbstractFunctionApproximation;
    } // namespace math::function_approximation
    namespace representation
    {
        class AbstractMovementPrimitive;
    } // namespace representation

    namespace generalization
    {

        class MPGeneralization
        {
        public:
        /**
        * @brief Movement Primitive Generator
        * Learn a mapping from queries to the MP weights
        * @param baseFunctionApproximator: a function approximator is used for learning this map (it can be GPR, see functionapproximation/gaussianprocessregression.h)
        * @param mp: which MP is used to learn the trajectory (such as PCVMP)
        * see examples/genMP for the usage.
        */

        MPGeneralization(std::unique_ptr<math::function_approximation::AbstractFunctionApproximation>
                            baseFunctionApproximator,
                         std::unique_ptr<representation::AbstractMovementPrimitive> mp);

        /**
        * @brief learn the MP generator with training data
        * @param queries: a list of double vectors. the size of the list is corresponding to the number of trajectories
        * each double vector is a numerical expression of the query
        * @param trajs: a list of trajectories. Each trajectory is corresponding to one query (a double vector).
        */
        void learn(const core::DVec2d& queries,
                   const std::vector<core::SampledTrajectory>& trajs);

        std::unique_ptr<representation::AbstractMovementPrimitive> generateMP(const core::DVec& query);

        private:
            std::unique_ptr<math::function_approximation::AbstractFunctionApproximation> baseFunctionApproximator;
            std::unique_ptr<representation::AbstractMovementPrimitive> baseMp;
            std::vector<std::unique_ptr<math::function_approximation::AbstractFunctionApproximation>>
                funcApproximators;
        };

    } // namespace generalization

} // namespace mplib
