#include "MPGeneralization.h"

#include <utility>

#include "mplib/core/Exception.h"
#include "mplib/core/SampledTrajectory.h"
#include "mplib/math/function_approximation/AbstractFunctionApproximation.h"
#include "mplib/math/canonical_system/CanonicalSystem.h"
#include "mplib/representation/AbstractMovementPrimitive.h"

namespace mplib::generalization
{

    MPGeneralization::MPGeneralization(
        std::unique_ptr<math::function_approximation::AbstractFunctionApproximation> baseFunctionApproximator,
        std::unique_ptr<representation::AbstractMovementPrimitive> mp) :
        baseFunctionApproximator(std::move(baseFunctionApproximator)),
        baseMp(std::move(mp))
    {
    }

    void
    MPGeneralization::learn(const core::DVec2d& queries,
                            const std::vector<core::SampledTrajectory>& trajs)
    {
        if (queries.size() != trajs.size())
        {
            throw core::Exception()
                << "the number of queries does not coincide with the number of trajectories";
        }

        if (queries.size() < 2)
        {
            throw core::Exception() << "MP generalization needs at least two tarjectories";
        }

        std::vector<core::DVec2d> wtrajs;
        unsigned int dim = trajs[0].dim();

        for (auto& traj : trajs)
        {
            if (traj.dim() != dim)
            {
                throw core::Exception() << "All trajectories must have the same dimension";
            }
            baseMp->learnFromTrajectory(traj);
            core::DVec2d weights = baseMp->getWeights();
            wtrajs.push_back(weights);
        }

        funcApproximators.clear();
        for (unsigned int i = 0; i < dim; ++i)
        {
            core::DVec2d dimWeights;
            for (auto& wtraj : wtrajs)
            {
                dimWeights.push_back(wtraj[i]);
            }
            std::unique_ptr<math::function_approximation::AbstractFunctionApproximation> funcApproximator =
                baseFunctionApproximator->clone();
            funcApproximator->learn(queries, dimWeights);
            funcApproximators.push_back(std::move(funcApproximator));
        }
    }

    std::unique_ptr<representation::AbstractMovementPrimitive>
    MPGeneralization::generateMP(const core::DVec& query)
    {
        size_t dim = funcApproximators.size();
        std::unique_ptr<representation::AbstractMovementPrimitive> mp = baseMp->clone(true);
        for (size_t i = 0; i < dim; ++i)
        {
            core::DVec weight = funcApproximators[i]->operator()(query);
            mp->setWeights(i, weight);
        }

        return mp;
    }

} // namespace mplib::generalization
