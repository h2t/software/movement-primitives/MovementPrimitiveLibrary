#include "multi_threading.h"

#include <future>

mplib::util::Semaphore::Semaphore(unsigned long count)
    : count(count)
{
}

void mplib::util::Semaphore::lock() {
    std::unique_lock<std::mutex> lock(mutex);
    condition_variable.wait(lock, [this] {
        return count != 0;
    });
    --count;
}

void mplib::util::Semaphore::unlock() {
    std::unique_lock<std::mutex> lock(mutex);
    ++count;
    condition_variable.notify_one();
}

size_t mplib::util::Semaphore::getCount() const {
    return count;
}



std::vector<std::unique_ptr<mplib::representation::AbstractMovementPrimitive> >
mplib::util::learn_mps_multithreaded(factories::AbstractMPFactory &factory, const std::string &mpType,
                                     const std::vector<core::SampledTrajectory> &trajectories, bool logging, unsigned int maxThreads) {
    if (logging)
        std::cout << "Learning VMPs multithreaded.." << std::endl;
    Semaphore semaphore(maxThreads);
    std::vector<std::future<std::unique_ptr<representation::AbstractMovementPrimitive>>> futures;

    auto start = std::chrono::steady_clock::now();
    for (const core::SampledTrajectory& trajectory : trajectories) {
        futures.push_back(std::async(std::launch::async, []
                                     (factories::AbstractMPFactory &factory, const std::string& mpType,
                                     const core::SampledTrajectory& trajectory, Semaphore &semaphore)
                          -> std::unique_ptr<representation::AbstractMovementPrimitive>
                          {
                              std::scoped_lock lock(semaphore);
                              if (auto mp = factory.createMP(mpType)) {
                                  mp->learnFromTrajectory(trajectory);
                                  return mp;
                              }
                              else return nullptr;
                          }, std::ref(factory), std::ref(mpType), std::ref(trajectory), std::ref(semaphore)));
    }

    std::vector<std::unique_ptr<representation::AbstractMovementPrimitive>> primitives;
    for (auto &future : futures) {
        if (auto result = future.get()) {
            primitives.push_back(std::move(result));
            unsigned int number = primitives.size();
            if (logging && number > 0 && number % 25 == 0) {
                float timeSec = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now() - start).count();
                std::cout << number << "/" << futures.size() << " vmps reconstructed! Estimated time left: "
                          << timeSec / ((float) number) * (futures.size() - number) << "sec" << std::endl;
            }
        }
    }
    return primitives;
}


std::vector<mplib::util::ReconstructionLoss>
mplib::util::compute_reconstruction_multithreaded(std::vector<representation::AbstractMovementPrimitive*> &primitives,
                                                  const std::vector<std::map<double, std::vector<double> > > &targetTrajectories,
                                                  bool adaptInitGoal, bool logging, unsigned int maxThreads) {
    if (primitives.size() != targetTrajectories.size())
        throw std::runtime_error("compute_reconstruction_multhreaded Failure!");

    if (logging)
        std::cout << "Computing reconstruction error.." << std::endl;

    Semaphore semaphore(maxThreads);
    std::vector<std::future<mplib::util::ReconstructionLoss>> futures;

    auto start = std::chrono::steady_clock::now();
    for (unsigned int i = 0; i < primitives.size(); i++) {
        futures.push_back(std::async(std::launch::async, []
                                     (representation::AbstractMovementPrimitive &primitive,
                                      const std::map<double, std::vector<double> > &targetTrajectory,
                                      bool adaptInitGoal, Semaphore &semaphore)
                          -> std::vector<double>
                          {
                              std::scoped_lock lock(semaphore);
                              return util::compute_reconstruction(primitive, targetTrajectory, adaptInitGoal);
                          }, std::ref(*primitives[i]), std::ref(targetTrajectories[i]), std::ref(adaptInitGoal), std::ref(semaphore)));
    }

    std::vector<mplib::util::ReconstructionLoss> error;
    for (auto &future : futures) {
         error.push_back(future.get());
         unsigned int number = error.size();
         if (logging && number > 0 && number % 25 == 0) {
             float timeSec = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now() - start).count();
             std::cout << number << "/" << futures.size() << " vmps reconstructed! Estimated time left: "
                       << timeSec / ((float) number) * (futures.size() - number) << "sec" << std::endl;
         }
    }
    return error;
}

std::vector<std::vector<mplib::util::ReconstructionLoss> >
mplib::util::compute_pairwise_reconstruction_multithreaded(std::vector<representation::AbstractMovementPrimitive *> &primitives,
                                                           const std::vector<std::map<double, std::vector<double> > > &targetTrajectories,
                                                           bool adaptInitGoal, bool logging, unsigned int maxThreads) {
    if (primitives.size() != targetTrajectories.size())
        throw std::runtime_error("compute_pairwise_reconstruction_multithreaded Failure!");

    if (logging)
        std::cout << "Computing reconstruction error.." << std::endl;

    Semaphore semaphore(maxThreads);
    std::vector<std::future<std::vector<ReconstructionLoss>>> futures;

    auto start = std::chrono::steady_clock::now();
    for (unsigned int i = 0; i < primitives.size(); i++) {
        futures.push_back(std::async(std::launch::async, []
                                     (representation::AbstractMovementPrimitive &primitive,
                                      const std::vector<std::map<double, std::vector<double> >> &targetTrajectories,
                                      bool adaptInitGoal, Semaphore &semaphore)
                          -> std::vector<ReconstructionLoss>
                          {
                              std::scoped_lock lock(semaphore);
                              std::vector<ReconstructionLoss> error;
                              for (const auto &targetTrajectory : targetTrajectories) {
                                  error.push_back(util::compute_reconstruction(primitive, targetTrajectory, adaptInitGoal));
                              }
                              return error;
                          }, std::ref(*primitives[i]), std::ref(targetTrajectories), std::ref(adaptInitGoal), std::ref(semaphore)));
    }

    std::vector<std::vector<ReconstructionLoss>> error;
    for (auto &future : futures) {
         error.push_back(future.get());
         unsigned int number = error.size();
         if (logging && number > 0 && number % 10 == 0) {
             float timeSec = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now() - start).count();
             int estimatedTimeSec = timeSec / ((float) number) * (futures.size() - number);
             std::cout << number << "/" << futures.size() << " vmps reconstructed! Estimated time left: "
                       << estimatedTimeSec / 60 << "min " << estimatedTimeSec % 60 << "s" << std::endl;
         }
    }
    if (logging) {
        int timeSec = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now() - start).count();
        std::cout << "Pairwise reconstruction took " << timeSec / 60 << "min " << (timeSec % 60) << "s" << std::endl;
    }
    return error;


}
