#pragma once

#include <mplib/factories/AbstractMPFactory.h>
#include <mplib/core/SampledTrajectory.h>
#include <mplib/representation/AbstractMovementPrimitive.h>
#include <mplib/math/canonical_system/CanonicalSystem.h>

#include "reconstruction.h"

#include <mutex>
#include <condition_variable>
#include <thread>

namespace mplib::util
{

class Semaphore {
public:
    Semaphore(unsigned long count);

    void lock();

    void unlock();

    size_t getCount() const;

private:
    std::mutex mutex;
    std::condition_variable condition_variable;
    unsigned long count;
};


std::vector<std::unique_ptr<representation::AbstractMovementPrimitive>>
learn_mps_multithreaded(factories::AbstractMPFactory &factory, const std::string& mpType, const std::vector<core::SampledTrajectory> &trajectories,
                        bool logging = false, unsigned int maxThreads = std::thread::hardware_concurrency());

std::vector<ReconstructionLoss>
compute_reconstruction_multithreaded(std::vector<representation::AbstractMovementPrimitive*> &primitives,
                                     const std::vector<std::map<double, std::vector<double>>> &targetTrajectories,
                                     bool adaptInitGoal = true, bool logging = false, unsigned int maxThreads = std::thread::hardware_concurrency());

std::vector<std::vector<ReconstructionLoss>>
compute_pairwise_reconstruction_multithreaded(std::vector<representation::AbstractMovementPrimitive*> &primitives,
                                              const std::vector<std::map<double, std::vector<double>>> &targetTrajectories,
                                              bool adaptInitGoal = true, bool logging = false, unsigned int maxThreads = std::thread::hardware_concurrency());

} // namespace mplib::util
