#include "reconstruction.h"

#include <mplib/core/SystemState.h>
#include <mplib/representation/MPState.h>
#include <mplib/math/util/eigen_arithmetic.h>

mplib::util::ReconstructionLoss mplib::util::compute_reconstruction(representation::AbstractMovementPrimitive &primitive,
                                                                    const std::map<double, std::vector<double> > &targetTrajectory,
                                                                    bool adaptInitGoal) {
    if (adaptInitGoal) {
        const std::vector<double> &start = targetTrajectory.begin()->second;
        primitive.setInitialState(core::SystemState::convertPosArrayToStates<representation::MPState>(start));
        const std::vector<double> &goal = targetTrajectory.rbegin()->second;
        primitive.setGoals(goal);
    }

    float previousTimestamp = targetTrajectory.begin()->first;
    core::DVec2d previousState = core::SystemState::convertStatesTo2DArray(primitive.getStartState());
    core::DVec canonicalValues = std::vector<double>(1, 1.0);

    ReconstructionLoss sumError = { 0.0 };
    if (primitive.getSpace() == representation::Space::TaskSpace7D)
        sumError.push_back(0.0);

    for (const auto& timePoint : targetTrajectory) {
        previousState = primitive.calculateTrajectoryPoint(timePoint.first, primitive.getGoals(),
                                                           previousTimestamp, previousState, canonicalValues, 1.0);
        previousTimestamp = timePoint.first;

        core::DVec positions;
        for (auto& d : previousState)
            positions.push_back(d[0]);

        switch (primitive.getSpace()) {
        case representation::Space::TaskSpace7D:
        {
            const Eigen::Matrix<double, 3, 1> position = math::util::toPosition<double>(positions);
            const Eigen::Matrix<double, 3, 1> targetPosition = math::util::toPosition<double>(timePoint.second);
            sumError[0] += (position - targetPosition).norm();
            const Eigen::Quaterniond orientation = math::util::toQuat<double>(positions);
            const Eigen::Quaterniond targetOrientation = math::util::toQuat<double>(timePoint.second);
            const double scalar = orientation.normalized().dot(targetOrientation.normalized());
            float theta = acos(std::min(std::max(scalar, -1.0), 1.0));
            if (theta > M_PI / 2.0)
                theta = M_PI - theta;
            sumError[1] += abs(theta);
            break;
        }
        case representation::Space::Custom:
        case representation::Space::JointSpace:
            throw std::runtime_error("Not implemented yet!");
        }
    }

    for (unsigned int i = 0; i < sumError.size(); i++)
        sumError[i] /= targetTrajectory.size();

    return sumError;
}
