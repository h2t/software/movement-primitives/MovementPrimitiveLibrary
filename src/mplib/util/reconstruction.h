#pragma once

#include <mplib/representation/AbstractMovementPrimitive.h>
#include <mplib/math/canonical_system/CanonicalSystem.h>

namespace mplib::util
{

using ReconstructionLoss = std::vector<double>;

ReconstructionLoss compute_reconstruction(representation::AbstractMovementPrimitive &primitive,
                                          const std::map<double, std::vector<double>> &targetTrajectory,
                                          bool adaptInitGoal = true);

} // namespace mplib::util
