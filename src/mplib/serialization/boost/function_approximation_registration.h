/**
* This file is part of MPLIB.
*
* MPLIB is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* MPLIB is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MPLIB::
* @author     H2T @ KIT
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#pragma once

#include <boost/serialization/export.hpp>

#include "mplib/math/function_approximation/ExactReproduction.h"
#include "mplib/math/function_approximation/LocallyWeightedRegression.h"
#include "mplib/math/function_approximation/RadialBasisFunctionInterpolator.h"

BOOST_CLASS_EXPORT_KEY(mplib::math::function_approximation::AbstractFunctionApproximation)
BOOST_CLASS_EXPORT_KEY(mplib::math::function_approximation::ExactReproduction)
BOOST_CLASS_EXPORT_KEY(mplib::math::function_approximation::LocallyWeightedRegression<
                       mplib::math::kernel::GaussianFunction>)
BOOST_CLASS_EXPORT_KEY(mplib::math::function_approximation::RadialBasisFunctionInterpolator<
                       mplib::math::kernel::GaussianRadialBasisFunction>)
