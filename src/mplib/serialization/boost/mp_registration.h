/**
* This file is part of DMP.
*
* DMP is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* MPLIB is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MPLIB::
* @author     H2T @ KIT
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#pragma once

#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/serialization.hpp>

#define FIXED_BOOST_SERIALIZATION_FACTORY_0(T)                                                     \
    namespace boost                                                                                \
    {                                                                                              \
        namespace serialization                                                                    \
        {                                                                                          \
            template <>                                                                            \
            inline T*                                                                              \
            factory<T, 0>(std::va_list)                                                            \
            {                                                                                      \
                return new T();                                                                    \
            }                                                                                      \
        }                                                                                          \
    }


#include "mplib/math/canonical_system/CanonicalSystem.h"
#include "mplib/math/canonical_system/LinearCanonicalSystem.h"
#include "mplib/math/canonical_system/LinearDecayCanonicalSystem.h"
#include "mplib/math/canonical_system/PeriodicCanonicalSystem.h"
#include "mplib/math/ode/BasicODE.h"
#include "mplib/representation/AbstractMovementPrimitive.h"
#include "mplib/representation/MPState.h"
#include "mplib/representation/dmp/DiscreteDMP.h"
#include "mplib/representation/dmp/PeriodicDMP.h"

BOOST_CLASS_EXPORT_KEY(mplib::math::canonical_system::CanonicalSystem)
BOOST_CLASS_EXPORT_KEY(mplib::math::canonical_system::LinearCanonicalSystem)
BOOST_CLASS_EXPORT_KEY(mplib::math::canonical_system::LinearDecayCanonicalSystem)
BOOST_CLASS_EXPORT_KEY(mplib::math::canonical_system::PeriodicCanonicalSystem)
BOOST_CLASS_EXPORT_KEY(mplib::math::ode::BasicODE)
BOOST_CLASS_EXPORT_KEY(mplib::representation::AbstractMovementPrimitive)
BOOST_CLASS_EXPORT_KEY(mplib::representation::MPState)
BOOST_CLASS_EXPORT_KEY(mplib::representation::dmp::DiscreteDMP)
BOOST_CLASS_EXPORT_KEY(mplib::representation::dmp::PeriodicDMP)

FIXED_BOOST_SERIALIZATION_FACTORY_0(mplib::math::canonical_system::CanonicalSystem)
FIXED_BOOST_SERIALIZATION_FACTORY_0(mplib::math::canonical_system::LinearDecayCanonicalSystem)
FIXED_BOOST_SERIALIZATION_FACTORY_0(mplib::math::canonical_system::PeriodicCanonicalSystem)
FIXED_BOOST_SERIALIZATION_FACTORY_0(mplib::representation::dmp::DiscreteDMP)
FIXED_BOOST_SERIALIZATION_FACTORY_0(mplib::representation::dmp::PeriodicDMP)


#ifdef ARMADILLO_FOUND

#include "mplib/representation/vmp/GaussianVMP.h"
#include "mplib/representation/vmp/PrincipalComponentVMP.h"
#include "mplib/representation/vmp/TaskSpaceGaussianVMP.h"
#include "mplib/representation/vmp/TaskSpacePrincipalComponentVMP.h"
#include "mplib/representation/vmp/VMP.h"
#include "mplib/representation/vmp/ViaPose.h"

BOOST_CLASS_EXPORT_KEY(mplib::representation::vmp::VMP)
BOOST_CLASS_EXPORT_KEY(mplib::representation::vmp::PrincipalComponentVMP)
BOOST_CLASS_EXPORT_KEY(mplib::representation::vmp::GaussianVMP)
BOOST_CLASS_EXPORT_KEY(mplib::representation::vmp::ViaPose)
BOOST_CLASS_EXPORT_KEY(mplib::representation::vmp::TaskSpacePrincipalComponentVMP)
BOOST_CLASS_EXPORT_KEY(mplib::representation::vmp::TaskSpaceGaussianVMP)

FIXED_BOOST_SERIALIZATION_FACTORY_0(mplib::representation::vmp::PrincipalComponentVMP)
FIXED_BOOST_SERIALIZATION_FACTORY_0(mplib::representation::vmp::GaussianVMP)
FIXED_BOOST_SERIALIZATION_FACTORY_0(mplib::representation::vmp::ViaPose)
FIXED_BOOST_SERIALIZATION_FACTORY_0(mplib::representation::vmp::TaskSpacePrincipalComponentVMP)
FIXED_BOOST_SERIALIZATION_FACTORY_0(mplib::representation::vmp::TaskSpaceGaussianVMP)

#endif
