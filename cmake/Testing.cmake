# macros for testing

# Switch for test cases
option(MPLIB_BUILD_TESTS "Build tests" ON)

if( MPLIB_BUILD_TESTS )
    find_package(Boost ${MPLIB_BOOST_VERSION} EXACT REQUIRED COMPONENTS unit_test_framework)

    enable_testing()
    
endif( MPLIB_BUILD_TESTS )

##
## This macro adds a testcase to the testsuite which is run by the command 'make test'.
## All test executables are stored in ${MPLIB_BIN_DIR}.
## The Output path of the log files is stored in Test.h which gets generated above.
##
## PARAM TEST_NAME name of the test and the executable which gets created
## PARAM TEST_FILE name of the cpp file containing the test code
## PARAM DEPENDENT_LIBRARIES the libraries which must be linked to the testcase executable
##
macro(MPLIB_add_test TEST_NAME TEST_FILE DEPENDENT_LIBRARIES)
    if (MPLIB_BUILD_TESTS)
        message(STATUS "    Building test ${TEST_NAME}")
#        message("${DEPENDENT_LIBRARIES}")
        add_executable(${TEST_NAME} ${TEST_FILE})
        target_include_directories(${TEST_NAME} PUBLIC ${PROJECT_SOURCE_DIR}/src)
        if(GCOV_FLAG)
            SET_TARGET_PROPERTIES(${TEST_NAME} PROPERTIES LINK_FLAGS ${GCOV_FLAG})
        endif()
        if (NOT MPLIB_OS_WIN)
            target_link_libraries(${TEST_NAME} ${Boost_UNIT_TEST_FRAMEWORK_LIBRARY} ${DEPENDENT_LIBRARIES})
        endif()
        message(STATUS "MPLIB_BIN_DIR: ${MPLIB_BIN_DIR}")
        add_test(NAME ${TEST_NAME}
                 COMMAND "${MPLIB_BIN_DIR}/${TEST_NAME}" --output_format=XML --log_level=all --report_level=detailed
                 WORKING_DIRECTORY ${CMAKE_BINARY_DIR})
    endif()
endmacro()
